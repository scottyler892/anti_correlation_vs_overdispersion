import os
import gc
import h5py
import numpy as np
import pandas as pd
import scanpy as sc
import networkx as nx
from copy import deepcopy
from matplotlib import pyplot as plt
from scipy.sparse import csc_matrix
from anticor_features.anticor_features import get_anti_cor_genes
from pyminer.common_functions import write_table, cmd
from pyminer.common_functions import import_dict, save_dict
from pyminer.pyminer_cell_graph_plotting import get_pos

gc.enable()

# You can download this from our figshare deposit
#adata=sc.read('/media/scott/ssd_2tb/pbmc_ref/million_pbmc/adata_log_rle_norm_annotated.h5ad' )
adata_dir = "/media/scott/ssd_2tb/pbmc_ref/million_pbmc/"
summary_out_dir = "data/pbmcs/pops_of_int_data/cd4cm_subclust"
subclust_dir = "/media/scott/ssd_2tb/pbmc_ref/cd4cm_subclust/"


adata=sc.read(os.path.join(adata_dir,"adata_log_rle_norm_annotated_analyzed.h5ad"))

## CD4 cm-ish top cluster
adata_sub = adata[adata.obs["cluster_manual"]=="12,21"]
del adata
gc.collect()


anti_cor_table = get_anti_cor_genes(adata_sub.X.T, adata_sub.var.index.tolist())
anti_cor_table.to_csv(os.path.join(summary_out_dir,"anticor_features_table.tsv"),sep="\t")

#anti_cor_table = pd.read_csv(os.path.join(summary_out_dir,"anticor_features_table.tsv"),sep="\t")

selected_genes = anti_cor_table[anti_cor_table["selected"]==True]["gene"].tolist()

adata_sub.raw = adata_sub



##################################
## make a dense hdf5 file
adata_sub = adata_sub[:,selected_genes]
adata_sub_X = csc_matrix(adata_sub.X)

# sns.clustermap(adata_sub_X[np.random.choice(np.arange(adata_sub_X.shape[0]),size=(1000)),:].todense())
# plt.show()


f = h5py.File(os.path.join(subclust_dir,"cd4cm_subset.hdf5"), "w")
dset = f.create_dataset("infile",
			                    (len(selected_genes),adata_sub_X.shape[0]),
			                    dtype=np.float32)
for i in range(len(selected_genes)):
	dset[i,:] = adata_sub_X[:,i].todense().squeeze()


f.close()


## write the genes to file
write_table([[g] for g in selected_genes], os.path.join(subclust_dir,"ID_list.txt"))
## don't forget to add a header line for the cell name column
write_table([["cells"]]+[[g] for g in adata_sub.obs.index], os.path.join(subclust_dir,"columns.txt"))

cmd("""python3 -m pyminer.clustering -block_size 10000 -no_plots -hdf5 -i '/media/scott/ssd_2tb/pbmc_ref/cd4cm_subclust/cd4cm_subset.hdf5' -ID_list '/media/scott/ssd_2tb/pbmc_ref/cd4cm_subclust/ID_list.txt' -columns '/media/scott/ssd_2tb/pbmc_ref/cd4cm_subclust/columns.txt' -clust_on_genes '/media/scott/ssd_2tb/pbmc_ref/cd4cm_subclust/ID_list.txt' -leave_mito_ribo -louvain_clust""")
####################################
## read in the clustering results
sub_cluster_results = pd.read_csv("/media/scott/ssd_2tb/pbmc_ref/cd4cm_subclust/sample_clustering_and_summary/sample_k_means_groups.tsv",sep="\t",header=None)
sub_cluster_results.columns=["cells","sub_clusters"]
sub_cluster_results.index = sub_cluster_results["cells"]
clusts = sub_cluster_results
clusts.columns = ["cells","cluster"]
####################################
clust_dict={}
for idx, row in enumerate(sub_cluster_results["cluster"].tolist()):
    clust_dict[idx]=row
####################################
## seed from umap

sc.pp.scale(adata_sub, max_value=10)
sc.tl.pca(adata_sub, svd_solver='arpack')
## get the neighbors
sc.pp.neighbors(adata_sub, n_neighbors=10, n_pcs=40)
## calculate the PCs
sc.tl.umap(adata_sub, n_components=2)
sc.tl.leiden(adata_sub, key_added='leiden_cd4em_subclust')


adata_sub.obs["sub_umap_1"]=adata_sub.obsm["X_umap"][:,0]
adata_sub.obs["sub_umap_2"]=adata_sub.obsm["X_umap"][:,1]


###########################
G = import_dict(os.path.join(subclust_dir,"sample_clustering_and_summary/cell_embedding.graphpkl"))

print("getting page ranks")
pr_dict = nx.pagerank(G)
nx.set_node_attributes(G, pr_dict ,name="page_rank")
nx.set_node_attributes(G, clust_dict, name="cluster")

#pr_dict = nx.get_node_attributes(G, "page_rank")
#sub_ids = sorted(np.random.choice(np.arange(len(G.nodes())), size=75000, replace=False).tolist())
percentile_keep=0.1

sub_ids = []
for clust in set(clusts["cluster"]):
    print("adding cluster:",clust)
    temp_idxs = np.where(clusts["cluster"]==clust)[0]
    temp_prs = np.array([pr_dict[idx] for idx in temp_idxs])
    pr_order = np.argsort(temp_prs)[::-1]
    print(pr_order)
    print("    ",temp_prs[pr_order[:5]])
    num_samples = int(percentile_keep * len(temp_idxs))
    sub_ids += temp_idxs[pr_order[:num_samples]].tolist()

sub_ids = sorted(sub_ids)

print("subset IDs:",sub_ids[:5],"...")

save_dict(G,os.path.join(subclust_dir,"sample_clustering_and_summary/cellPageRank_embedding.graphpkl"))
#G=import_dict(os.path.join(subclust_dir,"sample_clustering_and_summary/cellPageRank_embedding.graphpkl"))

G_sub = G.subgraph(sub_ids).copy()
del G
gc.collect()
G_sub = get_pos(G_sub,pos_iters=1)

save_dict(G_sub, os.path.join(subclust_dir,"sample_clustering_and_summary/cellPageRankSUBSET_embedding_with_pos.graphpkl"))

pos_dict = nx.get_node_attributes(G_sub,"pos_0")



##########################
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import cm


def convert_pos_dict_to_array(in_dict):
    array_len = max(list(in_dict.keys()))+1
    print(min(list(in_dict.keys())))
    out_array = np.zeros((int(array_len),in_dict[array_len-1].shape[0]))
    print(out_array.shape)
    for key, loc in in_dict.items():
        #print(key, loc)
        out_array[int(key)]=loc
    return(out_array)


##########################
cluster_key = 'leiden_cd4em_subclust'
adata_sub.obs["cd4cm_subclusters"] = sub_cluster_results["cluster"].astype(str).astype('category')
adata_sub.obs["sub_spring_1"]=np.nan#xy[:,0]
adata_sub.obs["sub_spring_2"]=np.nan#xy[:,1]


for i in range(len(sub_ids)):
	temp_id = sub_ids[i]
	adata_sub.obs.iloc[temp_id,-2]=pos_dict[temp_id][0]
	adata_sub.obs.iloc[temp_id,-1]=pos_dict[temp_id][1]



#################
x_key = "sub_umap_1"
y_key = "sub_umap_2"
xmin = adata_sub.obs[x_key].min()
xmax = adata_sub.obs[x_key].max()
xbuff = 0.05*(xmax-xmin)
xmin -= xbuff
xmax += xbuff
##
ymin = adata_sub.obs[y_key].min()
ymax = adata_sub.obs[y_key].max()
ybuff = 0.05*(ymax-ymin)
ymin -= ybuff
ymax += ybuff
################

adata_sub.obs["cell"]=adata_sub.obs.index



def merge_palettes(palette1, palette2, n_colors):
    palette1_colors = sns.color_palette(palette1, n_colors)
    palette2_colors = sns.color_palette(palette2, n_colors)[::-1]
    merged_palette = []
    for color1, color2 in zip(palette1_colors, palette2_colors):
        merged_palette.extend([color1, color2])
    return merged_palette


n_colors = ceil(len(set(adata_sub.obs[cluster_key]))/2)+1  # Adjust as needed
merged_palette = merge_palettes("bright", "colorblind", n_colors)
np.random.seed(12345)
np.random.shuffle(merged_palette)
sns.set_palette(merged_palette)
categories = sorted(list(set(adata_sub.obs[cluster_key])))
color_dict = dict(zip(categories, merged_palette))



#adata_sub.obs
## plot the cd4em comparisons
out_dir = summary_out_dir
temp_clusts = list(map(str,list(set(adata_sub.obs[cluster_key]))))
[2,3,4,7,8,10]
plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
sns.scatterplot(x="sub_umap_1",y="sub_umap_2",hue=cluster_key,data=adata_sub.obs[adata_sub.obs[cluster_key].isin(temp_clusts)],s=.38,edgecolor = None,
    palette=color_dict)#legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.26, 1),
       borderaxespad=0)
plt.title("CD4cm subclusters")
plt.tight_layout()
plt.savefig(os.path.join(summary_out_dir,"all_CD4cm_original_subclusters.png"),dpi=300)




plot_gene_in_mono_subset(
    adata_sub, 
    adata_sub.obs, 
    genes = ['TCF7','TOX','TTN','IL2RA','DUSP4','STAT4','FLNB','AFF3'], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = temp_clusts, 
    cell_subset_label = "CD4cm_subclusts",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29",#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/
                "IL2RA":"CD25"},
    x_key="sub_umap_1",y_key="sub_umap_2",
    cluster_key=cluster_key)




['TCF7','TOX','TTN','IL2RA','DUSP4','STAT4','FLNB','ITGAL','UTRN','AFF3','RUNX2','TXK',"ST8SIA1","GCNT4","TSHZ2","KCNQ5","FOXP3"]

['TCF7','TOX','TTN','IL2RA','DUSP4','STAT4','FLNB','ITGAL','UTRN','AFF3','RUNX2','TXK',"ST8SIA1","GCNT4","TSHZ2","KCNQ5","CCR4","TGFB1"]






###################################3
###################################
# Now re-do it with the manually stitched clusters


cluster_key_manual=cluster_key+"_manual"
adata_sub.obs[cluster_key_manual]=adata_sub.obs[cluster_key].astype(str)
merge_list = [sorted([str(i) for i in set(adata_sub.obs[cluster_key_manual]) if i not in ["7","11"]])]
for temp_merger in merge_list:
	main_label = ",".join(list(map(str,temp_merger)))
	for target_merge_label in temp_merger:
		adata_sub.obs[cluster_key_manual][adata_sub.obs[cluster_key_manual]==str(target_merge_label)]=main_label







n_colors = ceil(len(set(adata_sub.obs[cluster_key_manual]))/2)+1  # Adjust as needed
merged_palette = merge_palettes("bright", "colorblind", n_colors)
np.random.seed(12345)
np.random.shuffle(merged_palette)
sns.set_palette(merged_palette)
categories = sorted(list(set(adata_sub.obs[cluster_key_manual])))
color_dict = dict(zip(categories, merged_palette))
subset_color_dict = deepcopy(color_dict)


temp_clusts = list(set(adata_sub.obs[cluster_key_manual]))


import textwrap
plt.clf()
scatter = sns.scatterplot(x="sub_umap_1",y="sub_umap_2",hue=cluster_key_manual,data=adata_sub.obs[adata_sub.obs[cluster_key_manual].isin(temp_clusts)],s=.38,edgecolor = None, palette=color_dict)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
handles, labels = scatter.get_legend_handles_labels()
wrapped_labels = [textwrap.fill(label, 8) for label in labels]  # adjust the number to control the width of the text
plt.legend(handles, wrapped_labels, bbox_to_anchor=(1.26, 1), borderaxespad=0)
plt.title("CD4cm subclusters")
plt.tight_layout()
plt.savefig(os.path.join(summary_out_dir,"all_CD4cm_manual_subclusters.png"),dpi=300)




adata_sub.obs[cluster_key_manual]=adata_sub.obs[cluster_key_manual].astype("category")



plot_gene_in_mono_subset(
    adata_sub, 
    adata_sub.obs, 
    genes = ['TCF7','TOX','TTN',"PECAM1",'AFF3','IL2RA','DUSP4','STAT4','RUNX2',"TSHZ2"], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = temp_clusts, 
    cell_subset_label = "CD4cm_subclusts",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29",#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/
                "IL2RA":"CD25"},
    x_key="sub_umap_1",y_key="sub_umap_2",
    cluster_key=cluster_key_manual,
    color_dict = subset_color_dict)



adata_sub = sc.read(os.path.join(adata_dir,"adata_log_rle_norm_annotated_analyzed_CD4cm_subclust.h5ad"))
#adata_sub.write(os.path.join(adata_dir,"adata_log_rle_norm_annotated_analyzed_CD4cm_subclust.h5ad"))




plot_gene_in_mono_subset(
    adata_sub, 
    adata_sub.obs, 
    genes = ['STAT4','RUNX2',"GATA3","TSHZ2","ICOS", "IL2RA","TOX","SELL","PECAM1","AFF3"], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = temp_clusts, 
    cell_subset_label = "CD4cm_subclusts",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29",#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/
                "IL2RA":"CD25",
                "TBX21":"T-bet"},
    x_key="sub_umap_1",y_key="sub_umap_2",
    cluster_key=cluster_key_manual,
    color_dict = )



## Current
plot_gene_in_mono_subset(
    adata, 
    adata_sub.obs, 
    genes = ['STAT4','RUNX2',"GATA3","TSHZ2","ICOS", "IL2RA","TOX","FOXP3","SELL","PECAM1","AFF3"], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = temp_clusts, 
    cell_subset_label = "CD4cm_subclusts",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29",#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/
                "IL2RA":"CD25",
                "TBX21":"T-bet"},
    x_key="sub_umap_1",y_key="sub_umap_2",
    cluster_key=cluster_key_manual,
    color_dict = subset_color_dict)




plot_gene_in_mono_subset(
    adata_sub, 
    adata_sub.obs, 
    genes = ["IL2RA", "GATA3",'STAT4','RUNX2',"TSHZ2","ICOS", "KLRG1","TOX","TTN","SELL","PECAM1","AFF3"], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = temp_clusts, 
    cell_subset_label = "CD4cm_subclusts",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29",#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/
                "IL2RA":"CD25",
                "TBX21":"T-bet"},
    x_key="sub_umap_1",y_key="sub_umap_2",
    cluster_key=cluster_key_manual)




plot_gene_in_mono_subset(
    adata_sub, 
    adata_sub.obs, 
    genes = ["CD4","CD8A","PRDM1","TOX","LEF1","PECAM1","FOS","FOSB","ADGRE5"],#['TCF7','TOX','TTN','AFF3','IL2RA','DUSP4','STAT4','RUNX2',"TSHZ2"], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = temp_clusts, 
    cell_subset_label = "CD4cm_subclusts",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29",#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/
                "IL2RA":"CD25"},
    x_key="sub_umap_1",y_key="sub_umap_2",
    cluster_key=cluster_key_manual)









#############################################
import pandas as pd
import numpy as np


def transfer_columns(adata, adata_sub, transfer_cols):
    # Ensure that the inputs are of correct type
    if not isinstance(transfer_cols, list) or not all(isinstance(col, str) for col in transfer_cols):
        raise ValueError('transfer_cols should be a list of strings.')
    # Initialize the columns in adata with np.nan
    for col in transfer_cols:
        if col not in adata_sub.obs.columns:
            raise ValueError(f'Column {col} is not in adata_sub.obs.')
        adata.obs[col] = np.nan
    # Find the common indices between adata and adata_sub
    common_indices = adata.obs.index.intersection(adata_sub.obs.index)
    # Transfer the values from adata_sub to adata
    for col in transfer_cols:
        adata.obs.loc[common_indices, col] = adata_sub.obs.loc[common_indices, col]
    return



transfer_columns(adata,adata_sub,['leiden_cd4em_subclust_manual', "sub_umap_1", "sub_umap_2"])




#########################################################################
## write the mean expression values to file for tables
out_means_full= pd.DataFrame({}, index = adata.var["gene_id"])
out_means_full["symbol"]=adata.var.index
for ct in set(adata.obs["cluster_manual"]):
	out_means_full[ct]=np.mean(adata[adata.obs["cluster_manual"]==ct].X.T, axis=1)



out_means_sub= pd.DataFrame({}, index = adata.var["gene_id"])
out_means_sub["symbol"]=adata.var.index
new_groups = [thing for thing in list(set(adata.obs["leiden_cd4em_subclust_manual"])) if str(thing) != "nan"]
for ct in new_groups:
	print(str(ct))
	out_means_sub[ct]=np.mean(adata[adata.obs["leiden_cd4em_subclust_manual"]==ct].X.T, axis=1)


means_out_dir = "data/pbmcs/pops_of_int_data/"
out_means_full.to_csv(os.path.join(means_out_dir, "manual_cluster_means.tsv"),sep="\t")
out_means_sub.to_csv(os.path.join(means_out_dir, "manual_sub_cluster_means.tsv"),sep="\t")


######################################################
## and do the last pathway analysis for 5,8 vs 10
import random
from gprofiler import GProfiler
gp = GProfiler('BLAH_'+str(random.randint(0,int(1e6))), want_header = True)

def pretty_paths(in_res):
	out_df = pd.DataFrame(in_res[1:],columns = in_res[0])
	return(out_df)


def get_up_dn(
	in_df,
	lg_fc_cut = 0.2,
	p_cut = np.exp(-20)
	):
    up = in_df["names"][(in_df["logfoldchanges"]>lg_fc_cut) & (in_df["pvals_adj"]<p_cut)]
    dn = in_df["names"][(in_df["logfoldchanges"]<-lg_fc_cut) & (in_df["pvals_adj"]<p_cut)]
    bg = in_df["names"][(in_df["logfoldchanges"]!=0)]
    return(up, dn, bg)


fiveeight_dir = "/home/scott/bin/anti_correlation_vs_overdispersion/data/pbmcs/pops_of_int_data/5,8_vs_10_genes_and_paths/"
deg_table = pd.read_csv(os.path.join(fiveeight_dir,"5,8_vs_10monocyte_degs.tsv"),sep="\t")
up, dn, bg = get_up_dn(deg_table)

mono_up_paths = pretty_paths(gp.gprofile(
	list(up), custom_bg = list(bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

mono_up_paths.to_csv(os.path.join(fiveeight_dir,"paths_up_5,8_vs_10_mono.tsv"),sep="\t")


mono_dn_paths = pretty_paths(gp.gprofile(
	list(dn), custom_bg = list(bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

mono_dn_paths.to_csv(os.path.join(fiveeight_dir,"paths_dn_5,8_vs_10_mono.tsv"),sep="\t")


################################################
## Tested if there was a significant difference by disease & there was not

sample_by_subclust = adata_sub.obs[['sample','leiden_cd4em_subclust_manual']]


ct = pd.crosstab(sample_by_subclust['leiden_cd4em_subclust_manual'],sample_by_subclust['sample'])

from percent_max_diff.percent_max_diff import pmd

np.random.seed(123456)
pmd_res = pmd(ct)

res_table = deepcopy(pmd_res.z_scores).T
disease_vect = np.array(["Ctrl"]*res_table.shape[0])
disease_vect[res_table.index.str.startswith("D_")] = "T1D"

disease = [1 if samples[i][0]=="D" else 0 for i in range(len(res_table.index.tolist()))]

res_table["disease"]=disease
#res_table["disease"]=res_table["disease"].astype("category")





def fdr_p(in_p):
    dummy, out_p = fdr(in_p)
    return(out_p)


def get_stats(z_scores_df, sample_annotations):
    poisson_glm_statistic = []
    poisson_glm_p_val = []
    pmd_z_glm_statistic = []
    pmd_z_glm_p_val = []
    for i in range(z_scores_df.shape[0]):
        print("\n\n\n\n",i)
        x = deepcopy(sample_annotations)
        data = sm.add_constant(x)
        ####
        #p_model = sm.GLM(pbmcs.iloc[i,1:], data, family=sm.families.Poisson())
        ####
        g_model = sm.GLM(z_scores_df.iloc[i,:], data, family=sm.families.Gaussian())
        g_res = g_model.fit()
        print("\npmd z-scores with Gaussian")
        print(g_res.summary())
        pmd_z_glm_statistic.append(g_res.tvalues["disease"])
        pmd_z_glm_p_val.append(g_res.pvalues["disease"])
        ####
    out_stats = pd.DataFrame({
        "pmd_z_glm_statistic":pmd_z_glm_statistic,
        "pmd_z_glm_p_val":pmd_z_glm_p_val,
        "pmd_z_glm_p_val_BH":fdr_p(pmd_z_glm_p_val)}
        ,
        index=z_scores_df.index
    )
    return(out_stats)



res_table = get_stats(deepcopy(pmd_res.z_scores), res_table["disease"])




