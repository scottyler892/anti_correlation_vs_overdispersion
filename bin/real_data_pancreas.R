## import the basic clustering stuff
library("gprofiler2")
library("ggplot2")
source("bin/real_data_recursive.R")

starter_seed<-123456789
set.seed(starter_seed)

real_datasets<-c()

MASTER_OUT_DIR<-'data/pancreas/'
ANALYSIS_OUT<-'data/pancreas_results/'

panc_datasets <- c("data/pancreas/PMID_27693023_GSE85241/PMID_27693023_GSE85241.tsv","data/pancreas/PMID_30759402/PMID_30759402.tsv","data/pancreas/PMID_27667665/PMID_27667665_GSE81608.tsv","data/pancreas/PMID_27667667/PMID_27667667.tsv","data/pancreas/PMID_27364731/PMID_27364731_GSE83139.tsv","data/pancreas/PMID_26691212/PMID_26691212.tsv","data/pancreas/PMID_27693023_GSE81076/PMID_27693023_GSE81076.tsv")



do_stats<-function(in_dir, force = FALSE){
    in_file <- paste(in_dir, "expression.tsv", sep="/")
    sg_file <- paste(in_dir, "sample_clustering_and_summary/sample_k_means_groups.tsv", sep="/")
    sig_dir <- paste(in_dir, "sample_clustering_and_summary/", sep="/")
    dir.create(sig_dir, showWarnings = FALSE)
    stats_call <- c("python3 -m pyminer.get_stats -i ",in_file," -sample_groups ", sg_file, " -out_dir ", sig_dir)
    stats_call <- paste(stats_call, collapse = ' ')
    print(stats_call)
    aov_file <- paste(sig_dir,'significance/groups_1way_anova_results.tsv',sep='/')
    print(aov_file)
    finished_stats_bool <- file.exists(aov_file)
    if (!(force | !finished_stats_bool)) {
        print("already done stats & not forcing")
    } else {
        system(stats_call)
    }
}


do_pancreas<-function(real_datasets){
    for (i in 1:length(real_datasets)){
        in_file <- real_datasets[i]
        temp_species <- 'hsapiens'

        ## normalize the simulation through UMI downsampling
        sim <- load_data(in_file)
        iter_dir <- dirname(in_file)

        ## create the directories
        disp_dir <- paste(iter_dir,'disp', sep='/')
        dir.create(disp_dir, showWarnings=FALSE)
        
        anti_cor <- paste(iter_dir,'anti_cor', sep='/')
        dir.create(anti_cor, showWarnings=FALSE)

        disp_seurat_dir <- paste(iter_dir,'disp_seurat', sep='/')
        dir.create(disp_seurat_dir, showWarnings=FALSE)

        brennecke_dir <- paste(iter_dir,'brennecke', sep='/')
        dir.create(brennecke_dir,showWarnings=FALSE)

        m3_dir <- paste(iter_dir,'m3_drop', sep='/')
        dir.create(m3_dir,showWarnings=FALSE)
        
        ## adding zero inf
        zero_inf_dir <- paste(iter_dir,'zero_inf', sep='/')
        dir.create(zero_inf_dir, showWarnings=FALSE)

        ## adding deviance
        deviance_dir <- paste(iter_dir,'deviance', sep='/')
        dir.create(deviance_dir, showWarnings=FALSE)

        ## write the datasets
        all_dirs<-c(anti_cor, disp_dir, disp_seurat_dir, brennecke_dir,m3_dir,zero_inf_dir,deviance_dir)

        write_dataset(sim,all_dirs)

        do_seurat_overdispersed(sim, disp_seurat_dir, species = temp_species, cell_subset = all_clells, force=F, do_rank = F)
        do_m3drop(sim, m3_dir, species = temp_species, cell_subset = all_clells, force=F, do_rank = F)
        do_brennecke(sim, brennecke_dir, species = temp_species, cell_subset = all_clells, force=F, do_rank = F)
        do_pyminer_run(disp_dir, anti=FALSE, species = temp_species, cell_subset = all_clells, force=F, do_rank = F)
        do_anti_cor(anti_cor, anti_cor, species = temp_species, cell_subset = all_clells, force=F, do_rank = F)
        do_hippo_zero_inf(sim, zero_inf_dir, species = temp_species, cell_subset = all_clells, force=F)
        do_hippo_deviance(sim, deviance_dir, species = temp_species, cell_subset = all_clells, force=F)
        do_stats(disp_seurat_dir)
        do_stats(m3_dir)
        do_stats(brennecke_dir)
        do_stats(disp_dir)
        do_stats(anti_cor)
        do_stats(zero_inf_dir)
        do_stats(deviance_dir)
    }
}


get_pertinent_genes<-function(in_dir){
    in_genes <- read.csv(paste(in_dir, "sample_clustering_and_summary/genes_used_for_clustering.txt", sep='/'), sep='\t', stringsAsFactors = F)
    in_genes <- unlist(in_genes[,1])## the first column is the gene names
    return(in_genes)
}


get_sig_enrich_cluster_pathways<-function(in_dir, method_name, dataset_name){
    # in_genes <- read.csv(paste(in_dir, "sample_clustering_and_summary/genes_used_for_clustering.txt", sep='/'), sep='\t', stringsAsFactors = F)
    # in_genes <- unlist(in_genes[,1])## the first column is the gene names
    sig_bool_file <- paste(in_dir,"sample_clustering_and_summary/significance/significant_and_enriched_boolean_table.tsv",sep='/')
    sig_enrich_bool_table<-read.csv(sig_bool_file,header=TRUE,sep="\t", stringsAsFactors=FALSE)
    cluster_table<-NULL
    for (i in seq(2,dim(sig_enrich_bool_table)[2])){
        temp_clust <- names(sig_enrich_bool_table)[i]
        #print(temp_clust)
        temp_genes <- unlist(sig_enrich_bool_table[which(sig_enrich_bool_table[,i]=="True"),1])
        #print(head(temp_genes))
        temp_hpa <- get_hpa(in_dir, method_name, dataset_name, in_genes = temp_genes)
        ## add the cluster label
        temp_hpa <- cbind(temp_hpa, rep(temp_clust,dim(temp_hpa)[1]))
        if (is.null(cluster_table)){
            cluster_table <- temp_hpa
        } else {
            cluster_table <- rbind(cluster_table, temp_hpa)
        }
    }
    return(cluster_table)
}


get_hpa<-function(in_dir, method_name, dataset_name, in_genes = NULL){
    if (is.null(in_genes)){
        in_genes <- get_pertinent_genes(in_dir)
    }
    message()
    message(method_name)
    message()
    ## original implementation was the background is genes in the dataset
    #bg <- unlist(read.csv(paste(in_dir, "ID_list.txt", sep='/'), header = F, sep='\t', stringsAsFactors = F))
    ############################################
    ## a better way to do it is to use the bg as whatever is actually expressed in the dataset as a bg
    exprs_sum_file<-paste(in_dir,"sample_clustering_and_summary/k_group_means.tsv",sep="/")
    exprs_sum_table<-read.csv(exprs_sum_file, header=TRUE, sep="\t")
    exprs_sum_mat<-as.matrix(exprs_sum_table[,-1])
    bg<-as.character(exprs_sum_table[rowSums(exprs_sum_mat)>0,1])
    ############################################
    #print(head(in_genes))
    #print(head(bg))
    gprof_res <- gost(in_genes, custom_bg=bg, significant = F, sources=c("HPA"))
    method <- rep(method_name, dim(gprof_res$result)[1])
    dataset <- rep(dataset_name, dim(gprof_res$result)[1])
    result_table <- cbind(gprof_res$result, method, dataset)
    #print(head(filter_for_panc(result_table)))
    return(result_table)
}


filter_for_panc<-function(result_table){
    return(result_table[grep("pancr",result_table$term_name),])
}


get_min_neg_log_p<-function(result_table){
    temp_table<-result_table[which(result_table$p_value == min(result_table$p_value)),]
    return(temp_table[1,])
}


get_best_panc_full<-function(result_table){
    all_datasets<-unique(unlist(result_table$dataset))
    all_meth <- unique(unlist(result_table$method))
    out<-NULL
    for (dataset in all_datasets){
        temp_dset_table<-result_table[which(as.character(unlist(result_table$dataset))==dataset),]
        temp_meth_out<-NULL
        for (temp_meth in all_meth){
            #message()
            #message(temp_meth)
            temp_dset_meth<-temp_dset_table[which(as.character(unlist(temp_dset_table$method))==temp_meth),]
            #message(temp_dset_meth[1,"method"])
            temp_best_line <- get_min_neg_log_p(filter_for_panc(temp_dset_meth))
            if (is.null(temp_meth_out)){
                temp_meth_out<-temp_best_line[1,]
            } else {
                temp_meth_out<-rbind(temp_meth_out, temp_best_line[1,])
            }
        }
        ranks<-rank(-log10(unlist(temp_meth_out$p_value)),ties.method='min')
        temp_meth_out<-cbind(temp_meth_out,ranks)
        #print(temp_meth_out[,c("method","p_value","ranks")])
        if (is.null(out)){
            out <- temp_meth_out
        } else {
            out <- rbind(out,temp_meth_out)
        }
    }
    return(out)
}


do_panc_enrichment<-function(real_datasets){
    significance_list<-list()
    rolling_out <- NULL
    for (dataset in real_datasets){
        print(dataset)
        base_dir <- dirname(dataset)
        dset_name <- basename(base_dir)
        ## get all of the method dirs
        all_methods <- list.dirs(path=base_dir, full.names = FALSE, recursive = FALSE)
        print(all_methods)
        for (method in all_methods){
            unique_id <- paste(dset_name, method,sep=".")
            print(unique_id)
            temp_meth_dir <- paste(base_dir, method, sep='/')
            print(temp_meth_dir)
            temp_hpa_res <- get_hpa(temp_meth_dir, method, dset_name)
            #temp_hpa_res <- get_sig_enrich_cluster_pathways(temp_meth_dir, method, dset_name)
            print(head(get_min_neg_log_p(filter_for_panc(temp_hpa_res))))
            message()
            message()
            if (is.null(rolling_out)){
                rolling_out <- temp_hpa_res
            } else {
                rolling_out <- rbind(rolling_out, temp_hpa_res)
            }
            #significance_list[[unique_id]] <- temp_hpa_res
        }
    }
    return(rolling_out)
}


do_panc_analysis<-function(best_panc,
                           ANALYSIS_OUT){
    out_img<-paste(ANALYSIS_OUT,"/pancreatic_feature_selection_%02d.png",sep="")
    png(filename = out_img, height=5, width =6 , units="in", res = 600)
    ############
    seaborn_colors = c("#1f77b4","#ff7f0e","#2ca02c","#d62728","#9467bd","#8c564b","#e377c2","#7f7f7f","#bcbd22","#17becf")
    sink(paste(c(ANALYSIS_OUT,"rank_p_val.txt"),collapse='/'))
    aov_res<-aov(ranks ~ method + dataset,data=best_panc)
    print(summary(aov_res))
    print(TukeyHSD(aov_res)$method)
    sink()
    print(ggplot(best_panc, aes(x=method, y=ranks, fill=method))+
          geom_boxplot()+
          theme_bw()+
          scale_fill_manual(breaks = best_panc$method,
                    values = seaborn_colors)+
          theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 10, colour = "black"))+
          theme(axis.text.y = element_text(hjust = 1, size = 10, colour = "black")))
    ############
    ############
    sink(paste(c(ANALYSIS_OUT,"precision.txt"),collapse='/'))
    aov_res<-aov(precision ~ method + dataset,data=best_panc)
    print(summary(aov_res))
    print(TukeyHSD(aov_res)$method)
    sink()
    print(ggplot(best_panc, aes(x=method, y=precision, fill=method))+
          geom_boxplot()+
          theme_bw()+
          scale_fill_manual(breaks = best_panc$method,
                    values = seaborn_colors)+
          theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 10, colour = "black"))+
          theme(axis.text.y = element_text(hjust = 1, size = 10, colour = "black")))
    ############
    ############
    sink(paste(c(ANALYSIS_OUT,"recall.txt"),collapse='/'))
    aov_res<-aov(recall ~ method + dataset,data=best_panc)
    print(summary(aov_res))
    print(TukeyHSD(aov_res)$method)
    sink()
    print(ggplot(best_panc, aes(x=method, y=recall, fill=method))+
          geom_boxplot()+
          theme_bw()+
          scale_fill_manual(breaks = best_panc$method,
                    values = seaborn_colors)+
          theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 10, colour = "black"))+
          theme(axis.text.y = element_text(hjust = 1, size = 10, colour = "black")))
    ############
    dev.off()
}


#################################################

do_pancreas(panc_datasets)
all_enrich <- do_panc_enrichment(panc_datasets)
best_panc<-get_best_panc_full(all_enrich)
do_panc_analysis(best_panc, ANALYSIS_OUT)

#print(best_panc[,c("method","p_value","ranks")])
#all_enrich2 <- do_panc_enrichment(panc_datasets)

