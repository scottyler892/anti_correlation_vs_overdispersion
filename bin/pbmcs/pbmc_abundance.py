import os
import numpy as np
import pandas as pd
import seaborn as sns
import textwrap
from math import ceil
from percent_max_diff.percent_max_diff import pmd
from matplotlib.colors import LinearSegmentedColormap
from statsmodels.stats.multitest import fdrcorrection as fdr
from sklearn.decomposition import PCA
from copy import deepcopy
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from sklearn.preprocessing import MinMaxScaler
from scipy.interpolate import make_interp_spline, BSpline
from shapely.geometry import Polygon
from alphashape import alphashape
try:
    import statsmodels.api as sm
except:
    import statsmodels as sm
else:
    print("somethings wrong with the statsmodels installation")


np.random.seed(123456)
out_dir = "data/pbmcs/"
#pbmcs = pd.read_csv("data/pbmcs/sample_clust_round1_cross_tab.tsv",sep="\t")

pbmcs = pd.read_csv("data/pbmcs/sample_clust_round1_cross_tab2.tsv",sep="\t")
pbmcs.index = pbmcs.iloc[:,0]
pbmcs = pbmcs.iloc[:,1:]


#samples = pbmcs.columns[1:].tolist()
samples = pbmcs.columns.tolist()
## 0 is control, 1 is T1D
disease = [1 if samples[i][0]=="D" else 0 for i in range(len(samples))]
#total_cells = pbmcs.iloc[:,1:].sum(0)
total_cells = pbmcs.sum(0)
sample_annotations = pd.DataFrame({"disease":disease,"total_cells":total_cells},index=samples)

#pbmc_pmd = pmd(pbmcs.iloc[:,1:])## first column is cluster number
pbmc_pmd = pmd(pbmcs)## first column is cluster number




def fdr_p(in_p):
    dummy, out_p = fdr(in_p)
    return(out_p)


def get_stats(z_scores_df):
    poisson_glm_statistic = []
    poisson_glm_p_val = []
    pmd_z_glm_statistic = []
    pmd_z_glm_p_val = []
    for i in range(pbmcs.shape[0]):
        print("\n\n\n\n",i)
        x = deepcopy(sample_annotations)
        data = sm.add_constant(x)
        ####
        #p_model = sm.GLM(pbmcs.iloc[i,1:], data, family=sm.families.Poisson())
        p_model = sm.GLM(pbmcs.iloc[i,:], data, family=sm.families.Poisson())
        p_res = p_model.fit()
        print("poisson:")
        print(p_res.summary())
        poisson_glm_statistic.append(p_res.tvalues["disease"])
        poisson_glm_p_val.append(p_res.pvalues["disease"])
        ####
        g_model = sm.GLM(z_scores_df.iloc[i,:], data, family=sm.families.Gaussian())
        g_res = g_model.fit()
        print("\npmd z-scores with Gaussian")
        print(g_res.summary())
        pmd_z_glm_statistic.append(g_res.tvalues["disease"])
        pmd_z_glm_p_val.append(g_res.pvalues["disease"])
        ####
    out_stats = pd.DataFrame(
        {"poisson_glm_statistic":poisson_glm_statistic,
        "poisson_glm_p_val":poisson_glm_p_val,
        "poisson_glm_p_val_BH":fdr_p(poisson_glm_p_val),
        "pmd_z_glm_statistic":pmd_z_glm_statistic,
        "pmd_z_glm_p_val":pmd_z_glm_p_val,
        "pmd_z_glm_p_val_BH":fdr_p(pmd_z_glm_p_val)}
        ,
        index=z_scores_df.index
    )
    return(out_stats)


out_stats = get_stats(pbmc_pmd.z_scores)


#ncomps = min(*pbmc_pmd.z_scores.shape)

pcs = PCA()#n_components = ncomps)
pcomps = pcs.fit(pbmc_pmd.z_scores)
pcomps.components_


sig_order = np.argsort(out_stats["pmd_z_glm_p_val_BH"])


disease_colors = ["black","cyan"]
sig_color = ["grey","red"]

sns.clustermap(1-pbmc_pmd.post_hoc, 
    row_colors = [disease_colors[row] for row in disease],
    col_colors = [disease_colors[row] for row in disease])
plt.savefig(os.path.join(out_dir,"one_minus_pmd2.png"),dpi=300)



sns.clustermap(pbmc_pmd.z_scores.iloc[sig_order,:],
    vmin=-30, 
    vmax=30,
    row_cluster = False,
    col_colors = [disease_colors[row] for row in disease],
    row_colors = [sig_color[p<0.05] for p in out_stats["pmd_z_glm_p_val_BH"][sig_order]])
plt.savefig(os.path.join(out_dir,"pmd_z_residuals2.png"),dpi=300)




plt.figure(figsize=(10, 6))  # Adjust as needed
plt.bar(np.arange(out_stats.shape[0]), -np.log10(out_stats["pmd_z_glm_p_val_BH"][sig_order]), color=[sig_color[p<0.05] for p in out_stats["pmd_z_glm_p_val_BH"][sig_order]])
plt.ylabel("-log10(BH-P-val)", fontsize=14)  # Adjust fontsize as needed
plt.xticks(fontsize=12)  # Adjust fontsize as needed
plt.yticks(fontsize=12)  # Adjust fontsize as needed
# Optional: If the labels on the x-axis are overlapping, rotate them
# plt.xticks(rotation=45)
plt.tight_layout()  
plt.savefig(os.path.join(out_dir,"pmd_z_neg_log_10_p_bar2.png"),dpi=300)



plt.figure(figsize=(10, 6))  # Adjust as needed
plt.bar(np.arange(out_stats.shape[0]), out_stats["pmd_z_glm_statistic"][sig_order], color=[sig_color[p<0.05] for p in out_stats["pmd_z_glm_p_val_BH"][sig_order]])
plt.plot([-1, out_stats.shape[0]], [0,0], color="black")
plt.ylabel("t-statistic", fontsize=14)  # Adjust fontsize as needed
plt.xticks(fontsize=12)  # Adjust fontsize as needed
plt.yticks(fontsize=12)  # Adjust fontsize as needed
# Optional: If the labels on the x-axis are overlapping, rotate them
# plt.xticks(rotation=45)
plt.tight_layout()  
plt.savefig(os.path.join(out_dir,"pmd_z_t_stat2.png"),dpi=300)



out_stats.to_csv(os.path.join(out_dir,"differential_abundance_significance2.tsv"),sep="\t")
pbmc_pmd.z_scores.to_csv(os.path.join(out_dir,"pmd_z_scores2.tsv"), sep = "\t")
pbmc_pmd.post_hoc.to_csv(os.path.join(out_dir,"pmd_posthoc2.tsv"), sep = "\t")



##################################################################################

cluster_key="cluster_manual"
pbmc_data=pd.read_csv("data/pbmcs/cell_annotations_with_clusters_and_spring2.tsv",sep="\t")
pbmc_data[cluster_key] = pd.Series(pbmc_data[cluster_key],dtype="category")
########
## Add the significance
out_stats = pd.read_csv(os.path.join(out_dir,"differential_abundance_significance2.tsv"),sep="\t")
pbmc_data["cluster_disease_sig"]=np.nan
for i in range(out_stats.shape[0]):
    cur_clust_id = out_stats[cluster_key][i]
    cur_p = out_stats["pmd_z_glm_p_val_BH"][i]
    print(cur_clust_id, cur_p)
    pbmc_data.loc[pbmc_data[cluster_key]==cur_clust_id,"cluster_disease_sig"]=-np.log10(cur_p)


sig_clusts = out_stats[cluster_key][out_stats["pmd_z_glm_p_val_BH"]<0.05].tolist()
########
pbmc_data_orig = pbmc_data
pbmc_data = pbmc_data.dropna()
disease = np.array(["Ctrl" for i in range(pbmc_data.shape[0])])
disease[pbmc_data["sample"].str.startswith("D_")]="T1D"
pbmc_data["disease"] = disease

#################
xmin = pbmc_data["spring_1"].min()
xmax = pbmc_data["spring_1"].max()
xbuff = 0.05*(xmax-xmin)
xmin -= xbuff
xmax += xbuff
##
ymin = pbmc_data["spring_2"].min()
ymax = pbmc_data["spring_2"].max()
ybuff = 0.05*(ymax-ymin)
ymin -= ybuff
ymax += ybuff
################



def merge_palettes(palette1, palette2, n_colors):
    palette1_colors = sns.color_palette(palette1, n_colors)
    palette2_colors = sns.color_palette(palette2, n_colors)[::-1]
    merged_palette = []
    for color1, color2 in zip(palette1_colors, palette2_colors):
        merged_palette.extend([color1, color2])
    return merged_palette


n_colors = ceil(len(set(pbmc_data[cluster_key]))/2)+1  # Adjust as needed
merged_palette = merge_palettes("bright", "colorblind", n_colors)
np.random.seed(12345)
np.random.shuffle(merged_palette)
sns.set_palette(merged_palette)
categories = pbmc_data[cluster_key].unique()
color_dict = dict(zip(categories, merged_palette))
global_color_dict = deepcopy(color_dict)


plt.clf()
plt.figure(figsize=(7, 6))  # Adjust as needed
sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_data,s=.38, edgecolor = None, palette=color_dict)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.01, 1),
       borderaxespad=0)
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_clusters2.png"),dpi=300)





plt.clf()
sns.scatterplot(x="spring_1",y="spring_2",hue="sample",data=pbmc_data,s=.38,edgecolor = None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.01, 1),
       borderaxespad=0)
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_samples2.png"),dpi=300)




plt.clf()
sns.scatterplot(x="spring_1",y="spring_2",hue="sample",data=pbmc_data[pbmc_data["disease"]=="T1D"],s=.38,edgecolor = None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.01, 1),
       borderaxespad=0)
plt.title("T1D samples")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_T1D_samples2.png"),dpi=300)



plt.clf()
sns.scatterplot(x="spring_1",y="spring_2",hue="sample",data=pbmc_data[pbmc_data["disease"]=="Ctrl"],s=.38,edgecolor = None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.01, 1),
       borderaxespad=0)
plt.title("Control samples")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_Ctrl_samples2.png"),dpi=300)


plt.clf()
sns.scatterplot(x="spring_1",y="spring_2",hue="disease",data=pbmc_data,s=.38,edgecolor = None,
    palette = sns.color_palette(disease_colors[::-1]),legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# plt.legend(bbox_to_anchor=(1.01, 1),
#        borderaxespad=0)
plt.title("Disease status")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_disease_status2.png"),dpi=300)




plt.clf()
###############
colors = [(.85, .85, .85), (.85, 0, 0), (.85, 0, 0), (.875, 0, 0), (.900, 0, 0), (.925, 0, 0), (.95, 0, 0),(.975, 0, 0), (1, 0, 0)] 
cmap = LinearSegmentedColormap.from_list("grey_red", colors, N=360)
sns.scatterplot(x="spring_1",y="spring_2",hue="cluster_disease_sig",data=pbmc_data,s=.38,edgecolor = None,
    palette = cmap,
    legend=False)#sns.color_palette(["grey","red"]))
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# plt.legend(bbox_to_anchor=(1.01, 1),
#        borderaxespad=0)
plt.title("Cluster-level disease significance")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_clust_significance2.png"),dpi=300)


plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
sns.scatterplot(x="spring_1",y="spring_2",hue="disease",data=pbmc_data[pbmc_data[cluster_key].isin(sig_clusts)],s=.38,edgecolor = None,
    palette = sns.color_palette(disease_colors[::-1]),legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# plt.legend(bbox_to_anchor=(1.01, 1),
#        borderaxespad=0)
plt.title("Significantly different abundance")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_disease_status_in_SigClusts2.png"),dpi=300)



plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_data[pbmc_data[cluster_key].isin(sig_clusts)],s=.38,edgecolor = None,
    legend=None,
    palette=color_dict)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# plt.legend(bbox_to_anchor=(1.01, 1),
#        borderaxespad=0)
plt.title("Significantly different abundance")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"spring_disease_clusters_in_SigClusts2.png"),dpi=300)





###############################################
## plot the classical monocytes comparisons
plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
mono_clusts = ["5,8","10","19","20"]#
pbmc_mono = pbmc_data[pbmc_data[cluster_key].isin(mono_clusts)]
scatter = sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_mono,s=.38,edgecolor = None,
    palette=color_dict)#, legend=None)
# Get the handles and labels for all items in legend
handles, labels = scatter.get_legend_handles_labels()
# Identify the indices of the labels that match with mono_clusts
label_indices = [labels.index(l) for l in mono_clusts if l in labels]
# Subselect only relevant elements for legend
relevant_handles = [handles[i] for i in label_indices]
relevant_labels = [labels[i] for i in label_indices]
plt.legend(relevant_handles, relevant_labels, bbox_to_anchor=(1.26, 1),
       borderaxespad=0)#,box_to_anchor=(1.26, 1))
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.title("All Classical Monocytes")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_classical_monocytes.png"),dpi=300)




mono_clusts = ["5,8","10","19","20","17"]#
pbmc_mono = pbmc_data[pbmc_data[cluster_key].isin(mono_clusts)]
scatter = sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_mono,s=.38,edgecolor = None,
    palette=color_dict)#, legend=None)
# Get the handles and labels for all items in legend
handles, labels = scatter.get_legend_handles_labels()
# Identify the indices of the labels that match with mono_clusts
label_indices = [labels.index(l) for l in mono_clusts if l in labels]
# Subselect only relevant elements for legend
relevant_handles = [handles[i] for i in label_indices]
relevant_labels = [labels[i] for i in label_indices]
plt.legend(relevant_handles, relevant_labels, bbox_to_anchor=(1.26, 1),
       borderaxespad=0)#,box_to_anchor=(1.26, 1))
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.title("All Classical Monocytes")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_monocytes.png"),dpi=300)




plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
sns.scatterplot(x="spring_1",y="spring_2",hue="disease",data=pbmc_data[pbmc_data[cluster_key].isin(mono_clusts)],s=.38,edgecolor = None,
    palette = sns.color_palette(disease_colors[::-1]),legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# plt.legend(bbox_to_anchor=(1.01, 1),
#        borderaxespad=0)
plt.title("All Classical Monocytes by disease status")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_classical_monocytes_disease_status.png"),dpi=300)


###############################################
## plot the cd4em comparisons
plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
cd4em_clusts = ["12,21","13,23,25"]
sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_data[pbmc_data[cluster_key].isin(cd4em_clusts)],s=.38,edgecolor = None,
    palette=color_dict)#legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.26, 1),
       borderaxespad=0)
plt.title("CD4em")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_CD4em.png"),dpi=300)




plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
sns.scatterplot(x="spring_1",y="spring_2",hue="disease",data=pbmc_data[pbmc_data[cluster_key].isin(cd4em_clusts)],s=.38,edgecolor = None,
    palette = sns.color_palette(disease_colors[::-1]),legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# plt.legend(bbox_to_anchor=(1.01, 1),
#        borderaxespad=0)
plt.title("All CD4em clusters by disease status")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_cd4em_disease_status.png"),dpi=300)



################################################################

def get_smooth_polygon(points, s=1.0):
    # Unpack the points
    x, y = points[:,0], points[:,1]
    # Repeat the first point to create a 'closed loop'
    x = np.r_[x, x[0]]
    y = np.r_[y, y[0]]
    # Create parameterized values for the points
    l = np.arange(x.shape[0])
    # Create a cubic spline
    spl_x = make_interp_spline(l, x, k=3)
    spl_y = make_interp_spline(l, y, k=3)
    # Create new parameterized values
    l_new = np.linspace(0, l.max(), 500)
    # Create smoothed data points
    x_smooth = spl_x(l_new)
    y_smooth = spl_y(l_new)
    # Plot the smoothed polygon
    #ax.plot(x_smooth, y_smooth, 'r--')
    out_poly=np.zeros((x_smooth.shape[0],2))
    out_poly[:,0]=x_smooth
    out_poly[:,1]=y_smooth
    return(out_poly)




def get_polygon(xy_data, alpha=-1., edgecolor='r'):
    local_alpha_shape = alphashape(xy_data, alpha)
    polygon_coords = np.array(local_alpha_shape.exterior.coords)
    #polygon_coords = get_smooth_polygon(polygon_coords)
    polygon = patches.Polygon(polygon_coords, fill=False, edgecolor=edgecolor, linewidth=2, linestyle='dashed', alpha=1)
    #local_ax.add_patch(polygon)
    return(polygon)



def get_all_polygon_dict(pbmc_data, 
                         color_dict, 
                         x_coord_key="spring_1", 
                         y_coord_key="spring_2", 
                         clust_key="cluster_manual"):
    out_polygon_dict = {}
    for clust in set(pbmc_data[clust_key]):
        print("getting polygon for:",clust)
        xy_data = pbmc_data[pbmc_data[clust_key]==clust][[x_coord_key, y_coord_key]].to_numpy()
        print(xy_data)
        out_polygon_dict[clust]={}
        out_polygon_dict[clust]["poly"]=get_polygon(xy_data,edgecolor=color_dict[clust])
    return(out_polygon_dict)


all_polygons = get_all_polygon_dict(pbmc_data, color_dict)
plt.close("all")
plt.clf()
fig, ax = plt.subplots()
ax.scatter(pbmc_data[pbmc_data[cluster_key]=="13,23,25"]["spring_1"],pbmc_data[pbmc_data[cluster_key]=="13,23,25"]["spring_2"])
ax.add_patch(deepcopy(all_polygons["13,23,25"]["poly"]))
ax.set_ylim(-1,1)
ax.set_xlim(-1,1)
plt.show()








def plot_gene_in_mono_subset(adata, pbmc_data, genes = ["IKZF1"], close_mono_cells = ["5,8","10"], cell_subset_label = "close_monocytes", gene_alias={},
                             x_key="spring_1",y_key="spring_2",
                             cluster_key="cluster_manual",
                             color_dict={}):
    for g in genes:
        if g not in gene_alias:
            gene_alias[g]=g
    colors = [(.85, .85, .85),(.85, .5, .55),(1, 0, 0)] 
    cmap = LinearSegmentedColormap.from_list("grey_red", colors, N=360)
    plot_cells = adata.obs.index.isin(pbmc_data["cell"])
    try:
        exprs = np.log2(1+adata[plot_cells,genes].X.todense())
    except:
        exprs = np.array(np.log2(1+adata[plot_cells,genes].X).squeeze())
    scaler = MinMaxScaler() # Initialize a new MinMaxScaler object
    for g in range(len(genes)):
        if genes[g] not in pbmc_data.columns:
            pbmc_data[genes[g]] = exprs[:,g]
            # Compute the normalized values
            pbmc_data[genes[g] + '_norm'] = scaler.fit_transform(pbmc_data[genes[g]].values.reshape(-1,1))
            # Compute the color for each data point
            pbmc_data[genes[g] + '_color'] = [cmap(x) for x in pbmc_data[genes[g] + '_norm']]
    fig, axs = plt.subplots(2, len(genes), figsize=(5 * len(genes), 10)) # 10 is arbitrary, adjust as needed
    # If only one gene is provided, axs will not be a list.
    axs = np.ravel([axs])
    print(pbmc_data[cluster_key])
    temp_data = deepcopy(pbmc_data[pbmc_data[cluster_key].isin(close_mono_cells)])
    print(temp_data)
    temp_data[cluster_key]=temp_data[cluster_key].cat.remove_unused_categories()
    for i, gene in enumerate(genes):
        # Scatter plots
        axs[i].scatter(pbmc_data[x_key][pbmc_data[cluster_key].isin(close_mono_cells)], 
                       pbmc_data[y_key][pbmc_data[cluster_key].isin(close_mono_cells)], 
                       s=.38, color=pbmc_data[gene + '_color'][pbmc_data[cluster_key].isin(close_mono_cells)], 
                       edgecolor=None)
        # for clust in close_mono_cells:
        #     axs[i].add_patch(deepcopy(all_polygons[clust]["poly"]))
        axs[i].set_xlim(xmin, xmax)
        axs[i].set_ylim(ymin, ymax)
        axs[i].set_title(gene_alias[gene], fontsize='xx-large')  # Set the title as gene name
        axs[i].set_xlabel('')  # Hide x-axis label
        axs[i].set_ylabel('')  # Hide y-axis label
        if i==0:
            axs[i].set_ylabel('Spring embedding', fontsize='xx-large')  # Set y-axis label
        axs[i].set_xticks([])  # Hide x ticks
        axs[i].set_yticks([])  # Hide y ticks
        for spine in axs[i].spines.values():  # Add weight to borders
            spine.set_linewidth(2)
        # Violin plots
        print(temp_data)
        vp = sns.violinplot(x=cluster_key, y=gene, data=temp_data, 
                       palette=color_dict, ax=axs[i + len(genes)], linewidth=2)
        x_labels = vp.get_xticklabels()
        wrapped_labels = [textwrap.fill(label.get_text(), 8) for label in x_labels]
        vp.set_xticklabels(wrapped_labels)
        axs[i + len(genes)].set_xlabel('')  # Hide x-axis label
        axs[i + len(genes)].set_ylabel('')  # Hide y-axis label
        axs[i + len(genes)].set_title("")  # Set the title as gene name
        if i == 0:
            axs[i + len(genes)].set_ylabel('log2 RLE normalized counts', fontsize='xx-large')  # Set y-axis label
        for spine in axs[i + len(genes)].spines.values():  # Add weight to borders
            spine.set_linewidth(2)
        # Increase the thickness of the boxplots inside the violins
        for l in vp.lines:
            l.set_linewidth(7)
        # Increase the size of the median marker
        num_grps = len(close_mono_cells)
        num_cols = len(vp.collections)
        for col in range(num_grps):
            try:
                vp.collections[(col*2)+1].set_sizes([30])  # Increase size of median marker. Adjust index if needed.
            except:
                print("error in setting dot size")
        # Increase the size of the y-axis ticks
        axs[i + len(genes)].tick_params(axis='y', labelsize='xx-large')
        axs[i + len(genes)].tick_params(axis='x', labelsize='xx-large')
    plt.tight_layout()
    plt.savefig(os.path.join(out_dir, "_".join(genes) + "_" + cell_subset_label + ".png"), dpi=300)
    plt.close("all")



mono_clusts = ["5,8","10","19","20"]
plot_gene_in_mono_subset(
    adata,
    pbmc_data,
    genes=["CD14","FCGR3A", "CCR2","CSF1R"],
    close_mono_cells = mono_clusts, 
    cell_subset_label = "all_monocytes",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFSF13B":"BAFF",
                "TNFSF13":"APRIL",
                "TNFRSF13C":"CD268",#resting/mature
                "SPN":"CD43",
                "NT5E":"CD73",
                "FCER2":"CD23",
                "LILRB1":"CD85J",
                "CD38":"ADPRC1",
                "CD86":"B7-2",
                "MS4A1":"CD20",
                "SDC1":"CD138",
                "FCGR3A":"CD16",
                "CSF1R":"CD115"},
    cluster_key="cluster_manual",
    color_dict=global_color_dict
    )




plot_gene_in_mono_subset(
    adata,
    pbmc_data,
    genes=["NAMPT","HIF1A","FOSB","JAK2","RIPOR2"],
    )


###############################################
b_cells = ["11","2","15","16"]

plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_data[pbmc_data[cluster_key].isin(b_cells)],s=.38,edgecolor = None,
    palette=color_dict)#legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
plt.legend(bbox_to_anchor=(1.26, 1),
       borderaxespad=0)
plt.title("B-cells")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_B.png"),dpi=300)



plot_gene_in_mono_subset(
    adata, 
    pbmc_data, 
    genes = ["CD79A","IGHD","IGHM","TNFRSF13B","CD27","MS4A1", "TOX",
             "PRDM1","XBP1","CD38",
             "TNFRSF13C", "CD69","CD80","SPN",
             #"FAS","FCER2","CD86",
             "NFKB1","NFKB2",
             ], #"CD5","ITGA6", subpopulation
    close_mono_cells = ["11","2","15","16"], 
    cell_subset_label = "B_cells",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFSF13B":"BAFF",
                "TNFSF13":"APRIL",
                "TNFRSF13C":"CD268",#resting/mature
                "SPN":"CD43",
                "NT5E":"CD73",
                "FCER2":"CD23",
                "LILRB1":"CD85J",
                "CD38":"ADPRC1",
                "CD86":"B7-2",
                "MS4A1":"CD20",
                "SDC1":"CD138"
                })




##########################
## FINAL
plot_gene_in_mono_subset(
    adata, 
    pbmc_data, 
    genes = ["CD19","CD79A",# B-cells
             "PRDM1","XBP1",# plasma
             "IGHD","IGHM",#IgD/M
             "NFKB1",#NFKB high
             "FCER2",# CD23
             "TOX",# Tox covers most of B1
             "TNFRSF13B","CD80",# B1a
             "CD5","ITGA6"#B1b
             # ,"TNFRSF13B","CD27","MS4A1", 
             # ,"CD38",
             # "TNFRSF13C", "CD69","CD80","SPN",
             #"FAS","FCER2","CD86",
             
             ], #"CD5","ITGA6", subpopulation
    close_mono_cells = ["11","2","15","16"], 
    cell_subset_label = "B_cells",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFSF13B":"BAFF",
                "TNFSF13":"APRIL",
                "TNFRSF13C":"CD268",#resting/mature
                "SPN":"CD43",
                "NT5E":"CD73",
                "FCER2":"CD23",
                "LILRB1":"CD85J",
                "CD38":"ADPRC1",
                "CD86":"B7-2",
                "MS4A1":"CD20",
                "SDC1":"CD138"
                })





###############################################
t_cells = ["0","13,23,25","17","12,21","9,6"]

plt.clf()
#sig_clusts = pd.Series([8,5,13,12,9],dtype="category")
scatter = sns.scatterplot(x="spring_1",y="spring_2",hue=cluster_key,data=pbmc_data[pbmc_data[cluster_key].isin(t_cells)],s=.38,edgecolor = None,
    palette=global_color_dict)#legend=None)
plt.xlim(xmin,xmax)
plt.ylim(ymin,ymax)
# Get the handles and labels for all items in legend
handles, labels = scatter.get_legend_handles_labels()
# Identify the indices of the labels that match with mono_clusts
label_indices = [labels.index(l) for l in t_cells if l in labels]
# Subselect only relevant elements for legend
relevant_handles = [handles[i] for i in label_indices]
relevant_labels = [labels[i] for i in label_indices]
plt.legend(relevant_handles, relevant_labels, bbox_to_anchor=(1.26, 1),
       borderaxespad=0)#,box_to_anchor=(1.26, 1))
# plt.legend(bbox_to_anchor=(1.30, 1),
#        borderaxespad=0)
plt.title("T-cells")
plt.tight_layout()
plt.savefig(os.path.join(out_dir,"all_T.png"),dpi=300)



plot_gene_in_mono_subset(
    adata, 
    pbmc_data, 
    genes = ["CD28","CD3E","CD4","CD8A","GATA3","TOX","PRDM1","SELL","PECAM1","FOS","ADGRE5"], 
    close_mono_cells = t_cells, 
    cell_subset_label = "T_cells",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97"},
    color_dict = global_color_dict)#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/





plot_gene_in_mono_subset(
    adata, 
    pbmc_data, 
    genes = ["CD28","CD3E","CD4","CD8A","PRDM1","TOX","LEF1","FOS","FOSB","NFKB1","ADGRE5","HLA-DRB1"], 
    close_mono_cells = t_cells, 
    cell_subset_label = "T_cells",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97"})#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/



cd4t_cells = ["13,23,25","12,21"]# 12,21 is CD4cm, 13,23,25 is naive CD4

plot_gene_in_mono_subset(
    adata, 
    pbmc_data, 
    genes = ["RORA","ITGB1","PRDM1","CCR7","PECAM1","SELL", "TCF7"], # PECAM1 marks naieve and RTE ( recent thymic emigrants (RTEs))
    close_mono_cells = cd4t_cells, 
    cell_subset_label = "CD4_T_cells",
    gene_alias={"PRDM1":"BLIMP1",
                "TNFRSF17":"BCMA",
                "TNFRSF13B":"TACI",
                "TNFRSF13C":"CD268",#resting/mature
                "PTGDR2":"CRTH2",
                "ADGRE5":"CD97",
                "ITGB1":"CD29"})#early activation https://pubmed.ncbi.nlm.nih.gov/21618833/




