# README #

These are the scripts for comparing anti-correlation genes vs overdispersed genes

### How do I get set up? ###

* The R dependencies are listed in the R_requirements.txt file, and the python dependencies are listed in the python3_requirements.txt file
* Once all dependencies are installed the full benchmark should be executable by running `Rscript run_full_benchmark.R` or `R CMD BATCH run_full_benchmark.R`
* This benchmark was run with R version 3.6.0 (2019-04-26) -- "Planting of a Tree"

The R packages and versions used were:
```
scater_1.12.2
scde_2.12.0
lattice_0.20-38
numDeriv_2016.8-1.1
scran_1.12.1
monocle_2.12.0
irlba_2.3.3
Matrix_1.3-2
SingleCellExperiment_1.8.0
DelayedArray_0.10.0
matrixStats_0.61.0
GenomeInfoDb_1.22.1
S4Vectors_0.24.4
dplyr_1.0.8
gplots_3.1.1
networkBMA_2.24.0
RcppArmadillo_0.10.8.1.0
BMA_3.18.14
inline_0.3.17
leaps_3.1
stringr_1.4.0
umap_0.2.7.0
NMF_0.23.0
BiocGenerics_0.32.0
rngtools_1.5
registry_0.5-1
zeallot_0.1.0
kBET_0.99.6
flexmix_2.3-17
M3Drop_1.10.0
magrittr_2.0.2
Seurat_3.2.0
DDRTree_0.1.5
VGAM_1.1-5
splatter_1.8.0
SummarizedExperiment_1.14.1
BiocParallel_1.18.1
GenomicRanges_1.38.0
IRanges_2.20.2
gprofiler2_0.2.0
RColorBrewer_1.1-2
ggplot2_3.3.5
RcppEigen_0.3.3.9.1
Rcpp_1.0.8
rrcov_1.5-5
robustbase_0.93-9
survival_3.2-11
argparse_2.0.3
tsne_0.1-3
Biobase_2.44.0
cluster_2.0.8
pkgmaker_0.32.2
entropy_1.3.0
```

Python dependencies are (which versions of the specified packages should not matter):
```
bio_pyminer==0.10.4
bio_pyminer_norm==0.2.4
anticor_features==0.1.9
rpy2
numpy
pandas
scipy
matplotlib
seaborn
pygraphviz
```

### Who do I talk to? ###

* scottyler89+bitbucket@gmail.com
