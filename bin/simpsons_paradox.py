import os
import h5py
import numpy as np
import pandas as pd
import scanpy as sc
from scipy.stats import spearmanr
from anticor_features.anticor_features import get_anti_cor_genes
from anticor_features.anticor_features import dense_rank
from statsmodels.nonparametric.smoothers_lowess import lowess
from sklearn.neighbors import RadiusNeighborsRegressor as neighbors
from scipy.interpolate import interp1d


simpsons_dir = "data/simpsons_paradox/"
adata = sc.read("data/simpsons_paradox/TS_Muscle_TSP2.h5ad")

tenx_dir = "data/simpsons_paradox/tenx/"
smrt_dir = "data/simpsons_paradox/smrt/"
both_dir = "data/simpsons_paradox/both/"

for temp_dir in [tenx_dir, smrt_dir, both_dir]:
    if not os.path.isdir(temp_dir):
        os.mkdir(temp_dir)


ten_x_df = get_anti_cor_genes(
    adata[adata.obs["method"]=="10X"].X.T,
    adata.var.index.tolist(),
    species="hsapiens",
    scratch_dir = tenx_dir)


smrt_df = get_anti_cor_genes(
    adata[adata.obs["method"]=="smartseq2"].X.T,
    adata.var.index.tolist(),
    species="hsapiens",
    scratch_dir = smrt_dir)


both_df = get_anti_cor_genes(
    adata.X.T,
    adata.var.index.tolist(),
    species="hsapiens",
    scratch_dir = both_dir)




def get_tested_and_selected(in_table):
    all_tested = in_table.dropna().index.tolist()
    all_selected = in_table[in_table["selected"] == True].index.tolist()
    return(all_tested, all_selected)



tenx_tested, tenx_selected = get_tested_and_selected(ten_x_df)
smrt_tested, smrt_selected = get_tested_and_selected(smrt_df)
both_tested, both_selected = get_tested_and_selected(both_df)


tested_in_all = [g for g in smrt_tested if (g in tenx_tested and g in both_tested)]




def comparisons_to_df(comparisons, idx_df):
    gene_1 = []
    gene_2 = []
    idx1, idx2 = np.where(comparisons==True)
    print(idx1, idx2)
    gene_1 = idx_df.iloc[idx1,:]["gene"].tolist()
    gene_2 = idx_df.iloc[idx2,:]["gene"].tolist()
    simpsons_paradox_df = pd.DataFrame({"gene_1":gene_1,
                                        "gene_2":gene_2})
    return(simpsons_paradox_df)


def find_simpson(tenx_dir, smrt_dir, both_dir,
                 tenx_tested, smrt_tested, both_tested,
                 tested_in_all):
    tenx_tested_hash = {val:key for key, val in enumerate(tenx_tested)}
    smrt_tested_hash = {val:key for key, val in enumerate(smrt_tested)}
    both_tested_hash = {val:key for key, val in enumerate(both_tested)}
    idx_df = pd.DataFrame({"gene":tested_in_all,
                  "tenx_idx": [tenx_tested_hash[g] for g in tested_in_all],
                  "smrt_idx": [smrt_tested_hash[g] for g in tested_in_all],
                  "both_idx": [both_tested_hash[g] for g in tested_in_all]
                  })
    ##
    print("reading correlations in")
    tenx_cor = h5py.File(os.path.join(tenx_dir, "spearman.hdf5"),"r")
    smrt_cor = h5py.File(os.path.join(smrt_dir, "spearman.hdf5"),"r")
    both_cor = h5py.File(os.path.join(both_dir, "spearman.hdf5"),"r")
    ##
    print("subsetting for unity")
    print(tenx_cor["infile"].shape)
    print(smrt_cor["infile"].shape)
    print(both_cor["infile"].shape)
    tenx_cor_sub = tenx_cor["infile"][idx_df["tenx_idx"].tolist(),:]
    tenx_cor_sub = tenx_cor_sub[:,idx_df["tenx_idx"].tolist()]
    smrt_cor_sub = smrt_cor["infile"][idx_df["smrt_idx"].tolist(),:]
    smrt_cor_sub = smrt_cor_sub[:,idx_df["smrt_idx"].tolist()]
    both_cor_sub = both_cor["infile"][idx_df["both_idx"].tolist(),:]
    both_cor_sub = both_cor_sub[:,idx_df["both_idx"].tolist()]
    ##
    ## Now compare the correlations
    print("comparing")
    tenx_neg_bool = np.array(tenx_cor_sub<=-0.05, dtype=int)
    smrt_neg_bool = np.array(smrt_cor_sub<=-0.05, dtype = int)
    both_pos_bool = np.array(both_cor_sub>0.05, dtype = int)
    candidate_comparisons = np.array((tenx_neg_bool+smrt_neg_bool+both_pos_bool)==3)
    print(np.sum(candidate_comparisons)/2,"candidates")
    #############################
    simpsons_df = comparisons_to_df(candidate_comparisons, idx_df)
    #############################
    ## remember to close them
    tenx_cor.close()
    smrt_cor.close()
    both_cor.close()
    return simpsons_df


simpsons_df = find_simpson(tenx_dir, smrt_dir, both_dir,
                 tenx_tested, smrt_tested, both_tested,
                 tested_in_all)



plot_df = pd.DataFrame({"HSPA1A":adata[:,"HSPA1A"].X.toarray().flatten(),"TPT1":adata[:,"TPT1"].X.toarray().flatten(),"method":adata.obs["method"].tolist()})
tenx_plot_df = plot_df[plot_df["method"]=="10X"]
smrt_plot_df = plot_df[plot_df["method"]=="smartseq2"]

def add_lowess(y, x, clip_top = 0.95):
    sample_size = x.shape[0]
    x = x+np.random.normal(0,0.01,x.shape)
    y = y+np.random.normal(0,0.01,x.shape)
    print(np.max(x), np.max(y))
    lowess_estimates_sample = lowess(x+1,y+1, frac=0.666, delta = 0.05*np.max(x+1))-1
    #lowess_estimates_sample = lowess(x+1,y+1, frac=.666, delta = 0)-1
    lowess_estimates_sample[np.isnan(lowess_estimates_sample)]=0
    values, indices = np.unique(lowess_estimates_sample[:,0], return_index=True)
    print(lowess_estimates_sample)
    lowess_estimates_sample = np.array(lowess_estimates_sample[indices,:])
    #make the new x values evenly spaced a
    temp_min = min(lowess_estimates_sample[:,0])# - epsilon
    temp_max = max(lowess_estimates_sample[:,0])*clip_top# + epsilon
    new_x = np.linspace(temp_min,temp_max, num=sample_size, endpoint = False)
    interpolation = interp1d(lowess_estimates_sample[:,0], lowess_estimates_sample[:,1], kind='cubic')
    print("new_min_max:",min(new_x),max(new_x))
    print(new_x)
    ## interpolate the lowess function
    new_y = interpolation(new_x)
    print(new_x,new_y)
    ##
    ## get the interplated lowess ready for nearest neighbor regression
    train_x = np.array([[temp_x] for temp_x in lowess_estimates_sample[:,0].tolist()])
    train_y = lowess_estimates_sample[:,1]
    ##
    full_x = np.array([[temp_x] for temp_x in x.tolist()])
    #train_x = full_x[sample]
    #train_y = variance[sample]
    print(train_x)
    print(train_y)
    #neigh.fit(train_x,train_y)
    print(new_x)
    print(new_y)
    new_x = np.array([[x] for x in new_x.tolist()])
    ##
    #plt.clf()
    plt.scatter(new_x,new_y, c = 'black', s = 0.75)
    return()


plt.clf()
r, p = spearmanr(tenx_plot_df["HSPA1A"], tenx_plot_df["TPT1"])
sns.scatterplot(x=tenx_plot_df["HSPA1A"],y=tenx_plot_df["TPT1"],c=[sns.color_palette("tab10")[0]]*tenx_plot_df.shape[0],s=5)
add_lowess(tenx_plot_df["HSPA1A"],tenx_plot_df["TPT1"])
plt.title(f"r = {r:.3f}\np = {p:.2e}")
plt.savefig(os.path.join(simpsons_dir,"tenx_simpsons.png"),dpi=300)

plt.clf()
r, p = spearmanr(smrt_plot_df["HSPA1A"], smrt_plot_df["TPT1"])
sns.scatterplot(x=smrt_plot_df["HSPA1A"],y=smrt_plot_df["TPT1"],c=[sns.color_palette("tab10")[1]]*smrt_plot_df.shape[0],s=5)
add_lowess(smrt_plot_df["HSPA1A"],smrt_plot_df["TPT1"])
plt.title(f"r = {r:.3f}\np = {p:.2e}")
plt.savefig(os.path.join(simpsons_dir,"smrt_simpsons.png"),dpi=300)

plt.clf()
r, p = spearmanr(plot_df["HSPA1A"], plot_df["TPT1"])
sns.scatterplot(x=plot_df["HSPA1A"],y=plot_df["TPT1"],hue="method",data=plot_df,s=5)
add_lowess(plot_df["HSPA1A"],plot_df["TPT1"])
plt.title(f"r = {r:.3f}\np = {p:.2e}")
plt.savefig(os.path.join(simpsons_dir,"both_simpsons.png"),dpi=300)



