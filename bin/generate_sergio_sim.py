#!/usr/env python3
import os
import random
import time
import numpy as np
import networkx as nx
import pandas as pd
import seaborn as sns
import argparse
import os
import sys
from math import ceil as ceiling
from matplotlib import pyplot as plt
from copy import deepcopy
from scipy import stats
########################

def make_file(contents, path):
    file_handle = open(path, 'w')
    if isinstance(contents, list):
        file_handle.writelines(contents)
    elif isinstance(contents, str):
        file_handle.write(contents)
    file_handle.close()

def flatten_2D_table(table, delim):
    # print(type(table))
    if str(type(table)) == "<class 'numpy.ndarray'>":
        out = []
        for i in range(0, len(table)):
            out.append([])
            for j in range(0, len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i] = delim.join(out[i]) + '\n'
        return out
    else:
        for i in range(0, len(table)):
            for j in range(0, len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j] = str(table[i][j])
            table[i] = delim.join(table[i]) + '\n'
        # print(table[0])
        return table


def write_table(table, out_file, sep='\t'):
    make_file(flatten_2D_table(table, sep), out_file)

############################################################
def load_sergio(sergio_dir):
    print("loading SERGIO")
    PARENT_PACKAGE = "SERGIO"
    SCRIPT_DIR = sergio_dir#os.path.join(sergio_dir,"SERGIO")
    #print(sys.path)
    sys.path.append(os.path.normpath(SCRIPT_DIR))
    #print(sys.path)
    from SERGIO import sergio
    #print(dir(sergio))
    return(sergio)


def lin_norm_rows(in_mat):
    min_vect = np.min(in_mat,axis=1)
    for i in range(in_mat.shape[0]):
        in_mat[i,:]-=min_vect[i]
    max_vect = np.min(in_mat,axis=1)
    for i in range(in_mat.shape[0]):
        in_mat[i,:]-=max_vect[i]
    return(in_mat)



def add_random_genes(sim_count_mat, num_rand_genes):
    # sns.heatmap(sim_count_mat)
    # plt.show()
    print("adding the random genes")
    print("\tsim_mat shape:",sim_count_mat.shape)
    num_cells = sim_count_mat.shape[1]
    final_out_mat = np.zeros((sim_count_mat.shape[0]+num_rand_genes,num_cells))
    final_out_mat[:sim_count_mat.shape[0],:]+=sim_count_mat
    for rand_gene_idx in range(sim_count_mat.shape[0],final_out_mat.shape[0]):
        temp_vect = np.random.negative_binomial(0.2-np.abs(np.random.rand()/10),np.abs(np.random.rand()/50),size=num_cells)
        temp_vect=temp_vect/np.max(temp_vect)*np.max(sim_count_mat)
        final_out_mat[rand_gene_idx,:]+=temp_vect
    print()
    #sns.heatmap(final_out_mat)
    #plt.show()
    return(final_out_mat)


def get_cell_anno_vect(in_3d):
    cell_annos = []
    total_cell_count = 0
    for i in range(len(in_3d)):
        subset_mat = in_3d[i]
        for j in range(subset_mat.shape[1]):
            cell_annos.append(i)
    return(cell_annos)


def make_correct_num_cells_mat(orig_mat, num_cells):
    print("orig_mat.shape",orig_mat.shape)
    orig_num_cells = orig_mat.shape[0]*orig_mat.shape[2]
    print("orig_num_cells",orig_num_cells)
    remove = orig_num_cells - num_cells
    #remove = num_cells - at_least
    print("removing",remove)
    new_mat = []
    for i in range(orig_mat.shape[0]):
        if i < remove:
            new_mat.append(orig_mat[i,:,1:])
        else:
            new_mat.append(orig_mat[i,:,:])
    return(new_mat)


def flatten_3d(correct_num_cells_mat):
    ## first get the number of cells
    total_cells = 0
    for i in range(len(correct_num_cells_mat)):
        print(i,correct_num_cells_mat[i].shape[1])
        total_cells += correct_num_cells_mat[i].shape[1]
    print(total_cells, "cells in adjusted 3d mat")
    out_mat = np.zeros((correct_num_cells_mat[0].shape[0], total_cells))
    offset = 0
    for i in range(len(correct_num_cells_mat)):
        current_number_of_cells = correct_num_cells_mat[i].shape[1]
        print('\tcurrent_number_of_cells',i,current_number_of_cells)
        out_mat[:,offset:(offset+current_number_of_cells)]=correct_num_cells_mat[i][:,:]
        offset += current_number_of_cells
    print("final matrix dimentions:",out_mat.shape)
    return(out_mat)


def do_sim(num_groups, 
           num_genes, 
           num_rand_genes,
           num_cells, 
           out_dir, 
           input_file_taregts,
           input_file_regs,
           sergio_dir):
    ## load sergio
    sergio = load_sergio(sergio_dir)
    print("building SERGIO model")
    ## num cells is divided by the num_groups because SERGIO's number_sc argument is actually the number of single cells PER GROUP
    sim = sergio.sergio(number_genes=num_genes, number_bins = num_groups, number_sc = ceiling(num_cells/num_groups), noise_params = 1, decays=0.8, sampling_state=5, noise_type='dpd')
    sim.build_graph(input_file_taregts =input_file_taregts, input_file_regs=input_file_regs, shared_coop_state=0)
    print("running simulation")
    start = time.time()
    sim.simulate()
    print("\t",(time.time()-start)/60,"min")
    print("calculating expression")
    expr = sim.getExpressions()
    print(expr.shape)
    ## outlier hyperparameters
    outlier_prob = 0.01
    mean = 1.0
    scale = 0.5
    # do outliers
    print("adding outlier genes")
    expr_O = sim.outlier_effect(expr, outlier_prob, mean, scale)
    print(expr_O[0].shape,expr_O[1].shape)
    ## library size effect 
    mean = 1.0
    scale = 0.5
    print("adding library size effect")
    expr_O_L = sim.lib_size_effect(expr_O, mean, scale)
    print(expr_O_L[0].shape,expr_O_L[1].shape)
    # print("expr_O_L[0]")
    # sns.heatmap(expr_O_L[0])
    # plt.show()
    # print("expr_O_L[1]")
    # sns.heatmap(np.concatenate(expr_O_L[1],axis=1))
    # plt.show()
    ## and dropout effect
    #########################################
    ## !!!!!!! This is important !!!!!!!!! ##
    # The sergio implementation of dropout is incorrect
    # droput happens by random sampling of skewed distributions.
    # This means that . 
    # Doing matrix muliplication by a binary vector is randomonly MASKING
    # this does not produce equivalent results to random sampling of
    # skewed distributions. In reality, downsampling differentially loses
    # low abundance transcripts. This approach gives the loss of observation
    # of a given gene EQUAL chances of being observed or unobserved.
    # Below was the code that th
    # I therefore chose to use my own implementation of dropout
    # shape = None
    percentile = .90
    # binary_ind = sim.dropout_indicator(expr_O_L, shape, percentile)
    # print("simulating dropout")
    # binary_ind = sim.dropout_indicator(expr_O_L[0])
    # print("binary_ind")
    # print(binary_ind)
    # expr_O_L_D = np.multiply(binary_ind, expr_O_L[0])
    ## convert to UMI counts
    print("calculating final count matrix")
    #count_matrix = np.concatenate(sim.convert_to_UMIcounts(expr_O_L_D),axis=1)

    ## Now we'll give it a more realistic "mean expression" by multiplying these by a beta distribution
    ## this is also where we slip in the random negative binomial distirbuted genes into the mix
    correct_num_cells_mat = make_correct_num_cells_mat(expr_O_L[1], num_cells)
    cell_annotations = get_cell_anno_vect(correct_num_cells_mat)
    correct_flat_mat = flatten_3d(correct_num_cells_mat)
    temp_input = add_random_genes(correct_flat_mat, num_rand_genes)
    temp_input *= 1e4
    for i in range(temp_input.shape[0]):
        temp_input[i,:]*=np.random.beta(.15,1)+np.random.rand()*1e-3
    ## convert to UMI
    count_matrix = sim.convert_to_UMIcounts(temp_input)
    print(count_matrix.shape)
    norm_mat = lin_norm_rows(np.log2(count_matrix+1))
    # sns.heatmap(norm_mat)
    # plt.show()
    # sns.heatmap(norm_mat[:200,:])
    # plt.show()
    spear_r, spear_p = stats.spearmanr(norm_mat,norm_mat)
    # sns.heatmap(spear_r)
    # plt.show()
    # sns.heatmap(spear_p)
    # plt.show()
    return(count_matrix, cell_annotations)


def set_up_dirs(out_dir):
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    processing_dir = os.path.join(out_dir, 'processing')
    if not os.path.isdir(processing_dir):
        os.mkdir(processing_dir)
    return(processing_dir)


def get_all_cell_type_specific_gene_ids(num_groups,
                                        num_MRs,
                                        num_non_mr_genes_per_cell_type_specific_module,
                                        num_genes):
    ## generate a list of lists that hold all of the cell type sepcific genes
    cur_gene_id = num_MRs## start the indexing for the 
    cell_type_specific_gene_idx_lists = []
    for i in range(num_groups):
        cell_type_specific_gene_idx_lists.append([])
        for j in range(num_non_mr_genes_per_cell_type_specific_module):
            cell_type_specific_gene_idx_lists[i].append(cur_gene_id)
            cur_gene_id+=1
        remaining_genes = np.array(np.arange(num_genes-cur_gene_id)+cur_gene_id)
        remaining_genes = remaining_genes.tolist()
    cell_type_specific_gene_idx_lists.append(remaining_genes)
    regulator_lists = []
    for i in range(num_groups):
        regulator_lists.append([i])
    last_vect = np.arange(num_MRs - num_groups)+num_groups
    regulator_lists.append(last_vect.tolist())
    # for i in range(len(regulator_lists)):
    #     print(regulator_lists[i],cell_type_specific_gene_idx_lists[i])
    return(regulator_lists, cell_type_specific_gene_idx_lists)


def get_random_hill_coef(number=1):
    all_hill = []
    for i in range(number):
        temp_hill = np.random.rand()*2+1
        all_hill.append(temp_hill)
    return(all_hill)


def get_random_K(number=1,pos=True):
    all_hill = []
    for i in range(number):
        temp_hill = np.random.rand()*2+1
        if not pos:
            if np.random.rand()>0.5:
                temp_hill*=-1
        all_hill.append(temp_hill)
    return(all_hill)


def make_module(regulators, targets, regulator_cap = 3):
    """
    inputs: regulators: a list of indices that contain master regulators
    """
    grn_lines = []
    for i in range(len(targets)):
        num_possible_regs = len(regulators)
        if num_possible_regs==1:
            num_regs = 1
            grn_lines.append([targets[i],1]+regulators+get_random_K(number = num_regs)+get_random_hill_coef(number=num_regs))
        else:
            ## first pick out the final set of regulators for this gene
            random.shuffle(regulators)
            num_regs = max([1,np.sum(np.random.rand(regulator_cap)>0.5)]) 
            temp_regs = regulators[:num_regs]
            num_regs = len(temp_regs)
            if num_regs == 0:
                num_regs = 1## giving it 1 regulator, but without an actual parent ID appears to generate somewhat non-regulated gene expression
            ## 
            grn_lines.append([targets[i],num_regs]+temp_regs+get_random_K(number = num_regs, pos=False)+get_random_hill_coef(number=num_regs))
            #grn_lines.append([targets[i],1]+temp_regs+get_random_K(number = num_regs, pos = False)+get_random_hill_coef(number=num_regs))
    return(grn_lines)


def make_final_grn_file(regulator_lists, cell_type_specific_gene_idx_lists):
    """
     target gene id, 
     number of target’s regulators, 
     regulator ID_1,…, regulator ID_n, 
     K_1,…,K_n, 
     hill_coeff_1, …, hill_coeff_n
    """
    final_grn_table=[]
    for i in range(len(cell_type_specific_gene_idx_lists)):
        final_grn_table += make_module(regulator_lists[i], cell_type_specific_gene_idx_lists[i])
    return(final_grn_table)

def write_deg_table(regulator_lists, cell_type_specific_gene_idx_lists, num_groups, num_total_genes, out_dir):
    all_deg_indicies = []
    for i in range(num_groups):
        all_deg_indicies += regulator_lists[i]
        all_deg_indicies += cell_type_specific_gene_idx_lists[i]
    ## add the gene leader
    for i in range(len(all_deg_indicies)):
        all_deg_indicies[i]="Gene"+str(all_deg_indicies[i])
    all_deg_indicies= set(all_deg_indicies)
    out_table = [["","gene","deg"]]
    for i in range(num_total_genes):
        temp_gene_name = "Gene"+str(i)
        temp_line = [str(i),temp_gene_name]
        if temp_gene_name in all_deg_indicies:
            temp_line.append("TRUE")
        else:
            temp_line.append("FALSE")
        out_table.append(temp_line)
    write_table(out_table,os.path.join(os.path.dirname(out_dir),"sergio_ground_truth_degs.tsv"))
    return()

def make_grn(mr_table,
             num_genes,
             out_dir,
             num_total_genes,
             percent_non_mr_genes_group_specific = .15,
             num_cell_type_specific_per_group = None):
    """
    """
    num_groups = len(mr_table[0])-1
    print("num_groups",num_groups)
    num_non_mr = num_genes - len(mr_table)
    print("num_non_mr",num_non_mr)
    if num_cell_type_specific_per_group is None:
        num_cell_type_specific = num_non_mr * percent_non_mr_genes_group_specific
    else:
        num_cell_type_specific = num_cell_type_specific_per_group * num_groups
    print("num_cell_type_specific",num_cell_type_specific)
    num_non_mr_genes_per_cell_type_specific_module = int(num_cell_type_specific/num_groups)
    print("num_non_mr_genes_per_cell_type_specific_module",num_non_mr_genes_per_cell_type_specific_module)
    regulator_lists, cell_type_specific_gene_idx_lists = get_all_cell_type_specific_gene_ids(num_groups,
                                                                                             len(mr_table),
                                                                                             num_non_mr_genes_per_cell_type_specific_module,
                                                                                             num_genes)
    # ## write the ground truth DEGs
    # FOund that there were a lot of genes that weren't even expressed that were 
    # gt degs. Need to do DETECTABLE degs instead.
    write_deg_table(regulator_lists, 
                    cell_type_specific_gene_idx_lists, 
                    num_groups, 
                    num_total_genes, 
                    out_dir)
    final_grn_table = make_final_grn_file(regulator_lists, cell_type_specific_gene_idx_lists)
    out_grn_file = os.path.join(out_dir,"full_GRN.csv")
    write_table(deepcopy(final_grn_table),out_grn_file,sep=',')
    return(out_grn_file)


def make_gene_regs(num_groups,
                   out_dir,
                   non_specific_MRs_multiple = 1):
    ## this file has a different master regulator (MR) on each line
    ## first column is the index of the MR, subsequent columns are 
    # uniform distribution between 0 & 0.333
    group_level_mr_effect_mat = np.random.rand(int(non_specific_MRs_multiple*num_groups),num_groups)/5
    ## add 0.66 to the top num_group master regulators that will be regulating
    ## group specific module expression
    for i in range(num_groups):
        print("\tadding signal to group#",i)
        group_level_mr_effect_mat[i,:]=0
        group_level_mr_effect_mat[i,i]+=.7+np.random.rand()/5
        print("\t\t",group_level_mr_effect_mat[i,i])
    for i in range(num_groups,group_level_mr_effect_mat.shape[0]):
        ## for the non-group specific regulators, shrink the variance, and shift up
        #group_level_mr_effect_mat[i,:]
        group_level_mr_effect_mat[i,:]=np.random.rand()
    # sns.heatmap(group_level_mr_effect_mat)
    # plt.show()
    ## now add the MR index numbers
    gene_regulation_table = group_level_mr_effect_mat.tolist()
    for i in range(len(gene_regulation_table)):
        gene_regulation_table[i]=[i]+gene_regulation_table[i]
    out_mr_file = os.path.join(out_dir,"MRs.csv")
    write_table(deepcopy(gene_regulation_table),out_mr_file,sep=',')
    return(out_mr_file, gene_regulation_table)


def add_labels(sim_count_mat):
    header = ["Cell"]*sim_count_mat.shape[1]
    for i in range(len(header)):
        header[i]+=str(i)
    header = ['genes']+header
    row_labs = ["Gene"]*sim_count_mat.shape[0]
    for i in range(sim_count_mat.shape[0]):
        row_labs[i]+=str(i)
    sim_count_mat = sim_count_mat.tolist()
    for i in range(len(sim_count_mat)):
        sim_count_mat[i]=[row_labs[i]]+sim_count_mat[i]
    sim_count_mat = [header]+sim_count_mat
    return(sim_count_mat)


def sim_sergio(seed,
               num_groups, 
               num_genes, 
               num_rand_genes, 
               num_cells, 
               out_dir, 
               sergio_dir,
               num_cell_type_specific_per_group = None):
    random.seed(seed)
    np.random.seed(seed)
    ## set up the output directories
    processing_dir = set_up_dirs(out_dir)
    ## first we need to make the GRN csv
    out_mr_file, gene_regulation_table = make_gene_regs(num_groups,
                                                        processing_dir)
    num_total_genes = num_genes + num_rand_genes
    if num_cell_type_specific_per_group is not None:
        num_genes = num_groups + num_cell_type_specific_per_group*num_groups 
        num_rand_genes = num_total_genes-num_genes
        print("\tThe user specified a specific number of cell-type specific genes per group.\
               \n\tThis over-rides the total number of genes in the simulation to be the number of groups \
               \n\ttimes the number of genes per group.")
        print("\tThe current number of groups in the simulation are:",num_groups)
        print("\tThe current number of genes in the simulation are:",num_genes)
        print("\tThe current number of random genes in the simulation are:",num_rand_genes)
    ## then generate the hill coefficient stuff
    input_file_taregts = make_grn(gene_regulation_table,
                                  num_genes,
                                  processing_dir,
                                  num_total_genes,
                                  num_cell_type_specific_per_group = num_cell_type_specific_per_group)
    ## then make the simulation
    sim_count_mat, cell_annotations = do_sim(num_groups, 
                                             num_genes, 
                                             num_rand_genes, 
                                             num_cells, 
                                             out_dir, 
                                             input_file_taregts,
                                             out_mr_file,
                                             sergio_dir)
    sim_count_mat = add_labels(sim_count_mat)
    out_file = os.path.join(out_dir,"raw_expression.tsv")
    write_table(deepcopy(sim_count_mat),out_file)
    cell_labs = sim_count_mat[0][1:]
    out_cell_anno_mat = []
    for i in range(len(cell_labs)):
        out_cell_anno_mat.append([cell_labs[i]]+[cell_annotations[i]])
    write_table(deepcopy(out_cell_anno_mat),os.path.join(out_dir,'ground_truth_annotations.tsv'))
    return(out_file)


if __name__ == '__main__':
    ## get the arguments and pass them into the sergio simulation function
    parser = argparse.ArgumentParser()

    parser.add_argument("-num_groups",
        default = 10,
        type = int,
        help="the number of clusters to simulate")

    parser.add_argument("-num_network_genes",
        default = 2000,
        type = int,
        help="the number of genes to simulate")

    parser.add_argument("-num_cell_type_specific_per_group",
        default = None,
        type = int,
        help="the number of genes that are specific to each group")

    parser.add_argument("-num_total_genes",
        default = 10000,
        type = int,
        help="the number of genes to simulate")

    parser.add_argument("-num_cells",
        default = 2500,
        type = int,
        help="the number of genes to simulate")

    parser.add_argument("-sergio_dir", 
        help="the directory that contains the sergio repository.",
        default = "SERGIO/",
        type = str)

    parser.add_argument("-out_dir", '-out', '-o',
        help="the directory that we'll write the results to.",
        default="data/scRNAseq_sim/sergio/10/1",
        type = str)

    parser.add_argument("-seed", 
        help="The seed for random number generation.",
        default = 12345678,
        type = int)

    args = parser.parse_args()
    ## unpack the args
    out_dir = args.out_dir
    seed = args.seed
    num_groups = args.num_groups
    num_genes = args.num_network_genes
    num_rand_genes = args.num_total_genes - num_genes
    num_cells = args.num_cells
    sergio_dir = args.sergio_dir
    num_cell_type_specific_per_group = args.num_cell_type_specific_per_group
    ## call the main simulation function
    sim_sergio(seed,
               num_groups, 
               num_genes, 
               num_rand_genes, 
               num_cells, 
               out_dir, 
               sergio_dir,
               num_cell_type_specific_per_group = num_cell_type_specific_per_group)


