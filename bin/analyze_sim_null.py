#!/usr/env python3
import os
import networkx as nx
import pandas as pd
import seaborn as sns
import rpy2.rinterface as rinterface

from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri
import rpy2.robjects as ro
from matplotlib import pyplot as plt

IN_FILE = "data/scRNAseq_simNull_results/clustering_result_stats.tsv"
OUT_DIR = "data/scRNAseq_simNull_results/"


############################################################
## analysis
def process_stats(method_stats, out_dir):
    print('\n\n',method_stats)
    ## write the dataframe to file
    # out_path = os.path.join(out_dir,"method_summary.tsv")
    # method_stats.to_csv(path_or_buf=out_path,
    #                    sep='\t', index = False)
    # plt.clf()
    # ax = sns.boxplot(x = 'method', y='max_recursion_depth', data = method_stats)
    # ax = sns.swarmplot(x = 'method', y='max_recursion_depth', edgecolor = 'white', linewidth=1, data = method_stats)
    # plt.ylabel("maximum recursion depth")
    # plt.savefig(os.path.join(out_dir,'max_recursion_depth.png'),dpi=600,bbox_inches='tight')
    # plt.clf()
    # ax = sns.boxplot(x = 'method', y='total_number_of_groups', data = method_stats)
    # ax = sns.swarmplot(x = 'method', y='total_number_of_groups', edgecolor = 'white', linewidth=1, data = method_stats)
    # ax.set_yscale('log')
    # plt.ylabel("total number of groups")
    # plt.savefig(os.path.join(out_dir,'total_number_of_groups.png'),dpi=600,bbox_inches='tight')
    
    #ax = sns.boxplot(x = 'true_num_clust', y='guessed_num_clust', hue = 'method', data = method_stats)
    #ax = sns.swarmplot(x = 'method', y='total_number_of_groups', edgecolor = 'white', linewidth=1, data = method_stats)
    ax = sns.boxplot(x = 'cluster_method', y='guessed_num_clust', data = method_stats)
    ax = sns.swarmplot(x = 'cluster_method', y='guessed_num_clust', data = method_stats, edgecolor = 'white', linewidth = 1)
    #ax.set_yscale('log')
    plt.ylabel("guessed number of groups")
    plt.savefig(os.path.join(out_dir,'guessed_num_groups.png'),dpi=600,bbox_inches='tight')
    
    plt.clf()
    ax = sns.boxplot(x = 'cluster_method', y='abs_dist_to_true_num_clust', data = method_stats)
    ax = sns.swarmplot(x = 'cluster_method', y='abs_dist_to_true_num_clust', data = method_stats, edgecolor = 'white', linewidth = 1)
    #ax = plt.lineplot(x = 'true_num_clust', y='true_num_clust', data = method_stats, color = 'black', linestyles = ':')
    #ax.set_yscale('linear')
    plt.ylabel("abs(distance to true number of clusters)")
    plt.savefig(os.path.join(out_dir,'abs_dist_to_true_num_clust.png'),dpi=600,bbox_inches='tight')
    #plt.savefig(os.path.join(out_dir,'total_number_of_groups.png'),dpi=600,bbox_inches='tight')


    return


def do_analysis(top_dir, out_dir):
    out_dir = os.path.join(out_dir, '')
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    ## read in the summary file
    method_stats = pd.read_csv(IN_FILE, sep = '\t')

    process_stats(method_stats, out_dir)



def main():
    do_analysis(IN_FILE, OUT_DIR)

if __name__ == '__main__':
    main()
