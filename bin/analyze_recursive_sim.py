#!/usr/env python3
import os
import numpy as np
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import pandas as pd
import seaborn as sns
import argparse
from matplotlib import pyplot as plt


def get_dirs(top_dir):
    return(sorted([(temp_dir) for temp_dir in os.listdir(top_dir) if os.path.isdir(os.path.join(top_dir,temp_dir))]))

def get_color(G):
    color_list = ['black', 'red', 'orange', 'yellow', 'green', 'purple', 'blue', 'brown', 'pink', 'cyan']# + ['black']*10
    final_color_list = []
    for node in G.nodes(data=True):
        #print(node)
        temp_rec_layer = node[1]['recursion_layer']
        if temp_rec_layer == None:
            final_color_list.append('black')
        else:
            #print(temp_rec_layer)
            final_color_list.append(color_list[temp_rec_layer])
    return(final_color_list)


class method_result():
    def __init__(self, 
                 name, 
                 true_groups,
                 method_dir):
        print('\n\n',name)
        
        self.name = name
        self.true_groups = true_groups
        self.method_dir = method_dir 
        self.total_number_of_groups = 0
        self.max_recursion_depth = 0
        self.avg_recursion_depth = 0
        self.recursion_graph =nx.DiGraph()

        ## now do the analysis
        self.get_method_graph()
        #self.pos = nx.nx_pydot.graphviz_layout(self.recursion_graph, prog='twopi')
        self.pos = graphviz_layout(self.recursion_graph, prog='twopi')
        self.get_max_recursion_depth()
        self.get_avg_recursion_depth()
        plt.clf()
        nx.draw(self.recursion_graph,self.pos,node_size=100/(self.max_recursion_depth+1),alpha=0.5,node_color=get_color(self.recursion_graph), with_labels=False)
        plt.savefig(os.path.join(method_dir,name+'_recursion_graph.png'), height = 6, width = 6, units='in', dpi=600,bbox_inches='tight')
        self.get_total_groups()


    def get_total_groups(self):
        self.leaf_nodes = [x for x in self.recursion_graph.nodes() if self.recursion_graph.out_degree(x)==0 and self.recursion_graph.in_degree(x)==1]
        self.total_number_of_groups = len(self.leaf_nodes)
        return


    def get_max_recursion_depth(self):
        curr_max_rec_depth = 0
        for node in self.recursion_graph.nodes(data=True):
            if node[1]['recursion_layer'] != None:
                if node[1]['recursion_layer'] > curr_max_rec_depth:
                    curr_max_rec_depth = node[1]['recursion_layer']
        self.max_recursion_depth = curr_max_rec_depth
        return

    def get_nearest_first_layer(self, sp, first_layer_nodes, leaf_nodes):
        temp_undirected = self.recursion_graph.to_undirected()
        leaf_to_first_neighbor_dict={}
        first_layer_to_leaf_dict = {}
        for leaf in leaf_nodes:
            if leaf in first_layer_nodes:
                temp_leaf_nearest_dist = 0
                temp_leaf_nearest_first_id = leaf
            else:
                temp_leaf_nearest_dist = 9999999999
                temp_leaf_nearest_first_id = None
                for first_layer_node in first_layer_nodes:
                    temp_dist = nx.shortest_path_length(temp_undirected,source=first_layer_node,target=leaf)
                    # try:
                    #     temp_dist = len(sp[leaf][first_layer_node])-1
                    # except:
                    #     temp_dist = len(sp[first_layer_node][leaf])-1
                    #print(leaf, first_layer_node, ":", temp_dist)
                    #print(sp[leaf][first_layer_node])
                    if temp_dist < temp_leaf_nearest_dist:
                        temp_leaf_nearest_dist = temp_dist
                        temp_leaf_nearest_first_id = first_layer_node
            ## now that we know the nearest first layer & it's distance
            ## to the leaf at hand, update the first layer dict and the leaf dict
            if temp_leaf_nearest_first_id not in first_layer_to_leaf_dict:
                first_layer_to_leaf_dict[temp_leaf_nearest_first_id]=[]
            temp_list = first_layer_to_leaf_dict[temp_leaf_nearest_first_id]
            temp_list.append(temp_leaf_nearest_dist)
            first_layer_to_leaf_dict[temp_leaf_nearest_first_id]=temp_list
            leaf_to_first_neighbor_dict[leaf]=temp_leaf_nearest_dist
            #print(temp_leaf_nearest_first_id,first_layer_to_leaf_dict[temp_leaf_nearest_first_id])
        ## go through the first layer nodes & get the mean of the distance to their leaves
        all_first_layer_avg_rec_dist = []
        for temp_first_layer_node in first_layer_nodes:
            all_first_layer_avg_rec_dist.append(np.mean(np.array(first_layer_to_leaf_dict[temp_first_layer_node])))
        return(all_first_layer_avg_rec_dist)


    def get_avg_recursion_depth(self):
        ## first get the leaves
        #all_leaves = [x for x in self.recursion_graph.nodes() if self.recursion_graph.out_degree(x)==0 and self.recursion_graph.in_degree(x)==1]
        all_leaves = [x for x in self.recursion_graph.nodes() if self.recursion_graph.degree(x)==1]
        ## then calculate the shortest path from full_dataset to all leaves
        sp = dict(nx.all_pairs_shortest_path(self.recursion_graph))
        #print(sp)
        ## first get all of the layer 1 clusters
        first_layer_nodes = []
        for node in self.recursion_graph.nodes():
            if node != "full_dataset":
                print(node, sp["full_dataset"][node])
                if len(sp["full_dataset"][node])==2:
                    first_layer_nodes.append(node)
        ##
        self.all_leaf_dist = self.get_nearest_first_layer(sp, first_layer_nodes, all_leaves)
        # self.all_leaf_dist = []
        # for leaf in all_leaves:
        #     print(leaf,sp["full_dataset"][leaf])
        #     self.all_leaf_dist.append(sp["full_dataset"][leaf])
        self.avg_recursion_depth = np.mean(np.array(self.all_leaf_dist))
        return


    def get_method_graph(self):
        print("starting to build graph")
        ## initialize the with a single node representing the whole dataset prior to clustering
        self.recursion_graph.add_node('full_dataset')
        nx.set_node_attributes(self.recursion_graph, {'full_dataset':None}, 'recursion_layer')
        nx.set_node_attributes(self.recursion_graph, {'full_dataset':self.method_dir}, 'full_dir')

        ## build out thegraph network 
        self.recursive_strucutre = self.get_recursive_dirs(top_dir = self.method_dir,
                                                           originating_node = 'full_dataset',
                                                           recursion_layer = 0)
        return


    def get_recursive_dirs(self,
                           top_dir,
                           originating_node,
                           recursion_layer):
        #print('  '*recursion_layer,'doing recursion layer:',recursion_layer)
        #all_dirs = os.walk(self.method_dir)
        ## first get the list of all of the current directories
        all_dirs = get_dirs(top_dir)
        ## filter for dirs that the recursion is pertinent to (ie integer dirs)
        all_rec_dirs = []
        all_rec_full_dirs = []
        new_nodes = []
        for possible_dir in all_dirs:
            try:
                int(possible_dir)
            except:
                pass
            else:
                all_rec_dirs.append(possible_dir)
                all_rec_full_dirs.append(os.path.join(top_dir,possible_dir))
                new_nodes.append(originating_node+"|"+str(recursion_layer)+"_"+str(possible_dir))

        ## set the base case, where there are no more new nodes to add
        if len(new_nodes)==0:
            return

        ## log the new nodes
        for i in range(len(new_nodes)):
            temp_node = new_nodes[i]
            full_dir = all_rec_full_dirs[i]
            self.recursion_graph.add_node(temp_node)
            ## set the recursion layer attribute
            temp_rec_dict = nx.get_node_attributes(self.recursion_graph, 'recursion_layer')
            temp_rec_dict[temp_node] = recursion_layer
            nx.set_node_attributes(self.recursion_graph, temp_rec_dict, 'recursion_layer')
            ## now do the same for the full directory
            temp_dir_dict = nx.get_node_attributes(self.recursion_graph, 'full_dir')
            temp_dir_dict[temp_node] = full_dir
            nx.set_node_attributes(self.recursion_graph, temp_dir_dict, 'full_dir')

        ## log the new edges
        for new_node in new_nodes:
            #print('  '*recursion_layer,originating_node, new_node)
            self.recursion_graph.add_edge(originating_node, new_node)

        ## do the next round of recursion
        for i in range(len(new_nodes)):
            new_top_dir = all_rec_full_dirs[i]
            new_temp_node = new_nodes[i]
            new_recursion_layer = recursion_layer + 1
            self.get_recursive_dirs(top_dir = new_top_dir,
                                    originating_node = new_temp_node,
                                    recursion_layer = new_recursion_layer)
        return

############################################################
## analysis
def process_stats(method_stats, out_dir):
    ## remove the cases in which the clustering didn't work at all
    is_valid = method_stats['total_number_of_groups'] > 0
    method_stats = method_stats[is_valid]
    
    print('\n\n',method_stats)
    ## write the dataframe to file
    out_path = os.path.join(out_dir,"method_summary.tsv")
    method_stats.to_csv(path_or_buf=out_path,
                       sep='\t', index = False)
    plt.clf()
    ax = sns.boxplot(x = 'method', y='max_recursion_depth', data = method_stats)
    ax = sns.swarmplot(x = 'method', y='max_recursion_depth', edgecolor = 'white', linewidth=1, data = method_stats)
    plt.ylabel("maximum recursion depth")
    plt.savefig(os.path.join(out_dir,'max_recursion_depth.png'),dpi=600,bbox_inches='tight')
    plt.clf()
    ax = sns.boxplot(x = 'method', y='avg_recursion_depth', data = method_stats)
    ax = sns.swarmplot(x = 'method', y='avg_recursion_depth', edgecolor = 'white', linewidth=1, data = method_stats)
    plt.ylabel("mean recursion depth")
    plt.savefig(os.path.join(out_dir,'avg_recursion_depth.png'),dpi=600,bbox_inches='tight')
    plt.clf()
    ax = sns.boxplot(x = 'method', y='total_number_of_groups', data = method_stats)
    ax = sns.swarmplot(x = 'method', y='total_number_of_groups', edgecolor = 'white', linewidth=1, data = method_stats)
    ax.set_yscale('log')
    plt.ylabel("total number of groups")
    plt.savefig(os.path.join(out_dir,'total_number_of_groups.png'),dpi=600,bbox_inches='tight')
    return


def process_results_list(res_list):
    ## first layer is iterations second layer is each method object
    ## first thing we need to do is catelogue all of the methods
    all_methods = []
    for temp_iter in res_list:
        for method in temp_iter:
            all_methods.append(method.name)
    all_methods = sorted(list(set(all_methods)))
    ## second re-organize the list to be grouped by method rather than iteration
    method_index_dict = {value:key for key, value in enumerate(all_methods)}
    method_lists = []
    for meth in range(len(all_methods)):
        method_lists.append([])
    for temp_iter in res_list:
        for method in temp_iter:
            temp_idx = method_index_dict[method.name]
            method_lists[temp_idx].append(method)
    ## make the pandas data frame
    ## first make the method vector
    method = []
    max_recursion_depth = []
    avg_recursion_depth = []
    total_number_of_groups = []
    iteration = []
    for i in range(len(method_lists)):
        temp_meth_vect = method_lists[i]
        for j in range(len(temp_meth_vect)):
            iteration.append(j)
            method.append(temp_meth_vect[j].name)
            max_recursion_depth.append(temp_meth_vect[j].max_recursion_depth)
            avg_recursion_depth.append(temp_meth_vect[j].avg_recursion_depth)
            total_number_of_groups.append(temp_meth_vect[j].total_number_of_groups)
    method_stats = pd.DataFrame({'iteration':iteration,
                                 'method':method,
                                 'max_recursion_depth':max_recursion_depth,
                                 'avg_recursion_depth':avg_recursion_depth,
                                 'total_number_of_groups':total_number_of_groups})
    return(method_stats)


def do_recursive_analysis(top_dir, out_dir):
    out_dir = os.path.join(out_dir, '')
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    ## get the full recursive directory set
    all_results = []
    all_iters = get_dirs(top_dir)
    for iteration in all_iters:
        all_results.append([])
        temp_iter_dir = os.path.join(top_dir,iteration)
        for temp_method in get_dirs(temp_iter_dir):
            all_results[-1].append(method_result(temp_method, 2, os.path.join(temp_iter_dir, temp_method)))
    process_stats(process_results_list(all_results),out_dir)




if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("-top_dir", '-i',
        default = 'data/real_recursive/',
        help="the directory of the top of the recursive dir")

    parser.add_argument("-out_dir", 
        default = "data/real_recursive_results/",
        help="the directory of the top of the recursive dir")
    args = parser.parse_args()
    #TOP_DIR = "data/scRNAseq_sim_recursive/2/"
    #OUT_DIR = "data/scRNAseq_sim_recursive_results/"
    TOP_DIR = args.top_dir
    OUT_DIR = args.out_dir

    do_recursive_analysis(TOP_DIR, OUT_DIR)