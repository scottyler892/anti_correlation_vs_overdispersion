import fileinput
import numpy as np
from anticor_features import get_anti_cor_genes



def strip_split(line, delim='\t'):
    return line.strip('\n').split(delim)


def fast_read_mat(source_file):
    row_labs = []
    col_labs = []
    first = True
    for line in fileinput.input(source_file):
        temp_line = strip_split(line)
        if first:
            first = False
            col_labs = temp_line[1:]
        else:
            row_labs.append(temp_line[0])
    fileinput.close()
    out_mat = np.zeros((len(row_labs),len(col_labs)))
    first = True
    counter=0
    for line in fileinput.input(source_file):
        if first:
            first = False
        else:
            temp_line = list(map(float,strip_split(line)[1:]))
            out_mat[counter,:]=temp_line
            counter+=1
    fileinput.close()
    return(row_labs, col_labs, out_mat)





if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-infile", '-i',
                        default = "/home/scott/bin/anti_correlation_vs_overdispersion/data/cell_line_null/human/anti_cor/expression.tsv",
                        help="the input expression matrix")
    parser.add_argument("-species",
                        default = "hsapiens",
                        help="gProfiler compatible species code")
    parser.add_argument("-out_file",
                        help="the output file for the anti_cor_feature table")
    args = parser.parse_args()
    all_features, all_cells, in_mat = fast_read_mat(args.infile)
    anti_cor_table = get_anti_cor_genes(in_mat,
                                        all_features,
                                        species=args.species,
                                        pre_remove_pathways = [])




