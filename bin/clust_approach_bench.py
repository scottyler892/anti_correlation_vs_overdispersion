import os
import sc3s
import numpy as np
import pandas as pd
import scanpy as sc
import anndata as ad
import seaborn as sns
from copy import deepcopy
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn import metrics
from sklearn.metrics import pairwise
from sklearn.metrics.pairwise import euclidean_distances as euc
import statsmodels.api as sm
import statsmodels.formula.api as smf
from pyminer.pyminer_clust_funcs import do_louvain_primary_clustering
from pyminer.common_functions import cmd, save_dict, import_dict
from anticor_features.anticor_features import get_anti_cor_genes


def get_subdirs(top_dir):
    blah = sorted([ f.path for f in os.scandir(top_dir) if f.is_dir() ])
    return(blah)


def load_dset(iter_dir, real_dset = False, start_gene_idx=1):
    ## returns genes, cells, exprs
    exprs = pd.read_csv(os.path.join(iter_dir,"expression_ds_norm_counts.tsv"),
                        sep="\t")
    if real_dset:
        ## TODO
        exprs.index=exprs.iloc[:,0]
    else:
        exprs.index=exprs.iloc[:,0]
        #exprs.index=["Gene"+str(i) for i in range(start_gene_idx,exprs.shape[0]+start_gene_idx)]
    c = exprs.columns[1:].tolist()
    print(exprs.head())
    return(exprs.index.tolist(), c, exprs.iloc[:,1:].to_numpy())


def mcc(TP, TN, FP, FN):
    return((TP*TN - FP*FN) / np.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN)))


def acc(TP, TN, FP, FN):
    return((TP+TN)/(FP+FN+TP+TN))


def f1_score(TP, TN, FP, FN):
    return(2*TP/(2*TP+FP+FN))


def add_other_metrics(in_df):
    in_df["precision"]=in_df["TP"]/(in_df["TP"]+in_df["FP"])
    in_df["TPR"]=in_df["TP"]/(in_df["TP"]+in_df["FN"])
    in_df["TNR"]=in_df["TN"]/(in_df["TN"]+in_df["FP"])
    in_df["FPR"]=in_df["FP"]/(in_df["FP"]+in_df["TN"])
    in_df["FNR"]=in_df["FN"]/(in_df["FN"]+in_df["TP"])
    in_df["FDR"]=in_df["FP"]/(in_df["FP"]+in_df["TP"])
    in_df["FOR"]=in_df["FN"]/(in_df["FN"]+in_df["TN"])
    in_df["ACC"]=acc(in_df["TP"], in_df["TN"], in_df["FP"], in_df["FN"])
    in_df["BA"]= (in_df["TPR"] + in_df["TNR"]) / 2 
    in_df["MCC"]=mcc(in_df["TP"], in_df["TN"], in_df["FP"], in_df["FN"])
    in_df["F1_score"]=f1_score(in_df["TP"], in_df["TN"], in_df["FP"], in_df["FN"])
    return in_df


def get_confusion(gt_deg_annotations, selected_genes, all_genes):
    gt_deg_annotations.index = gt_deg_annotations["gene"]
    gt_deg_annotations=gt_deg_annotations.loc[all_genes,:]
    print(gt_deg_annotations.head())
    num_total_ps = len(gt_deg_annotations[gt_deg_annotations["deg"]==True]["gene"])
    num_total_ns = gt_deg_annotations.shape[0]-num_total_ps
    selected_annotations = gt_deg_annotations.loc[selected_genes,"deg"]
    tps = np.sum(np.array(selected_annotations==True))
    fps = len(selected_annotations)-tps
    fns = num_total_ps - tps
    tns = num_total_ns - fps
    print("TP:",tps)
    print("FP:",fps)
    print("FN:",fns)
    print("TN:",tns)
    print("sum:",tps+fps+fns+tns)
    return(tps,fps,fns,tns)


def run_sweep_on_dir(iter_dir, fpr, fdr,
                    gt_deg_annotations,
                    genes, cells, exprs):
    anticor_res = get_anti_cor_genes(exprs, genes, pre_remove_pathways=[],
                                     FPR = fpr,
                                     FDR = fdr,
                                     scratch_dir="/media/scott/ssd_2tb/scratch/")
    print(anticor_res.head())
    selected_genes = anticor_res[anticor_res["selected"]==True]["gene"]
    conf_res = get_confusion(gt_deg_annotations, selected_genes,gt_deg_annotations["gene"])
    return(conf_res)

####################################################################

def lin_norm_rows(in_mat):
    row_mins = np.min(in_mat, axis = 1)
    for i in range(in_mat.shape[0]):
        in_mat[i,:] -= row_mins[i] 
    row_maxs = np.max(in_mat, axis = 1)
    for i in range(in_mat.shape[0]):
        if row_maxs[i] != 0:
            in_mat[i,:] /= row_maxs[i] 
    return(in_mat)


def get_hypotenus(k, sse):
    return(np.sqrt((k)**2 + sse**2))


def get_k_estimate(sse_vect, min_k_tried = 2):
    ## normalize the sse vector
    sse_vect = lin_norm_rows(np.array([sse_vect]))[0].tolist()
    #print('\t\t\tnormalized sse vect:',sse_vect)
    hypotenuses = []
    for k in range(len(sse_vect)):
        hypotenuses.append(get_hypotenus(k/(len(sse_vect)-1), sse_vect[k]))
        print('\t\t\thypotenuse (k='+str(k+min_k_tried)+'):',hypotenuses[-1])
    k_est = np.argmin(np.array(hypotenuses))+min_k_tried
    print("\t\t\t\tK estimate:",k_est)
    return(hypotenuses, sse_vect, k_est)


def get_sse(pcs, clust_vect):
    all_clusts = sorted(list(set(clust_vect)))
    clust_vect = np.array(clust_vect)
    total_err = 0
    for temp_clust in all_clusts:
        cur_clust_mat = pcs[np.where(clust_vect==temp_clust)[0],:]
        centroid = np.mean(cur_clust_mat,axis=0)
        total_err+=np.sum((cur_clust_mat-centroid[:,None].T)**2)
    return(total_err)


def get_sc3_elbow(local_adata, sc3_col_names):
    print("getting elbow")
    sse_vect = []
    for i in range(len(sc3_col_names)):
        sse_vect.append(get_sse(local_adata.obsm["X_pca"],local_adata.obs[sc3_col_names[i]].tolist()))
    hypotenuses, sse_vect, k_est = get_k_estimate(sse_vect)
    return(k_est)


def get_sc3_clusts(local_adata, k_sweep=[2,3,4,5,6,7,8,9,10,11,12,13,14,15]):
    # dimensionality reduction with PCA
    if "X_pca" not in local_adata.obsm:
        local_adata.obs["sc3_elbow"] = 0
    else:
        sc3s.tl.consensus(local_adata, n_clusters=k_sweep)
        out_names = ["sc3s_"+str(i) for i in k_sweep]
        k_est = get_sc3_elbow(local_adata, out_names)
        print("k_est:",k_est)
        local_adata.obs["sc3_elbow"] = local_adata.obs["sc3s_"+str(k_est)]
    return(local_adata.obs, ["sc3_elbow"])


def scanpy_leiden(local_adata):
    if "X_pca" not in local_adata.obsm:
        local_adata.obs["leiden"] = 0
    else:
        sc.pp.neighbors(local_adata, n_neighbors=10, n_pcs=50)
        sc.tl.leiden(local_adata)
    return(local_adata.obs, ["leiden"])



def scanpy_louvain(local_adata):
    if "X_pca" not in local_adata.obsm:
        local_adata.obs["louvain"] = 0
    else:
        sc.pp.neighbors(local_adata, n_neighbors=10, n_pcs=50)
        sc.tl.louvain(local_adata)
    return(local_adata.obs, ["louvain"])


# def pc50_euc(temp_mat, npcs=50, count_norm = 10000, skip_norm=True, **kwargs):
#     ## do the counts per 10k & log
#     if not skip_norm:
#       ## not doing the normalization, because it's alrady done
#       n_cells = temp_mat.shape[1]
#       cglsums = np.squeeze(np.array(np.sum(temp_mat,axis=0)))
#       norm_factor = cglsums/count_norm
#       for i in range(n_cells):
#           temp_mat[:,i]/=norm_factor[i]
#     num_comps = min(npcs,int(n_cells/2),int(temp_mat.shape[0]/2)) 
#     #print(num_comps)
#     pca = PCA(n_components=num_comps)
#     if "todense" in dir(temp_mat):
#         temp_mat.data = np.log2(1+temp_mat.data)
#     else:
#         temp_mat = np.log2(1+temp_mat)
#         temp_mat[np.isnan(temp_mat)]=0
#     #temp_mat = np.log2(1+temp_mat.T)
#     if "toarray" in dir(temp_mat):
#         if type(temp_mat)==csc_matrix:
#             pass
#         else:
#             temp_mat=csc_matrix(temp_mat)
#         ## to save on memory, remove the non-expressed genes
#         keep_idxs = sorted(list(set(temp_mat.indices)))
#         out_pcs = pca.fit_transform(temp_mat[keep_idxs,:].toarray().T)
#     else:
#         out_pcs = pca.fit_transform(temp_mat.T)
#     euc_mat = euc(out_pcs)
#     return(euc_mat)


def locally_weighted_louvain(local_adata):
    if "X_pca" not in local_adata.obsm:
        local_adata.obs["locally_weighted_louvain"] = 0
    else:
        louvain_res = do_louvain_primary_clustering(
            -1*euc(local_adata.obsm["X_pca"]),
            local_adata.obs.index.tolist()
        )
        local_adata.obs["locally_weighted_louvain"] = louvain_res.labels_
    return(local_adata.obs, ["locally_weighted_louvain"])




####################################################################
def copy_fs_subset(local_adata,feature_dict, gt_vect, cells, genes):
    fs_adata_dict = {}
    for fs_meth, fs_file in feature_dict.items():
        if fs_file is None:
            fs_adata_dict[fs_meth]=deepcopy(local_adata)
        else:
            fs_f = open(fs_file,"r")
            features = [feat.strip().replace('"','') for feat in fs_f.readlines()]
            keep_idxs = [i for i in range(local_adata.shape[1]) if str(local_adata.var.index[i]) in features]
            #print(keep_idxs)
            temp_adata = ad.AnnData(local_adata.X[:,keep_idxs])
            temp_adata.obs_names = cells
            temp_adata.var_names = np.array(genes)[keep_idxs]
            fs_adata_dict[fs_meth]=deepcopy(temp_adata)
    for temp_meth in fs_adata_dict.keys():
        fs_adata_dict[temp_meth].obs["ground_truth"]=gt_vect
        n_comps = np.min([50,fs_adata_dict[temp_meth].shape[1]-1])
        print(n_comps)
        if n_comps>=1:
            sc.tl.pca(fs_adata_dict[temp_meth], svd_solver='arpack', n_comps = n_comps)
    return(fs_adata_dict)


def do_python_cluster(
    iter_dir,
    feature_dict,
    norm_dset_file,
    gt_vect,
    real_dset=False):
    print(iter_dir)
    out_clust_files = []
    ## just using this file because it's already log transformed and normalized
    genes, cells, exprs = load_dset(iter_dir, real_dset=real_dset)
    adata = ad.AnnData(exprs.T)
    adata.obs_names = cells
    adata.var_names = genes
    sc.pp.scale(adata, max_value=10)
    print("making fs dict")
    fs_adata_dict = copy_fs_subset(adata, feature_dict, gt_vect, cells, genes)
    py_methods = {"sc3s":get_sc3_clusts,
                  "locally_weighted_louvain":locally_weighted_louvain,
                  "scanpy_louvain":scanpy_louvain,
                  "scanpy_leiden":scanpy_leiden,
                  }
    out_col_dict = {}
    for clust_meth_key, clust_meth_func in py_methods.items():
        print(clust_meth_key)
        for adata_key, adata_val in fs_adata_dict.items():
            print('\t',adata_key)
            print(adata_val)
            fs_adata_dict[adata_key].obs
            fs_adata_dict[adata_key].obs, temp_res_cols=clust_meth_func(adata_val)
            out_col_dict[clust_meth_key]=temp_res_cols[0]
    return(fs_adata_dict, out_col_dict)


def do_clust_r(non_norm_dset_file, out_file, feature_file="none"):
    if feature_file is None:
        feature_file = "none"
    cmd(" ".join(["Rscript --vanilla bin/run_all_clustR_algs.R ",
                  "-f", non_norm_dset_file,
                  "-s",feature_file,
                  "-o",out_file]))
    return 


def write_feats(temp_out_f, temp_feats):
    f = open(temp_out_f,"w")
    f.writelines("\n".join(temp_feats))
    f.close()
    return


def add_more_fs(iter_dir, 
                feature_dict,
                gt_feats,
                real_dset = False,
                percent_real_remaining = [.5, .2]):
    new_feature_dict = {}
    all_done = True
    for pct in percent_real_remaining:
        ##
        temp_name = "none_"+str(pct)+"_remain"
        temp_out_file = os.path.join(iter_dir,"anti_cor/"+temp_name+"_features.txt")
        new_feature_dict[temp_name]=temp_out_file
        if not os.path.isfile(temp_out_file):
            all_done = False
        ##
        temp_name = "anticor_"+str(pct)+"_remain"
        temp_out_file = os.path.join(iter_dir,"anti_cor/"+temp_name+"_features.txt")
        new_feature_dict[temp_name]=temp_out_file
        if not os.path.isfile(temp_out_file):
            all_done = False
        ## 
        temp_name = "anticor.25_"+str(pct)+"_remain"
        temp_out_file = os.path.join(iter_dir,"anti_cor/"+temp_name+"_features.txt")
        new_feature_dict[temp_name]=temp_out_file
        if not os.path.isfile(temp_out_file):
            all_done = False
    if not all_done:
        ## check if all are done
        genes, cells, exprs = load_dset(iter_dir, real_dset=real_dset, start_gene_idx=0)
        genes = [g.strip().replace('"','') for g in genes]
        gene_hash = {val:key for key, val in enumerate(genes)}
        for pct in percent_real_remaining:
            temp_anti_name = "anticor_"+str(pct)+"_remain"
            temp_anti_name2 = "anticor.25_"+str(pct)+"_remain"
            temp_none_name = "none_"+str(pct)+"_remain"
            ## if the none file exists already, read that in, as that will be the 
            ## original background to perform fs upon
            if not os.path.isfile(new_feature_dict[temp_none_name]):
                temp_keep_feat_idxs = [i for i in range(len(genes)) if ((np.random.uniform()<pct) or (genes[i] not in gt_feats))]
            else:
                f = open(new_feature_dict[temp_none_name],"r")
                temp_keep_feat_idxs = [gene_hash[g.strip()] for g in f.readlines()]
                f.close()
            temp_kept_gene_names = [genes[idx] for idx in temp_keep_feat_idxs]
            temp_keep_feat_idxs = np.array(temp_keep_feat_idxs)
            ######################
            ## write the no feature selection 
            if not os.path.isfile(new_feature_dict[temp_none_name]):
                write_feats(new_feature_dict[temp_none_name], temp_kept_gene_names)
            ######################
            ## do and write the anti-cor w/ default FDR = 1/15
            if not os.path.isfile(new_feature_dict[temp_anti_name]):
                print("doing ", pct, "fs")
                if not real_dset:
                    temp_anticor_table = get_anti_cor_genes(
                        exprs[temp_keep_feat_idxs,:],
                        temp_kept_gene_names,
                        pre_remove_pathways=[],
                        scratch_dir="/media/scott/ssd_2tb/scratch/"
                        )
                else:
                    temp_anticor_table = get_anti_cor_genes(
                        exprs[temp_keep_feat_idxs,:],
                        temp_kept_gene_names,
                        scratch_dir="/media/scott/ssd_2tb/scratch/"
                        )
                temp_anti_cor_feats = temp_anticor_table[temp_anticor_table["selected"]==True]["gene"].tolist()
                print("anticor feats total of:",len(temp_anti_cor_feats)/len(gt_feats), "percent real features")
                write_feats(new_feature_dict[temp_anti_name], temp_anti_cor_feats)
            ######################
            ## do and write the anti-cor w/ default FDR = 1/15
            if not os.path.isfile(new_feature_dict[temp_anti_name2]):
                print("doing ", pct, "fs; FDR:0.25")
                if not real_dset:
                    temp_anticor_table = get_anti_cor_genes(
                        exprs[temp_keep_feat_idxs,:],
                        temp_kept_gene_names,
                        pre_remove_pathways=[],
                        FDR=0.25,
                        scratch_dir="/media/scott/ssd_2tb/scratch2/")
                else:
                    temp_anticor_table = get_anti_cor_genes(
                        exprs[temp_keep_feat_idxs,:],
                        temp_kept_gene_names,
                        FDR=0.25,
                        scratch_dir="/media/scott/ssd_2tb/scratch2/"
                        )
                temp_anti_cor_feats = temp_anticor_table[temp_anticor_table["selected"]==True]["gene"].tolist()
                print("anticor feats total of:",len(temp_anti_cor_feats)/len(gt_feats), "percent real features")
                write_feats(new_feature_dict[temp_anti_name2], temp_anti_cor_feats)
    return({**feature_dict, **new_feature_dict})


def do_all_clustering_methods(
    iter_dir,
    real_dset=False,
    force=True):
    out_file = os.path.join(iter_dir,"merged_cluster_results.pkl")
    if os.path.isfile(out_file) and not force:
        return(import_dict(out_file))
    np.random.seed(123456)
    norm_dset_file = os.path.join(iter_dir,"anti_cor/expression.tsv")
    non_norm_dset_file=os.path.join(iter_dir,"expression_ds_norm_counts.tsv")
    gt_vect = pd.read_csv(os.path.join(iter_dir,"anti_cor/ground_truth_annotations.tsv"),sep="\t", header=None).iloc[:,1].tolist()
    feature_dict = {"none":None, "anticor":os.path.join(iter_dir,"anti_cor/overdispersed_genes.txt")}
    if not real_dset:
        gt_feat_df = pd.read_csv(os.path.join(iter_dir,"ground_truth_degs.tsv"),sep="\t")
        gt_feat_df_sig = gt_feat_df[gt_feat_df["deg"]==True]
        gt_feats = [thing.strip().replace('"','') for thing in gt_feat_df_sig["gene"].tolist()]
        gt_feats = ["Gene"+str(int(thing.replace("Gene",""))-1) for thing in gt_feats]
        ## now add the others
        feature_dict = add_more_fs(iter_dir, feature_dict, gt_feats)
    ## first we'll do the python methods
    fs_adata_dict, out_col_dict = do_python_cluster(iter_dir,
                        feature_dict,
                        norm_dset_file,
                        gt_vect,
                        real_dset=real_dset)
    for fs, fs_file in feature_dict.items():
        temp_out_file = os.path.join(iter_dir,fs+"_R_clusts.tsv")
        if not os.path.isfile(temp_out_file):
            do_clust_r(non_norm_dset_file, temp_out_file, feature_file=fs_file)
        temp_res = pd.read_csv(temp_out_file,sep="\t")
        temp_res["cell"] = temp_res["cell"].str.replace(".","-")
        temp_res.index = temp_res["cell"]
        new_cols = temp_res.columns[1:].tolist()
        print(fs)
        print(temp_res)
        print(temp_res.shape)
        new_obs = pd.concat([fs_adata_dict[fs].obs, temp_res[new_cols]], axis=1)
        print(new_obs)
        print(new_obs.shape)
        fs_adata_dict[fs].obs = new_obs
        print(fs_adata_dict[fs].obs)
        for col in new_cols:
            out_col_dict[col]=col
        print(out_col_dict)
    out_dict={"columns":out_col_dict,
              "results":{}}
    for fs, fs_file in feature_dict.items():
        out_dict["results"][fs]=fs_adata_dict[fs].obs
    save_dict(out_dict, out_file)
    return(out_dict)



#fs_adata_dict["anticor"].obs[["ground_truth"]+list(out_col_dict.values())]



####################################################################

data_dir = "data/scRNAseq_sim/"
out_dir = "data/scRNAseq_sim_clust_alg_compare/"
if not os.path.isdir(out_dir):
    os.mkdir(out_dir)

####
# prep the R environment by running empty once
#cmd("Rscript --vanilla bin/run_all_clustR_algs.R")

#### 

np.random.seed(123456)

all_results = {}
sim_methods = get_subdirs(data_dir)
for sim_method in sim_methods:
    temp_sim_meth = os.path.basename(sim_method)
    all_results[temp_sim_meth]={}
    print("\n\n\n",temp_sim_meth)
    clust_num_dirs = get_subdirs(sim_method)
    for clust_dir in clust_num_dirs:
        temp_num_clusts = os.path.basename(clust_dir)
        all_results[temp_sim_meth][temp_num_clusts]={}
        print("\t",temp_num_clusts)
        temp_iter_dirs = get_subdirs(clust_dir)
        for iter_dir in temp_iter_dirs:
            temp_iter = os.path.basename(iter_dir)
            print("\t\t",temp_iter)
            all_results[temp_sim_meth][temp_num_clusts][temp_iter]=do_all_clustering_methods(iter_dir)





res_obj = os.path.join(out_dir, "full_cluster_results2.pkl")
save_dict(all_results,res_obj)
all_results = import_dict(res_obj)


##############################################################################
##############################################################################
import numpy as np
from sklearn import metrics

def purity_score(y_true, y_pred):
    # compute contingency matrix (also called confusion matrix)
    contingency_matrix = metrics.cluster.contingency_matrix(y_true, y_pred)
    # return purity
    return np.sum(np.amax(contingency_matrix, axis=0)) / np.sum(contingency_matrix) 


def get_pct_remain_signal(fs_meth):
    if "remain" in fs_meth:
        ## first is the fs_method
        ## second is the percent remaining signal
        ## third is the word remain
        split_annotations = fs_meth.split("_")
        remain = split_annotations[1]
        fs_meth = split_annotations[0]
    else:
        remain = 1.
        fs_meth = fs_meth
    return(fs_meth, remain)


def analyze_single_cluster_res(temp_res, target_col, clust_meth, fs_meth, iter_num, sim_meth):
    #print(temp_res[target_col][0])
    true_num_groups = len(list(set(temp_res["ground_truth"])))
    fs_meth, percent_remain = get_pct_remain_signal(fs_meth)
    temp_df = pd.DataFrame({"sim":sim_meth,
                            "cluster_method":clust_meth,
                            "fs_method":fs_meth,
                            "percent_signal":percent_remain,
                            "fs_clust_method":fs_meth+"||"+clust_meth,
                            "num_clust":true_num_groups,
                            "guessed_num_clust":np.nan,
                            "abs_delta_clust_num":np.nan,
                            "purity":np.nan,
                            "rev_purity":np.nan,
                            "ari":np.nan,
                            "nMI":np.nan},
                            index = [sim_meth+"_"+fs_meth+"_"+clust_meth+"_"+str(true_num_groups)+"_"+iter_num])
    if len(temp_res[target_col].dropna())==0:
        return(temp_df)
    ####
    guessed_num_groups = len(list(set(temp_res[target_col])))
    temp_df["guessed_num_clust"]=guessed_num_groups
    group_number_delta = abs(guessed_num_groups-true_num_groups)
    temp_df["abs_delta_clust_num"]=group_number_delta
    #print("group_num_delta:",group_number_delta)
    ####
    temp_purity = purity_score(temp_res["ground_truth"], temp_res[target_col])
    temp_df["purity"]=temp_purity
    #print("purity:",temp_purity)
    temp_rev_purity = purity_score(temp_res[target_col], temp_res["ground_truth"])
    temp_df["rev_purity"]=temp_rev_purity
    #print("temp_rev_purity:",temp_rev_purity)
    temp_ari = metrics.adjusted_rand_score(temp_res["ground_truth"],  temp_res[target_col])
    temp_df["ari"]=temp_ari
    #print("ari:",temp_ari)
    temp_nmi = metrics.normalized_mutual_info_score(temp_res["ground_truth"], temp_res[target_col])
    temp_df["nMI"]=temp_nmi
    #print("nMI:",temp_nmi)
    return(temp_df)


def do_cluster_analysis(in_temp_dict,iter_num, sim_meth):
    all_results_local = None
    for clust_method, column in in_temp_dict["columns"].items():
        for fs_method, fs_df in in_temp_dict["results"].items():
            print("\n\n",fs_method,column)
            temp_res = analyze_single_cluster_res(fs_df, column, clust_method, fs_method,iter_num, sim_meth)
            if all_results_local is None:
                all_results_local = deepcopy(temp_res)
            else:
                all_results_local = pd.concat([all_results_local, temp_res],axis=0)
    return(all_results_local)


##############################################################################################
##############################################################################################
##############################################################################################

all_summary_results = None
for sim_meth in all_results.keys():
    for num_groups in all_results[sim_meth].keys():
        for iter_num in all_results[sim_meth][num_groups].keys():
            print("\n\n\n\n\n",sim_meth,"\n\t",num_groups,"\n\t\t",iter_num)
            temp_res = do_cluster_analysis(all_results[sim_meth][num_groups][iter_num],iter_num, sim_meth)
            if all_summary_results is None:
                all_summary_results = deepcopy(temp_res)
            else:
                all_summary_results = pd.concat([all_summary_results, temp_res],axis=0)


all_summary_results.to_csv(os.path.join(out_dir,"all_results.tsv"),sep="\t")
all_summary_results_good = all_summary_results.dropna()

#all_res_summary = all_summary_results_good.groupby(["sim","fs_clust_method"]).mean(numeric_only=True)
#all_res_summary = all_summary_results_good.groupby(["sim","fs_clust_method"]).mean(numeric_only=True)
all_res_summary = all_summary_results_good.groupby(["cluster_method"]).mean(numeric_only=True)
out_measures = ["purity","rev_purity", "ari", "nMI"]
m = all_res_summary[out_measures].mean(axis=1)
s = all_res_summary[out_measures].std(axis=1)
summary = pd.concat([m,s],axis=1)
summary.columns = ["means","StDevs"]
summary=summary.sort_values(by="means")
clust_order = summary.index.tolist()[::-1]




def get_stats(var_of_int, dset, out_dir=None):
    ## I hate how bloated this run-away code got, but it works and I'm not redoing it
    plt.clf()
    ## correct for everything except the cluster_method
    remove_mod = mod = smf.ols(formula = var_of_int+" ~ num_clust + sim + fs_method", data=dset)
    remove_res = remove_mod.fit()
    clust_corrected_name = var_of_int+"_corrected_clust"
    dset[clust_corrected_name] = remove_res.resid_pearson
    ## correct for everything except the fs_method
    remove_mod = mod = smf.ols(formula = var_of_int+" ~ num_clust + sim + cluster_method", data=dset)
    remove_res = remove_mod.fit()
    fs_corrected_name = var_of_int+"_corrected_fs"
    dset[fs_corrected_name] = remove_res.resid_pearson
    ######
    mod = smf.ols(formula = var_of_int+" ~ num_clust + sim + percent_signal + cluster_method + fs_method", data=dset)
    res = mod.fit()
    resgression_text = str(res.summary())
    if out_dir is None:
        print(resgression_text)
    else:
        f = open(os.path.join(out_dir,var_of_int+"_model_results.txt"),"w")
        f.writelines(resgression_text)
        f.close()
    ########
    clust_meds = dset.groupby(["cluster_method","percent_signal"])[clust_corrected_name].median().reset_index()
    ordered_clusts = []
    for thing in dset["cluster_method"].tolist():
        if thing not in ordered_clusts:
            ordered_clusts.append(thing)
    order_dict = {val:key for key, val in enumerate(ordered_clusts)}
    new_idx = []
    for i in range(len(clust_meds.index)):
        new_idx.append(order_dict[clust_meds.iloc[i,0]])
    clust_meds.index = new_idx
    clust_meds = clust_meds.iloc[np.argsort(clust_meds.index),:]
    print(clust_meds)
    ########
    sns.scatterplot(x=np.array(dset["percent_signal"],dtype=np.float32)+np.random.uniform(size=dset.shape[0])/10,
    y=clust_corrected_name,
    hue="cluster_method", 
    s=10,
    data = dset)
    ## add the medians
    sns.scatterplot(x=np.array(clust_meds["percent_signal"],dtype=np.float32)-.08+np.random.uniform(size=clust_meds.shape[0])/20,
    y=clust_corrected_name,
    hue="cluster_method", 
    s=50,
    legend = False,
    data = clust_meds)
    plt.xlabel("Percent of original signal in simulation")
    plt.legend(bbox_to_anchor=(1.01, 1),
           borderaxespad=0)
    plt.title(var_of_int)
    plt.tight_layout()
    if out_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(out_dir,var_of_int+"_cluster_method_results.png"), dpi=350)
    ########
    clust_meds = dset.groupby(["fs_method","percent_signal"])[fs_corrected_name].median().reset_index()
    ordered_clusts = []
    for thing in dset["fs_method"].tolist():
        if thing not in ordered_clusts:
            ordered_clusts.append(thing)
    order_dict = {val:key for key, val in enumerate(ordered_clusts)}
    new_idx = []
    for i in range(len(clust_meds.index)):
        new_idx.append(order_dict[clust_meds.iloc[i,0]])
    clust_meds.index = new_idx
    clust_meds = clust_meds.iloc[np.argsort(clust_meds.index),:]
    print(clust_meds)
    ########
    plt.clf()
    sns.scatterplot(x=np.array(dset["percent_signal"],dtype=np.float32)+np.random.uniform(size=dset.shape[0])/10,
    y=fs_corrected_name,
    hue="fs_method", 
    s=10,
    data = dset)
    ## add the medians
    sns.scatterplot(x=np.array(clust_meds["percent_signal"],dtype=np.float32)-.08+np.random.uniform(size=clust_meds.shape[0])/20,
    y=fs_corrected_name,
    hue="fs_method", 
    s=50,
    legend = False,
    data = clust_meds)
    plt.xlabel("Percent of original signal in simulation")
    plt.legend(bbox_to_anchor=(1.01, 1),
       borderaxespad=0)
    plt.title(var_of_int)
    plt.tight_layout()
    if out_dir is None:
        plt.show()
    else:
        plt.savefig(os.path.join(out_dir,var_of_int+"_fs_method_results.png"), dpi=350)



get_stats("nMI",all_summary_results_good, out_dir)
get_stats("ari",all_summary_results_good, out_dir)
get_stats("purity",all_summary_results_good, out_dir)
get_stats("rev_purity",all_summary_results_good, out_dir)




sns.scatterplot(x=np.array(all_summary_results_good_sergio["percent_signal"],dtype=np.float32)+np.random.uniform(size=all_summary_results_good_sergio.shape[0])/10,
    y="nMI",
    hue="cluster_method", 
    data = all_summary_results_good_sergio)
plt.show()

sns.scatterplot(x=np.array(all_summary_results_good_sergio["percent_signal"],dtype=np.float32)+np.random.uniform(size=all_summary_results_good_sergio.shape[0])/10,
    y="purity",
    hue="cluster_method", 
    data = all_summary_results_good_sergio)
plt.show()


sns.scatterplot(x=np.array(all_summary_results_good_sergio["percent_signal"],dtype=np.float32)+np.random.uniform(size=all_summary_results_good_sergio.shape[0])/10,
    y="rev_purity",
    hue="cluster_method", 
    data = all_summary_results_good_sergio)
plt.show()

sns.scatterplot(x=np.array(all_summary_results_good_sergio["percent_signal"],dtype=np.float32)+np.random.uniform(size=all_summary_results_good_sergio.shape[0])/10,
    y="ari",
    hue="cluster_method", 
    data = all_summary_results_good_sergio)
plt.show()




