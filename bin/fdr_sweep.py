import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from anticor_features.anticor_features import get_anti_cor_genes



def get_subdirs(top_dir):
	blah = [ f.path for f in os.scandir(top_dir) if f.is_dir() ]
	return(blah)

# get_anti_cor_genes(exprs,
#                        feature_ids,
#                        pre_remove_features=[],
#                        pre_remove_pathways=None,
#                        species="hsapiens",
#                        n_rand_feat=5000,
#                        FPR = 0.001,
#                        FDR = 1/15,
#                        num_pos_cor = 10,
#                        min_express_n=None,
#                        bin_size=5000,
#                        scratch_dir=None,
#                        cell_axis=1)

def load_dset(in_file):
	exprs = pd.read_csv(os.path.join(iter_dir,"expression_ds_norm_counts.tsv"),
		                sep="\t")
	exprs.index=["Gene"+str(i) for i in range(1,exprs.shape[0]+1)]
	c = exprs.columns[1:].tolist()
	return(exprs.index.tolist(), c, exprs.iloc[:,1:].to_numpy())




def mcc(TP, TN, FP, FN):
    return((TP*TN - FP*FN) / np.sqrt((TP+FP)*(TP+FN)*(TN+FP)*(TN+FN)))


def acc(TP, TN, FP, FN):
    return((TP+TN)/(FP+FN+TP+TN))


def f1_score(TP, TN, FP, FN):
    return(2*TP/(2*TP+FP+FN))


def add_other_metrics(in_df):
    in_df["precision"]=in_df["TP"]/(in_df["TP"]+in_df["FP"])
    in_df["TPR"]=in_df["TP"]/(in_df["TP"]+in_df["FN"])
    in_df["TNR"]=in_df["TN"]/(in_df["TN"]+in_df["FP"])
    in_df["FPR"]=in_df["FP"]/(in_df["FP"]+in_df["TN"])
    in_df["FNR"]=in_df["FN"]/(in_df["FN"]+in_df["TP"])
    in_df["FDR"]=in_df["FP"]/(in_df["FP"]+in_df["TP"])
    in_df["FOR"]=in_df["FN"]/(in_df["FN"]+in_df["TN"])
    in_df["ACC"]=acc(in_df["TP"], in_df["TN"], in_df["FP"], in_df["FN"])
    in_df["BA"]= (in_df["TPR"] + in_df["TNR"]) / 2 
    in_df["MCC"]=mcc(in_df["TP"], in_df["TN"], in_df["FP"], in_df["FN"])
    in_df["F1_score"]=f1_score(in_df["TP"], in_df["TN"], in_df["FP"], in_df["FN"])
    return in_df


def get_confusion(gt_deg_annotations, selected_genes, all_genes):
	gt_deg_annotations.index = gt_deg_annotations["gene"]
	gt_deg_annotations=gt_deg_annotations.loc[all_genes,:]
	print(gt_deg_annotations.head())
	num_total_ps = len(gt_deg_annotations[gt_deg_annotations["deg"]==True]["gene"])
	num_total_ns = gt_deg_annotations.shape[0]-num_total_ps
	selected_annotations = gt_deg_annotations.loc[selected_genes,"deg"]
	tps = np.sum(np.array(selected_annotations==True))
	fps = len(selected_annotations)-tps
	fns = num_total_ps - tps
	tns = num_total_ns - fps
	print("TP:",tps)
	print("FP:",fps)
	print("FN:",fns)
	print("TN:",tns)
	print("sum:",tps+fps+fns+tns)
	return(tps,fps,fns,tns)


def run_sweep_on_dir(iter_dir, fpr, fdr,
					gt_deg_annotations,
					genes, cells, exprs):
	anticor_res = get_anti_cor_genes(exprs, genes, pre_remove_pathways=[],
		                             FPR = fpr,
		                             FDR = fdr,
		                             scratch_dir="/media/scott/ssd_2tb/scratch/")
	print(anticor_res.head())
	selected_genes = anticor_res[anticor_res["selected"]==True]["gene"]
	conf_res = get_confusion(gt_deg_annotations, selected_genes,gt_deg_annotations["gene"])
	return(conf_res)


####################################################################

data_dir = "data/scRNAseq_sim/"
out_dir = "data/scRNAseq_sim_results_FDRsweep/"
fpr_sweep = [0.1,.01,.001]
fdr_sweep = [.99,0.5,0.25, 1/15, 0.1, 0.01]



if not os.path.isdir(out_dir):
	os.mkdir(out_dir)



np.random.seed(123456)


all_methods = get_subdirs(data_dir)
meth_vect=[]
clust_num_vect = []
iter_vect = []
fpr_vect = []
fdr_vect = []
tp_vect = []
fp_vect = []
fn_vect = []
tn_vect = []
ID_vect = []



for method_dir in all_methods:
	temp_meth = os.path.basename(method_dir)
	all_clust_num = get_subdirs(method_dir)
	clust_num = os.path.basename(all_clust_num[0])
	for n_clust in all_clust_num:
		temp_n_clust = os.path.basename(n_clust)
		local_data_dir = get_subdirs(n_clust)
		for iter_dir in local_data_dir:
			temp_iter = os.path.basename(iter_dir)
			gt_deg_annotations = pd.read_csv(os.path.join(iter_dir,"ground_truth_degs.tsv"),sep="\t")
			genes, cells, exprs = load_dset(os.path.join(iter_dir,"expression_ds_norm_counts.tsv"))
			for temp_fpr in fpr_sweep:
				for temp_fdr in fdr_sweep:
					print(temp_fdr)
					temp_ID = "||".join([temp_meth, temp_n_clust, temp_iter,str(round(temp_fpr,5)),str(round(temp_fdr,5))])
					if temp_ID not in ID_vect:
						print("\n\n\n",temp_meth)
						print("\t",temp_n_clust)
						print("\t\t",temp_iter)
						print("\t\tFPR:",temp_fpr)
						print("\t\tFDR:",temp_fdr)
						iter_res = run_sweep_on_dir(iter_dir,temp_fpr, temp_fdr,
							gt_deg_annotations,
							genes, cells, exprs)
						meth_vect.append(temp_meth)
						clust_num_vect.append(temp_n_clust)
						iter_vect.append(temp_iter)
						fpr_vect.append(temp_fpr)
						fdr_vect.append(temp_fdr)
						tp_vect.append(iter_res[0])
						fp_vect.append(iter_res[1])
						fn_vect.append(iter_res[2])
						tn_vect.append(iter_res[3])
						ID_vect.append(temp_ID)


out_df = pd.DataFrame({"ID":ID_vect,
	"sim_method":meth_vect,
	"n_clust":clust_num_vect,
	"iter":iter_vect,
	"anticor_FPR":fpr_vect,
	"anticor_FDR":fdr_vect,
	"TP":tp_vect,
	"FP":fp_vect,
	"FN":fn_vect,
	"TN":tn_vect})


out_df = add_other_metrics(out_df)
out_df.index = out_df["ID"]
out_df.to_csv(os.path.join(out_dir,"full_param_sweep_results.tsv"),sep="\t")




def do_3d_plot(out_df, z_var):
	plt.clf()
	axes = plt.axes(projection='3d')
	axes.set_xlabel('log10(FPR)')
	axes.set_ylabel('log10(FDR)')
	axes.set_zlabel(z_var)
	edge_cols = ["k","r"]
	sim_meth = ["splatter","sergio"]
	for i in range(len(sim_meth)):
		sub_df = out_df[out_df["sim_method"]==sim_meth[i]]
		print(sub_df.shape)
		axes.scatter3D(np.log10(sub_df["anticor_FPR"]), 
			           np.log10(sub_df["anticor_FDR"]), 
			           sub_df[z_var],
			           c=sub_df[z_var], 
			           edgecolors=edge_cols[i])
		for temp_fp in set(sub_df["anticor_FPR"].tolist()):
			sub_sub_df = sub_df[sub_df["anticor_FPR"]==temp_fp]
			all_x = []
			all_y = []
			all_z = []
			for temp_fd in sorted(list(set(sub_sub_df["anticor_FDR"].tolist()))):
				sub_sub_sub_df = sub_sub_df[sub_sub_df["anticor_FDR"]==temp_fd]
				all_x.append(np.log10(temp_fp))
				all_y.append(np.log10(temp_fd))
				all_z.append(np.mean(sub_sub_sub_df[z_var]))
			axes.plot(all_x, all_y, all_z, c = edge_cols[i], linestyle='dashed')
	return()


def get_padded_lims(temp_df, in_var, pad_percent = 0.05):
	min_val = np.min(temp_df[in_var])
	max_val = np.max(temp_df[in_var])
	pad = (max_val - min_val)*pad_percent
	return(min_val-pad,max_val+pad)


def do_3_panel_plot(out_df, z_var):
	plt.close("all")
	plt.clf()
	edge_cols = ["k","r"]
	sim_meth = ["splatter","sergio"]
	all_fprs = sorted(list(set(out_df["anticor_FPR"].tolist())))
	fig, ax = plt.subplots(len(all_fprs))
	plt.subplots_adjust (left=0.185, right=0.95, bottom=.15)
	fig.suptitle(z_var)
	y_min, y_max = get_padded_lims(out_df, z_var)
	x_min = np.log10(np.min(out_df["anticor_FDR"]))
	x_max = np.log10(np.max(out_df["anticor_FDR"]))
	## make panels
	for fpr_idx in range(len(all_fprs)):
		temp_fp = all_fprs[fpr_idx]
		sub_df = out_df[out_df["anticor_FPR"]==temp_fp]
		print(sub_df.head())
		#ax[fpr_idx].yaxis.set_label_position("right")
		ax[fpr_idx].set_ylabel("FPR\n"+str(round(temp_fp,3)), rotation=0, labelpad=20)
		ax[fpr_idx].set_ylim(y_min, y_max)
		ax[fpr_idx].set_xlim(x_min, x_max)
		for i in range(len(sim_meth)):
			sub_sub_df = sub_df[sub_df["sim_method"]==sim_meth[i]]
			print(sub_sub_df.shape)
			ax[fpr_idx].scatter(np.log10(sub_sub_df["anticor_FDR"]), 
				           sub_sub_df[z_var],
				           c=sub_sub_df[z_var], 
				           edgecolors=edge_cols[i])
			all_x = []
			all_y = []
			all_z = []
			for temp_fd in sorted(list(set(sub_sub_df["anticor_FDR"].tolist()))):
				sub_sub_sub_df = sub_sub_df[sub_sub_df["anticor_FDR"]==temp_fd]
				all_x.append(np.log10(temp_fd))
				all_y.append(np.mean(sub_sub_sub_df[z_var]))
			ax[fpr_idx].plot(all_x, all_y, c = edge_cols[i], linestyle='dashed')
		#for temp_fp in set(out_df["anticor_FPR"].tolist()):
	for i in range(len(all_fprs)-1):
		ax[i].tick_params(
		    axis='x',          # changes apply to the x-axis
		    which='both',      # both major and minor ticks are affected
		    bottom=True,      # ticks along the bottom edge are off
		    top=False,         # ticks along the top edge are off
		    labelbottom=False)
	ax[-1].set_xlabel("-log10(FDR)")
	plt.rc("font",size=15)
	return()




for thing in ['precision', 'TPR', 'TNR', 'FPR', 'FNR', 'FDR','FOR', 'ACC', 'BA', 'MCC', 'F1_score']:
	do_3_panel_plot(out_df, thing)
	plt.savefig(os.path.join(out_dir, thing+".png"),dpi=300)

