import os
import sys
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from anticor_features.anticor_features import get_anti_cor_genes

######################################
# helper functions

def get_subdirs(top_dir):
	blah = [ f.path for f in os.scandir(top_dir) if f.is_dir() ]
	return(blah)


def load_dset(in_file):
	exprs = pd.read_csv(os.path.join(iter_dir,"expression_ds_norm_counts.tsv"),
		                sep="\t")
	exprs.index=["Gene"+str(i) for i in range(1,exprs.shape[0]+1)]
	c = exprs.columns[1:].tolist()
	return(exprs.index.tolist(), c, exprs.iloc[:,1:].to_numpy())

######################################


data_dir = "data/scRNAseq_simElbow/"
out_dir = "data/scRNAseq_sim_Null_FDRsweep/"
fpr_sweep = [0.1,.01,.001]
fdr_sweep = [.99,0.5,0.25, 1/15, 0.1, 0.01]



if not os.path.isdir(out_dir):
	os.mkdir(out_dir)



np.random.seed(123456)


clust_num_vect = []
n_selected = []
n_not_selected = []
pass_vect = []
iter_vect = []
fpr_vect = []
fdr_vect = []
ID_vect = []





n_clust = "1"
temp_n_clust = os.path.basename(n_clust)
local_data_dir = get_subdirs(os.path.join(data_dir,n_clust))
for iter_dir in local_data_dir:
	temp_iter = os.path.basename(iter_dir)
	genes, cells, exprs = load_dset(os.path.join(iter_dir,"expression_ds_norm_counts.tsv"))
	for temp_fpr in fpr_sweep:
		for temp_fdr in fdr_sweep:
			temp_ID = "||".join([temp_n_clust, temp_iter,str(round(temp_fpr,5)),str(round(temp_fdr,5))])
			if temp_ID not in ID_vect:
				print("\n\n\n")
				print("\t",temp_n_clust)
				print("\t\t",temp_iter)
				print("\t\tFPR:",temp_fpr)
				print("\t\tFDR:",temp_fdr)
				temp_res = get_anti_cor_genes(exprs, genes, pre_remove_pathways=[], FPR=temp_fpr, FDR=temp_fdr)
				n_selected.append(np.sum(temp_res["selected"]==True))
				n_not_selected.append(np.sum(temp_res["selected"]==False))
				if n_selected[-1] == 0:
					pass_vect.append(True)
				else:
					pass_vect.append(False)
				## House keeping
				iter_vect.append(temp_iter)
				fpr_vect.append(temp_fpr)
				fdr_vect.append(temp_fdr)
				ID_vect.append(temp_ID)



null_fpr_sweep_df = pd.DataFrame({
	"parameters":ID_vect,
	"iteration":iter_vect,
	"FPR_param":fpr_vect,
	"FDR_param":fdr_vect,
	"n_selected":n_selected,
	"n_not_selected":n_not_selected,
	"pass_null":pass_vect
})

if not os.path.isdir(out_dir):
	sys.mkdir(out_dir)

# Save the result
null_fpr_sweep_df.to_csv(os.path.join(out_dir, "full_param_sweep_null_results.tsv"),sep="\t")


###########
# First, we will convert the boolean values of 'pass_null' to integers (1=True, 0=False)
# This will allow us to perform calculations like mean, which will effectively give us the pass percentage
null_fpr_sweep_df['pass_null'] = null_fpr_sweep_df['pass_null'].astype(int)

# Now we group by 'FPR_param' and 'FDR_param' and calculate the mean of 'pass_null'
grouped_df = null_fpr_sweep_df.groupby(['FPR_param', 'FDR_param'])['pass_null'].mean()

# The mean of 'pass_null' in each group will give us the pass percentage
# However, this will return a Series. To convert this back to a DataFrame:
grouped_df = grouped_df.reset_index()

# We might want the pass percentage in percentage form, not decimal
grouped_df['pass_null'] = grouped_df['pass_null'] * 100

grouped_df.rename(columns={'pass_null': 'pass_percentage'}, inplace=True)

grouped_df['FDR_param'] = round(grouped_df['FDR_param'],3)

# Creating a heatmap for pass_percentage
plt.figure(figsize=(10, 8))
sns.heatmap(grouped_df.pivot('FPR_param', 'FDR_param', 'pass_percentage'), annot=True, fmt=".1f")#, cmap="YlGnBu")
plt.title('Pass Percentage Heatmap for each FPR_param and FDR_param combination')
plt.savefig(os.path.join(out_dir,"pass_percentage_heatmap.png"))

# Creating a scatter plot for n_selected
plt.figure(figsize=(10, 8))
scatter_plot = sns.scatterplot(x='FDR_param', y='FPR_param', size='n_selected', data=grouped_df, sizes=(20, 200))
plt.title('Mean n_selected Scatter plot for each FPR_param and FDR_param combination')
plt.savefig(os.path.join(out_dir,"pass_n_selected_scatter.png"))

# Adjust values to handle log(0)
grouped_df['n_selected_log'] = np.log(grouped_df['n_selected'] + 1)

# Creating a heatmap for n_selected on log scale
plt.figure(figsize=(10, 8))
sns.heatmap(grouped_df.pivot('FPR_param', 'FDR_param', 'n_selected_log'), annot=True, fmt=".1f")#, cmap="YlGnBu")
plt.title('Log of Mean n_selected Heatmap for each FPR_param and FDR_param combination')
plt.savefig(os.path.join(out_dir,"log_n_selected_heatmap.png"))


# Creating a heatmap for n_selected on log scale
plt.figure(figsize=(10, 8))
sns.heatmap(grouped_df.pivot('FPR_param', 'FDR_param', 'n_selected'), annot=True, fmt=".1f")#, cmap="YlGnBu")
plt.title('Mean n_selected Heatmap for each FPR_param and FDR_param combination')
plt.savefig(os.path.join(out_dir,"n_selected_heatmap.png"))

