import os
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
from copy import deepcopy
from matplotlib import pyplot as plt
from scipy.stats import mstats
from scipy.stats.mstats import spearmanr as spear


out_dir = "data/pbmcs/ref_compare_and_detailed_plots"
in_file_dir = "data/pbmcs/misc_files"
if not os.path.isdir(out_dir):
	sys.mkdir(out_dir)


# Download this from this paper:
# https://www.nature.com/articles/s41586-021-04345-x
# You will need to replace
ref_dir = "/media/scott/ssd_2tb/pbmc_ref"
adata = sc.read(os.path.join(ref_dir,"local.h5ad"))


# create the interaction between disease status and cell type in case it makes a diff
disease_dict = {'COVID-19':"covid", 'normal':"ctrl", 'post-COVID-19 disorder':"post-covid"}
disease_cell_type=[]
for idx, row in adata.obs.iterrows():
	disease_cell_type.append(disease_dict[row["disease"]]+"|"+row["cell_type"])


disease_cell_type_df = pd.DataFrame({
	"disease":adata.obs["disease"],
	"cell_type":adata.obs["cell_type"],
	"disease_cell_type":disease_cell_type},
	index = adata.obs.index)



out_means = pd.DataFrame({}, index = adata.var["name"])

# for ct in set(adata.obs["cell_type"]):
# 	out_means[ct]=np.mean(adata[adata.obs["cell_type"]==ct].X.T, axis=1)

cor_key = "cell_type"

for ct in set(disease_cell_type_df[cor_key]):
	out_means[ct]=np.mean(adata[disease_cell_type_df[cor_key]==ct].X.T, axis=1)


out_means.to_csv(os.path.join(out_dir,"cell_type_means.tsv"),sep="\t")



our_data_means = pd.read_csv(os.path.join(in_file_dir,"mean_clust_exprs.tsv"),sep="\t")
our_data_means.index  = our_data_means.iloc[:,0]
our_data_means = our_data_means.iloc[:,1:]

selected_features = pd.read_csv(os.path.join(in_file_dir,"selected_features.txt"),header=None).iloc[:,0].tolist()
selected_features = [feat for feat in selected_features if (feat in our_data_means.index and feat in out_means.index)]


spear_array = np.zeros((our_data_means.shape[1], out_means.shape[1]))
for i in range(our_data_means.shape[1]):
	for j in range(out_means.shape[1]):
		rho, p = spear(np.array(our_data_means.loc[selected_features,:])[:,i], np.array(out_means.loc[selected_features,:])[:,j])
		spear_array[i,j]=rho



spear_df = pd.DataFrame(spear_array, index = our_data_means.columns, columns = out_means.columns)



norm_spear_df = deepcopy(spear_df)
for i in range(norm_spear_df.shape[0]):
	norm_spear_df.iloc[i,:]-=np.min(norm_spear_df.iloc[i,:])
	norm_spear_df.iloc[i,:]/=np.max(norm_spear_df.iloc[i,:])



sns.clustermap(spear_df.T,  yticklabels=True)
##plt.subplots_adjust (left=0.05, right=0.75, bottom=.05, top = .9)
#plt.savefig("spearm_cors_with_ref.png",dpi=350)
plt.show()


sns.clustermap(norm_spear_df.T,  yticklabels=True)
#plt.subplots_adjust (left=0.05, right=0.75, bottom=.05, top = .9)
#plt.savefig("norm_spearm_cors_with_ref.png",dpi=350)
plt.show()


TRAj = [thing for thing in out_means.index[out_means.index.str.startswith("TRAJ")] if thing in our_data_means.index]
TRBj = [thing for thing in out_means.index[out_means.index.str.startswith("TRBJ")] if thing in our_data_means.index]
TRDj = [thing for thing in out_means.index[out_means.index.str.startswith("TRDJ")] if thing in our_data_means.index]
TRGj = [thing for thing in out_means.index[out_means.index.str.startswith("TRGJ")] if thing in our_data_means.index]

our_data_means.loc[TRAj,:].sum(0)
our_data_means.loc[TRBj,:].sum(0)
our_data_means.loc[TRDj,:].sum(0)
our_data_means.loc[TRGj,:].sum(0)

