import os
import gc
import numpy as np
import pandas as pd
import scanpy as sc
from copy import deepcopy
from matplotlib import pyplot as plt
gc.enable()

# You can download this from our figshare deposit
#adata=sc.read('/media/scott/ssd_2tb/pbmc_ref/million_pbmc/adata_log_rle_norm_annotated.h5ad' )
adata_dir = "/media/scott/ssd_2tb/pbmc_ref/million_pbmc/"
adata=sc.read(os.path.join(adata_dir,"adata_log_rle_norm_annotated_analyzed.h5ad"))



def do_rank_genes(adata, groups, ref, new_key, force=False, cluster_key="cluster"):
	print(new_key, new_key in adata.uns.keys())
	if new_key not in adata.uns.keys() or force:
		sc.tl.rank_genes_groups(
			adata, 
			cluster_key, 
			groups=groups, 
			reference=ref, 
			method='wilcoxon')
		adata.uns[new_key] = adata.uns['rank_genes_groups']
	else:
		pass


def get_rank_genes(adata, 
	               group, 
	               temp_parent, 
	               out_merged_df = None, 
	               local_running_markers = []):
	poi_str = ",".join(list(map(str,group)))
	key = 'rank_genes_groups_'+poi_str+'_vs'+str(temp_parent)
	adata.uns['rank_genes_groups']=adata.uns[key]
	temp_df = sc.get.rank_genes_groups_df(adata,group=group)
	local_running_markers += temp_df.head(5)["names"].tolist() + temp_df.tail(5)["names"].tolist()
	temp_df["reference"]=str(temp_parent)
	if out_merged_df is None:
		out_merged_df = deepcopy(temp_df)
	else:
		out_merged_df = pd.concat([out_merged_df, temp_df])
	return(out_merged_df, local_running_markers)



def clean_markers(in_list, ban=["MALAT1", "XIST", "UTY"]):
	out_list = []
	for thing in in_list:
		if thing not in ban and thing not in out_list:
			out_list.append(thing)
	return(out_list)



def get_diffs_from_parent_pop(
	adata,
	pops_of_interest,
	parent_pops,
	cluster_key = "cluster",
	force = False):
	###
	parent_str_list = list(map(str,parent_pops))
	poi_str = ",".join(list(map(str,pops_of_interest)))
	# Do all the DEGs
	for temp_parent in parent_pops:
		print("doing DEG analysis on:",poi_str, "vs",str(temp_parent))
		try:
			do_rank_genes(adata, 
				[np.int64(poi) for poi in pops_of_interest], 
				np.int64(temp_parent), 
				'rank_genes_groups_'+poi_str+'_vs'+str(temp_parent),
				cluster_key=cluster_key,
				force= force)
		except:
			do_rank_genes(adata, 
				[str(poi) for poi in pops_of_interest], 
				str(temp_parent), 
				'rank_genes_groups_'+poi_str+'_vs'+str(temp_parent),
				cluster_key=cluster_key,
				force= force)
	#
	# Initialize on the first ref group
	local_running_markers=[]
	out_class_df, local_running_markers = get_rank_genes(
		adata, 
		group=list(map(str,pops_of_interest)), 
		temp_parent=parent_pops[0]
	)
	# Go through all the rest, appending the results
	for temp_parent in parent_pops[1:]:
		out_class_df, running_markers = get_rank_genes(
			adata, 
			group=list(map(str,pops_of_interest)), 
			temp_parent=temp_parent,
			out_merged_df=out_class_df,
			local_running_markers=local_running_markers
		)
	# Get the averages
	avg_class_df = out_class_df.groupby('names')[['scores', 'logfoldchanges', 'pvals', 'pvals_adj']].mean().reset_index()
	avg_class_df = avg_class_df.sort_values(by='scores', ascending=False)
	try:
		adata_subset = adata[adata.obs[cluster_key].isin(np.array(pops_of_interest+parent_pops,dtype=np.int64))]
	except:
		adata_subset = adata[adata.obs[cluster_key].isin(np.array(pops_of_interest+parent_pops,dtype=str))]
	print("getting the individual group markers relative to each other")
	sc.tl.rank_genes_groups(adata_subset, cluster_key, method='wilcoxon',key_added = "wilcoxon")
	for grp in list(map(str,pops_of_interest))+list(map(str,parent_pops)):
		local_running_markers=[sc.get.rank_genes_groups_df(adata_subset, group=[grp], key = "wilcoxon")["names"][0]]+local_running_markers
	local_running_markers = clean_markers(local_running_markers)
	return out_class_df, local_running_markers, avg_class_df, adata_subset


#########################
## Do analysis on classical monocytes
out_class_mono_df, running_markers, avg_class_mono_df, adata_class_mono = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[5,8],# 5 same as 8
	parent_pops=[10,19,20]
)

adata_class_mono.write(os.path.join(adata_dir,"adata_class_mono_orig_clust.h5ad"))

sc.pl.rank_genes_groups(adata_class_mono, n_genes=25, sharey=False, key = "wilcoxon")
sc.pl.dotplot(adata_class_mono, running_markers, groupby='cluster')#, show=False)


#########################
## Check if the other populations are different from each other as well

out_class_mono_df1, running_markers1, avg_class_mono_df1, adata_class_mono1 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[10],
	parent_pops=[19,20]
)



out_class_mono_df2, running_markers2, avg_class_mono_df2, adata_class_mono2 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[19],
	parent_pops=[20]
)


sc.pl.rank_genes_groups(adata_class_mono1, n_genes=25, sharey=False, key = "wilcoxon")
sc.pl.dotplot(adata_class_mono1, running_markers1, groupby='cluster')#, show=False)


sc.pl.rank_genes_groups(adata_class_mono2, n_genes=25, sharey=False, key = "wilcoxon")
sc.pl.dotplot(adata_class_mono2, running_markers2, groupby='cluster')#, show=False)

## Good news everyone! These ones all seem different from each other based on biology...

################################
# cluster 1 looks fairly different, but similar enough to test
out_class_mono_df31, running_markers31, avg_class_mono_df31, adata_class_mono31 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[1],
	parent_pops=[19]
)



##
out_class_mono_df32, running_markers32, avg_class_mono_df32, adata_class_mono32 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[1],
	parent_pops=[20]
)


#
out_class_mono_df33, running_markers33, avg_class_mono_df33, adata_class_mono33 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[1],
	parent_pops=[10]
)


##
out_class_mono_df34, running_markers34, avg_class_mono_df34, adata_class_mono34 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[1],
	parent_pops=[5]
)

# sc.pl.rank_genes_groups(adata_class_mono31, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_class_mono31, running_markers31, groupby='cluster')#, show=False)


# sc.pl.rank_genes_groups(adata_class_mono32, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_class_mono32, running_markers32, groupby='cluster')#, show=False)


# sc.pl.rank_genes_groups(adata_class_mono33, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_class_mono33, running_markers32, groupby='cluster')#, show=False)


# sc.pl.rank_genes_groups(adata_class_mono34, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_class_mono34, running_markers34, groupby='cluster')#, show=False)


## Yes, it looks different than all others



################################





out_cd4em_df, cd4em_running_markers, avg_cd4em_df, adata_cd4em = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[12,21],# 12 is the same as 21
	parent_pops=[17]
)


adata_cd4em.write(os.path.join(adata_dir,"adata_cd4em_orig_clust.h5ad"))

#sc.pl.rank_genes_groups(adata_cd4em, n_genes=25, sharey=False, key = "wilcoxon")
#sc.pl.dotplot(adata_cd4em, cd4em_running_markers, groupby='cluster')#, show=False)


##########################################################################################################
##########################################################################################################
## Naive CD4 different is 12, comparison group is 25/23
out_cdfour_df, cdfour_running_markers, avg_cdfour_df, adata_cdfour = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[13],
	parent_pops=[25,23]# 23 is the same as 25
)


#sc.pl.rank_genes_groups(adata_cdfour, n_genes=25, sharey=False, key = "wilcoxon")
#sc.pl.dotplot(adata_cdfour, cdfour_running_markers, groupby='cluster')#, show=False)



## Naive CD4 different is 12, comparison group is 25/23
out_cdfour_dfx, cdfour_running_markersx, avg_cdfour_dfx, adata_cdfourx = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[13],
	parent_pops=[23]# 23 is the same as 25
)

## 13 looks the same as 23
#sc.pl.rank_genes_groups(adata_cdfourx, n_genes=25, sharey=False, key = "wilcoxon")
#sc.pl.dotplot(adata_cdfourx, cdfour_running_markersx, groupby='cluster')#, show=False)


## Naive CD4 different is 12, comparison group is 25/23
out_cdfour_dfxx, cdfour_running_markersxx, avg_cdfour_dfxx, adata_cdfourxx = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[13],
	parent_pops=[25]
)

## 13 looks different than 25
# sc.pl.rank_genes_groups(adata_cdfourxx, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_cdfourxx, cdfour_running_markersxx, groupby='cluster')#, show=False)


## 23 and 25 look different
out_cdfour_dfxxx, cdfour_running_markersxxx, avg_cdfour_dfxxx, adata_cdfourxxx = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[23],
	parent_pops=[25]# 23 is the same as 25
)


# sc.pl.rank_genes_groups(adata_cdfourxxx, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_cdfourxxx, cdfour_running_markersxxx, groupby='cluster')#, show=False)

#################################################
## Check the B-cells
## These weren't different between T1D/ctrl, but 
## they look similar transcriptionally, so checking anyway
out_b_df, b_running_markers, avg_b_df, adata_b = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[2],
	parent_pops=[15]
)

# They are indeed different. NFKB is much higher in 15


# sc.pl.rank_genes_groups(adata_b, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_b, b_running_markers, groupby='cluster')#, show=False)



##########################################################################################################
##########################################################################################################


## 
out_cd4em2_df, cd4em2_running_markers, avg_cd4em2_df, adata_cd4em2 = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=[9],
	parent_pops=[6]# 9 is the same as 6
)


# sc.pl.rank_genes_groups(adata_cd4em2, n_genes=25, sharey=False, key = "wilcoxon")
# sc.pl.dotplot(adata_cd4em2, cd4em2_running_markers, groupby='cluster')#, show=False)



def standardize(in_vect):
	in_vect -=np.mean(in_vect)
	in_vect /=np.std(in_vect)
	return

#sc.pl.dotplot(adata,["MS4A1","LYZ","GNLY","FCGR3A","CD14","CD3E","CD3G","CD8A","CD8B","CD4"], groupby='cluster')

###################################################
###################################################
###################################################
## manual mergers



adata.obs["cluster_manual"]=adata.obs["cluster"].astype(str)
merge_list = [[5,8],[12,21],[13,23,25],[9,6]]
for temp_merger in merge_list:
	main_label = ",".join(list(map(str,temp_merger)))
	for target_merge_label in temp_merger:
		adata.obs["cluster_manual"][adata.obs["cluster_manual"]==str(target_merge_label)]=main_label




# pbmc_data["cluster_manual"]=pbmc_data["cluster"].astype(str)
# for temp_merger in merge_list:
# 	main_label = ",".join(list(map(str,temp_merger)))
# 	for target_merge_label in temp_merger:
# 		pbmc_data["cluster_manual"][pbmc_data["cluster_manual"]==str(target_merge_label)]=main_label#


# pbmc_data.to_csv("data/pbmcs/cell_annotations_with_clusters_and_spring2.tsv",sep="\t")
# sample_by_cluter_manual = pd.crosstab(pbmc_data["cluster_manual"],pbmc_data["sample"])
# sample_by_cluter_manual.to_csv("data/pbmcs/sample_clust_round1_cross_tab2.tsv",sep="\t")



sample_by_cluter_manual = pd.crosstab(adata.obs["cluster_manual"],adata.obs["sample"])
sample_by_cluter_manual.to_csv("data/pbmcs/sample_clust_round1_cross_tab2.tsv",sep="\t")


## Classical monocyte comparison
out_class_mono_df_man, running_markers_man, avg_class_mono_df_man, adata_class_mono_man = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=["5,8"],
	parent_pops=["10","19","20"],
	cluster_key = "cluster_manual",
	force=True
)



## 
out_cd4em_df_man, cd4em_running_markers_man, avg_cd4em_df_man, adata_cd4em_man = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=["12,21"],
	parent_pops=["13,23,25"],
	cluster_key = "cluster_manual",
	force=True
)

pan_t_df_man, pan_t_running_markers_man, avg_pan_t_df_man, adata_pan_t_man = get_diffs_from_parent_pop(
	adata,
	pops_of_interest=["17"],
	parent_pops=["0","13,23,25","12,21"],
	cluster_key = "cluster_manual",
	force=True
)



import os
pathways_out_dir = "data/pbmcs/pops_of_int_data/"
if not os.path.isdir(pathways_out_dir):
	os.mkdir(pathways_out_dir)



def get_up_dn(
	in_df,
	lg_fc_cut = 0.2,
	p_cut = np.exp(-20)
	):
    up = in_df["names"][(in_df["logfoldchanges"]>lg_fc_cut) & (in_df["pvals_adj"]<p_cut)]
    dn = in_df["names"][(in_df["logfoldchanges"]<-lg_fc_cut) & (in_df["pvals_adj"]<p_cut)]
    bg = in_df["names"][(in_df["logfoldchanges"]!=0)]
    return(up, dn, bg)


import os
def write_set(in_set, out_file, out_dir):
    # check if directory exists, if not, create it
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    # combine directory and filename
    full_path = os.path.join(out_dir, out_file)
    # open the file in write mode
    with open(full_path, 'w') as f:
        for item in in_set:
            # write each item on a new line
            f.write(str(item) + '\n')


#####################
seventeen_df = sc.get.rank_genes_groups_df(adata_pan_t_man, group=["17"])
seventeen_up, seventeen_dn, seventeen_bg = get_up_dn(seventeen_df)
write_set(seventeen_up, "17_up.tsv",pathways_out_dir)
write_set(seventeen_dn, "17_dn.tsv",pathways_out_dir)
write_set(seventeen_bg, "17_bg.tsv",pathways_out_dir)


seventeen_df.to_csv(os.path.join(pathways_out_dir,"17_vs_0,13,23,25,12,21tcell_degs.tsv"),sep="\t")

seventeen_up_paths = pretty_paths(gp.gprofile(
	list(seventeen_up), custom_bg = list(seventeen_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

seventeen_up_paths.to_csv(os.path.join(pathways_out_dir,"paths_up_17_vs_0,13,23,25,12,21tcell_degs.tsv"),sep="\t")

seventeen_dn_paths = pretty_paths(gp.gprofile(
	list(seventeen_dn), custom_bg = list(seventeen_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

seventeen_dn_paths.to_csv(os.path.join(pathways_out_dir,"paths_dn_17_vs_0,13,23,25,12,21tcell_degs.tsv"),sep="\t")
#####################


classical_mono_df = sc.get.rank_genes_groups_df(adata_class_mono_man, group=["5,8"])
mono_up, mono_dn, mono_bg = get_up_dn(classical_mono_df)

classical_mono_df.to_csv(os.path.join(pathways_out_dir,"monocyte_degs.tsv"),sep="\t")
write_set(mono_up, "5,8_up.tsv",pathways_out_dir)
write_set(mono_dn, "5,8_dn.tsv",pathways_out_dir)
write_set(mono_bg, "5,8_bg.tsv",pathways_out_dir)

five_eight_vs_ten_deg_df, five_eight_vs_ten_markers = get_rank_genes(adata, 
	               ["5,8"], 
	               "10", 
	               out_merged_df = None, 
	               local_running_markers = [])

five_eight_vs_ten_deg_df.to_csv(os.path.join(pathways_out_dir,"5,8_vs_10monocyte_degs.tsv"),sep="\t")







cd4em_df = sc.get.rank_genes_groups_df(adata_cd4em_man, group=["12,21"])
cd4em_df.to_csv(os.path.join(pathways_out_dir,"CD4em_12,21_vs_13,23_degs.tsv"),sep="\t")


cd4em_up, cd4em_dn, cd4em_bg = get_up_dn(cd4em_df)

write_set(cd4em_up,"12,21_up.tsv", pathways_out_dir)
write_set(cd4em_dn,"12,21_dn.tsv", pathways_out_dir)
write_set(cd4em_bg,"12,21_bg.tsv", pathways_out_dir)

########################
all_cd4em_up_paths = pretty_paths(gp.gprofile(
	list(cd4em_up), custom_bg = list(cd4em_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

all_cd4em_up_paths.to_csv(os.path.join(pathways_out_dir,"paths_up_12,21_vs_13,23_T1Dcd4em.tsv"),sep="\t")

all_cd4em_dn_paths = pretty_paths(gp.gprofile(
	list(cd4em_dn), custom_bg = list(cd4em_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

all_cd4em_dn_paths.to_csv(os.path.join(pathways_out_dir,"paths_dn_12,21_vs_13,23_T1Dcd4em.tsv"),sep="\t")

#######################



both_up = set(mono_up.tolist()).intersection(set(cd4em_up.tolist()))
both_dn = set(mono_dn.tolist()).intersection(set(cd4em_dn.tolist()))
both_bg = set(mono_bg.tolist()).intersection(set(cd4em_bg.tolist()))



write_set(both_up,"genes_up_in_both_T1Dmono_and_T1Dcd4em.tsv", pathways_out_dir)
write_set(both_dn,"genes_dn_in_both_T1Dmono_and_T1Dcd4em.tsv", pathways_out_dir)
write_set(both_bg,"genes_bg_in_both_T1Dmono_and_T1Dcd4em.tsv", pathways_out_dir)



import random
from gprofiler import GProfiler
gp = GProfiler('BLAH_'+str(random.randint(0,int(1e6))), want_header = True)

def pretty_paths(in_res):
	out_df = pd.DataFrame(in_res[1:],columns = in_res[0])
	return(out_df)

###############
## Both up and down
both_up_paths = pretty_paths(gp.gprofile(
	list(both_up), custom_bg = list(both_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

both_up_paths.to_csv(os.path.join(pathways_out_dir,"paths_up_in_both_T1Dmono_and_T1Dcd4em.tsv"),sep="\t")

both_dn_paths = pretty_paths(gp.gprofile(
	list(both_dn), custom_bg = list(both_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))
both_dn_paths.to_csv(os.path.join(pathways_out_dir,"paths_dn_in_both_T1Dmono_and_T1Dcd4em.tsv"),sep="\t")

###############
## Individualized up and down
mono_up_paths = pretty_paths(gp.gprofile(
	list(mono_up), custom_bg = list(mono_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

mono_up_paths.to_csv(os.path.join(pathways_out_dir,"paths_up_in_only_T1Dmono_not_T1Dcd4em.tsv"),sep="\t")

##
mono_dn_paths = pretty_paths(gp.gprofile(
	list(mono_dn), custom_bg = list(mono_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

mono_dn_paths.to_csv(os.path.join(pathways_out_dir,"paths_dn_in_only_T1Dmono_not_T1Dcd4em.tsv"),sep="\t")

##
cd4em_up_paths = pretty_paths(gp.gprofile(
	list(mono_up), custom_bg = list(cd4em_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

cd4em_up_paths.to_csv(os.path.join(pathways_out_dir,"paths_up_in_only_T1Dcd4em_not_T1Dmono.tsv"),sep="\t")



t_cell_receptor_related = list(cd4em_only_up_paths[cd4em_only_up_paths["term ID"]=="GO:0050852"]["Q&T list"])[0].split(",")



##
cd4em_dn_paths = pretty_paths(gp.gprofile(
	list(cd4em_dn), custom_bg = list(cd4em_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

cd4em_dn_paths.to_csv(os.path.join(pathways_out_dir,"paths_dn_in_only_T1Dcd4em_not_T1Dmono.tsv"),sep="\t")



#################
## now look at their unique profiles
mono_up = set(mono_up.tolist())
mono_dn = set(mono_dn.tolist())
cd4em_up = set(cd4em_up.tolist())
cd4em_dn = set(cd4em_dn.tolist())

mono_only_up = mono_up - both_up
mono_only_dn = mono_dn - both_dn

cd4em_only_up = cd4em_up - both_up
cd4em_only_dn = cd4em_dn - both_dn

##
mono_only_up_paths = pretty_paths(gp.gprofile(
	list(mono_only_up), custom_bg = list(mono_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

mono_only_dn_paths = pretty_paths(gp.gprofile(
	list(mono_only_dn), custom_bg = list(mono_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

cd4em_only_up_paths = pretty_paths(gp.gprofile(
	list(cd4em_only_up), custom_bg = list(cd4em_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))

cd4em_only_dn_paths = pretty_paths(gp.gprofile(
	list(cd4em_only_dn), custom_bg = list(cd4em_bg), 
    organism = "hsapiens", numeric_ns="ENTREZGENE_ACC"
))



#######################
## UGHHH FORGET IT... Memory crashes constantly, sparse inefficiencies... Not worth it..
# from scipy.sparse import csc_matrix, csr_matrix

# def z_norm_exprs(in_x, in_means):
# 	in_x=csc_matrix(in_x)
# 	norm_idxs = np.where(in_means>0)[0]
# 	for g in norm_idxs:
# 		if g%10==0:
# 			print(g,in_means[g])
# 		in_x[:,g]/=in_x[:,g].max()
# 		#in_x[:,g]=in_x[:,g]/in_x[:,g].std()
# 	in_x = csr_matrix(in_x)
# 	gc.collect()
# 	return(in_x)


# dummy_X = deepcopy(adata.X[:100,:])
# dummy = z_norm_exprs(dummy_X,np.array(dummy_X.mean(0)).flatten())


# adata.layers["Z"] = z_norm_exprs(deepcopy(adata.X), adata.var['mean_counts'].to_numpy())

##################################################
# ##################################################
# ##################################################
# temp_genes = ["CD3E","CD4","CD8A","CD14","FCGR3A"]
# num_genes = len(temp_genes)

# fig, axs = plt.subplots(1, num_genes, figsize=(5*num_genes, 5))

# for ax, gene in zip(axs, temp_genes):
#     sc.pl.dotplot(adata, 
#                   [gene], 
#                   groupby='cluster_manual', 
#                   ax=ax, # Add this line to plot in the correct subplot
#                   show=False) # Add this line to not display the legend
#     ax = blah['mainplot_ax']
#     ax.set_title(gene) # This will set the title of each subplot to the respective gene name

# plt.tight_layout() # This will make sure everything fits well
# plt.show() # This will display the figure
# ##################################################
# ##################################################
# ##################################################








# import matplotlib.pyplot as plt
# import matplotlib.gridspec as gridspec

# #temp_genes = ["CD3E","CD4","CD8A","TOX","CD79A","CST3","CD14","FCGR3A", "NKG7"]
# num_genes = len(temp_genes)

# # Create figure and gridspec
# fig = plt.figure(figsize=(5*num_genes, 5))
# gs = gridspec.GridSpec(1, num_genes, figure=fig)

# for i, gene in enumerate(temp_genes):
#     # Create an Axes object in the position of the i-th subplot
#     ax = fig.add_subplot(gs[i])
#     # Call dotplot. Since an Axes object is passed, dotplot will use it instead of creating a new one
#     sc.pl.dotplot(adata, 
#                   [gene], 
#                   groupby='cluster_manual', 
#                   ax=ax,
#                   show=False,
#                   standard_scale="group") 
#     print(ax)
#     print(type(ax))
#     ax.set_title(gene) # This will set the title of each subplot to the respective gene name
#     # Remove any legends or colorbars that might have been created
#     	# if thing != 'mainplot_ax':
#     	# 	del ax[thing]#.remove()
#     #fig.colorbar(ax.collections[0], ax=ax).remove()

# plt.tight_layout() # This will make sure everything fits well
# plt.show() # This will display the figure




temp_genes = ["CD3E","CD4","CD8A","NKG7","CCR7","TOX","CD79A","CST3","CD14","FCGR3A","JAK2","NFKB1","NFKB2"]
num_genes = len(temp_genes)


sc.pl.dotplot(adata, 
                  temp_genes, 
                  groupby='cluster_manual', 
                  standard_scale="var", 
                  ) 


# ##################################################
# ##################################################
# ##################################################

# import matplotlib.pyplot as plt
# import numpy as np
# from matplotlib.transforms import Bbox

# temp_genes = ["CD3E","CD4","CD8A","CD14","FCGR3A"]
# num_genes = len(temp_genes)

# # Create figure and subplots
# fig, axs = plt.subplots(1, num_genes, figsize=(5*num_genes, 5))

# for ax, gene in zip(axs, temp_genes):
#     # Call dotplot, return_fig=True returns the figure
#     dot_fig = sc.pl.dotplot(adata, 
#                             [gene], 
#                             groupby='cluster_manual', 
#                             show=False,
#                             return_fig=True) 
#     # Get the image data from dotplot's main plot, adjust bbox if necessary
#     bbox = Bbox(np.array([[.2, .2], [.8, .8]]))
#     dot_img = dot_fig.figimage(dot_fig['mainplot_ax'].figure.canvas.get_renderer().buffer_rgba(), 
#                                bbox=bbox, 
#                                origin='upper')
#     # Set the title of each subplot to the respective gene name
#     ax.set_title(gene)
#     # Remove everything from the ax
#     ax.cla()
#     # Paste the image data onto ax
#     ax.imshow(dot_img, aspect='auto')


# plt.tight_layout() # This will make sure everything fits well
# plt.show() # This will display the figure


##################################################
##################################################
##################################################
import matplotlib.pyplot as plt

temp_genes = ["CD3E","CD4","CD8A","CD14","FCGR3A"]
num_genes = len(temp_genes)

# Create figure
fig, axs = plt.subplots(1, num_genes, figsize=(5*num_genes, 5), squeeze=False)

for ax, gene in zip(axs[0], temp_genes):
    # Call dotplot, save returned DotPlot object
    dotplot = sc.pl.dotplot(adata, 
                            [gene], 
                            groupby='cluster_manual', 
                            show=False)
    # Copy content of dotplot's mainplot to ax
    ax.imshow(dotplot['mainplot_ax'].figure.canvas.renderer.buffer_rgba())
    # Set the title of each subplot to the respective gene name
    ax.set_title(gene)
    # Hide unwanted elements
    dotplot['color_legend_ax'].set_visible(False)
    dotplot['size_legend_ax'].set_visible(False)

plt.tight_layout() # This will make sure everything fits well
plt.show() # This will display the figure


##################################################
##################################################
##################################################


sc.pl.dotplot(adata_class_mono_man, 
       ["SAMHD1","JAK2","NOTCH2","IRAK3","FOS","FOSB","CD36",'NFKB2', 'NFKBIZ', 'NFKBID', 'NFKBIA', 'NFKB1'], 
       groupby='cluster_manual', 
                  standard_scale="var", 
                  )#, show=False)




sc.pl.dotplot(adata, 
       ['DPYD',"SAMHD1","JAK2","NOTCH2","IRAK3","FOS","FOSB","CD36",'NFKB2', 'NFKBIZ', 'NFKBID', 'NFKBIA', 'NFKB1'], 
       groupby='cluster_manual', 
                  standard_scale="var", 
                  )#, show=False)





sc.pl.dotplot(adata_class_mono_man, 
       ["IL12B","IL1B",'TNF','IL6','IL4',"IL10",'TGFB1'], 
       groupby='cluster_manual')#, show=False)


sc.pl.dotplot(adata_class_mono_man, 
       ["IL1B",'TGFB1',"CD36","SAMHD1","JAK2","FOS","FOSB", 'PPARG','NFKBIZ', 'NFKBIA', 'NFKB1'], 
       groupby='cluster_manual')#, show=False)


sc.pl.dotplot(adata_class_mono_man, 
       ["IL1A", "IL1B", "IL6", "NOS2", "TLR2", "TLR4", "CD80", "CD86", "PPARG", "ARG1", "CD163"], 
       groupby='cluster_manual')#, show=False)



classical_monocyte = ['MS4A6A','FCGR2A','CD36','IL17RA','PGD','DPYD','IER2','ANPEP','CSF3R','CSTA']
classical_monocyte_cyto = ['CXCL5','CXCL1','CXCL3','PPBP','CCL2']
monocyte_proinflamatory = ['IL1B','IL6','TNF','PTGS2','PTX3','CCL20','CXCL2','DUSP2','ATF3','TNFAIP3','BCL2A1']
monocyte_chemotaxis = ['CCL7','CCL2','CDC42','STX1A','EMP1','FABP5','DHRS3','NAB2','PTPN7','MAPK6']
monocyte_activation = ['TNF','IL6','CXCL10','IL1B','CD14','CCR2','FCGR3A','CSF1','CSF1R','CSF2','CSF2RA','CSF3','CSF3R']
non_classical = ['TOB1','FCGR3B','CDKN1C','NFKBIZ','SIGLEC10','NAP1L1','TCF7L2']



sc.pl.violin(adata_class_mono_man, 
       classical_monocyte+non_classical, 
       groupby='cluster_manual',return_plot = True)#, show=False)
sc.pl.dotplot(adata_class_mono_man, 
       classical_monocyte, 
       groupby='cluster_manual')#, show=False)
sc.pl.dotplot(adata_class_mono_man, 
       non_classical, 
       groupby='cluster_manual')#, show=False)
sc.pl.dotplot(adata_class_mono_man, 
       monocyte_proinflamatory, 
       groupby='cluster_manual')#, show=False)
sc.pl.dotplot(adata_class_mono_man, 
       monocyte_chemotaxis, 
       groupby='cluster_manual')#, show=False)
sc.pl.dotplot(adata_class_mono_man, 
       monocyte_activation, 
       groupby='cluster_manual')#, show=False)






ax = sc.pl.dotplot(adata_class_mono_man, 
       clean_markers(classical_mono_df.head(40)["names"].tolist()+classical_mono_df.tail(40)["names"].tolist()), 
       groupby='cluster_manual', show=False)



sc.pl.dotplot(adata_cd4em_man, 
      ["CD28","CD3E","CD4","TOX","FOS","FOSB"], 
      groupby='cluster_manual')#, show=False)








#########################
## TODO: left off here




#adata_class_mono = adata[adata.obs["cluster"].isin(np.array([5,8,10,19,20],dtype=np.int64))]

#sc.tl.rank_genes_groups(adata_class_mono, 'cluster', method='wilcoxon',key_added = "wilcoxon")
#sc.pl.rank_genes_groups(adata_class_mono, n_genes=25, sharey=False, key = "wilcoxon")


## focused genes of interest


for grp in ["5","8","10","19","20"]:
	running_markers=[sc.get.rank_genes_groups_df(adata_class_mono, group=[grp], key = "wilcoxon")["names"][0]]+running_markers


running_markers = clean_markers(running_markers)

sc.pl.dotplot(adata_class_mono, running_markers, groupby='cluster')#, show=False)



####################

goi=["ATP2B1","CTNNB1",
"NAMPT",#https://pubmed.ncbi.nlm.nih.gov/30031171/
"FOS",#https://pubmed.ncbi.nlm.nih.gov/9472112/
"ATG7",#https://www.nature.com/articles/s41418-019-0297-6
"RIPOR2",# polarization in T-cells, but little info in monocytes
"JAK2",
"STAT1",
"ITGA5",
"ABCA1",
"PIK3R5",
"ITGAX",
"JARID2",
"HIF1A",
"MYO9B",
"IL1B",
"IL17A",
"IL17B",
"IFNG",
"SOCS3",
"ISG15",
"IFI6",
"OAS1"]
sc.pl.dotplot(adata_class_mono, goi, groupby='cluster')#, show=False)



##########################################################################
## Just exploratory analysis on technical effects

cur_cluster = "5,8"
sub_idxs = adata.obs["cluster_manual"]==cur_cluster
sub_counts = np.array(adata.obs['total_counts'][sub_idxs])

#mono_subset_raw = adata.X[sub_idxs,:]
mono_subset = adata.X[sub_idxs,:]
mono_subset /= sub_counts[:,None]

cell_sums = np.array(mono_subset_raw.sum(1)).flatten()
sorted_cells = np.argsort(cell_sums)

mono_subset = mono_subset[sorted_cells,:]


per_gene_totals = np.max(mono_subset,axis=0)
mono_subset /= per_gene_totals
per_gene_totals = np.array(per_gene_totals).squeeze()

valid_genes = per_gene_totals>0 

mono_subset[np.isnan(mono_subset)]=0

mono_subset /= mono_subset.sum(1)

np.random.seed(123456)
cell_subset = sorted(np.random.choice(np.arange(mono_subset.shape[0]),10000))
gene_subset = sorted(np.random.choice(np.where(valid_genes)[0],1000))
plot_subset = mono_subset[cell_subset,:]
plot_subset /= plot_subset.sum(1)
sns.clustermap(
	plot_subset[:,gene_subset],
	row_cluster=False,
	#col_cluster=False
	)
plt.show()




##########################################################################


