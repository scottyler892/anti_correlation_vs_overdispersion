#!/usr/env python3
import os
import networkx as nx
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import argparse
########################


############################################################
## analysis
def process_stats(method_stats, out_dir, is_null = False):
    if is_null:
        ax = sns.boxplot(x = 'cluster_method', y='guessed_num_clust', data = method_stats)
        ax = sns.swarmplot(x = 'cluster_method', y='guessed_num_clust', data = method_stats, edgecolor = 'white', linewidth = 1)
        #ax.set_yscale('log')
        plt.ylabel("guessed number of groups")
        plt.savefig(os.path.join(out_dir,'guessed_num_groups.png'),dpi=600,bbox_inches='tight')
        
        
        plt.clf()
        ax = sns.boxplot(x = 'cluster_method', y='abs_dist_to_true_num_clust', data = method_stats)
        ax = sns.swarmplot(x = 'cluster_method', y='abs_dist_to_true_num_clust', data = method_stats, edgecolor = 'white', linewidth = 1)
        #ax = plt.lineplot(x = 'true_num_clust', y='true_num_clust', data = method_stats, color = 'black', linestyles = ':')
        #ax.set_yscale('linear')
        plt.ylabel("abs(distance to true number of clusters)")
        plt.savefig(os.path.join(out_dir,'abs_dist_to_true_num_clust.png'),dpi=600,bbox_inches='tight')


        plt.clf()
        ax = sns.boxplot(x = 'cluster_method', y='guessed_num_clust', data = method_stats)
        ax = sns.swarmplot(x = 'cluster_method', y='guessed_num_clust', data = method_stats, edgecolor = 'white', linewidth = 1)
        ax.set_yscale('log')
        plt.ylabel("log(guessed number of groups)")
        plt.savefig(os.path.join(out_dir,'log_guessed_num_groups.png'),dpi=600,bbox_inches='tight')


        method_stats["abs_dist_to_true_num_clust"] = method_stats["abs_dist_to_true_num_clust"]+1
        plt.clf()
        ax = sns.boxplot(x = 'cluster_method', y='abs_dist_to_true_num_clust', data = method_stats)
        ax = sns.swarmplot(x = 'cluster_method', y='abs_dist_to_true_num_clust', data = method_stats, edgecolor = 'white', linewidth = 1)
        #ax = plt.lineplot(x = 'true_num_clust', y='true_num_clust', data = method_stats, color = 'black', linestyles = ':')
        ax.set_yscale('log')
        plt.ylabel("log(abs(distance to true number of clusters)+1)")
        plt.savefig(os.path.join(out_dir,'log_abs_dist_to_true_num_clust.png'),dpi=600,bbox_inches='tight')
    else:
        print('\n\n',method_stats)
        ## write the dataframe to file
        # out_path = os.path.join(out_dir,"method_summary.tsv")
        # method_stats.to_csv(path_or_buf=out_path,
        #                    sep='\t', index = False)
        # plt.clf()
        # ax = sns.boxplot(x = 'method', y='max_recursion_depth', data = method_stats)
        # ax = sns.swarmplot(x = 'method', y='max_recursion_depth', edgecolor = 'white', linewidth=1, data = method_stats)
        # plt.ylabel("maximum recursion depth")
        # plt.savefig(os.path.join(out_dir,'max_recursion_depth.png'),dpi=600,bbox_inches='tight')
        # plt.clf()
        # ax = sns.boxplot(x = 'method', y='total_number_of_groups', data = method_stats)
        # ax = sns.swarmplot(x = 'method', y='total_number_of_groups', edgecolor = 'white', linewidth=1, data = method_stats)
        # ax.set_yscale('log')
        # plt.ylabel("total number of groups")
        # plt.savefig(os.path.join(out_dir,'total_number_of_groups.png'),dpi=600,bbox_inches='tight')
        
        #ax = sns.boxplot(x = 'true_num_clust', y='guessed_num_clust', hue = 'method', data = method_stats)
        #ax = sns.swarmplot(x = 'method', y='total_number_of_groups', edgecolor = 'white', linewidth=1, data = method_stats)
        ax = sns.pointplot(x = 'true_num_clust', y='guessed_num_clust', hue = 'cluster_method', dodge = True, data = method_stats)
        ax = sns.pointplot(x = 'true_num_clust', y='true_num_clust', data = method_stats, color = 'black', linestyles = ':')
        ax.set_yscale('log')
        plt.ylabel("guessed number of groups")
        plt.savefig(os.path.join(out_dir,'guessed_num_groups.png'),dpi=600,bbox_inches='tight')
        
        plt.clf()
        ax = sns.pointplot(x = 'true_num_clust', y='abs_dist_to_true_num_clust', hue = 'cluster_method', dodge = True, data = method_stats)
        #ax = plt.lineplot(x = 'true_num_clust', y='true_num_clust', data = method_stats, color = 'black', linestyles = ':')
        ax.set_yscale('linear')
        plt.ylabel("abs(distance to true number of clusters)")
        plt.savefig(os.path.join(out_dir,'abs_dist_to_true_num_clust.png'),dpi=600,bbox_inches='tight')
        #plt.savefig(os.path.join(out_dir,'total_number_of_groups.png'),dpi=600,bbox_inches='tight')
        if not is_null:
            plt.clf()
            ax = sns.boxplot(x = 'cluster_method', y='relative_cluster_mi', data = method_stats)
            ax = sns.swarmplot(x = 'cluster_method', y='relative_cluster_mi', edgecolor = 'white', linewidth=1, data = method_stats)
            ax.set_yscale('linear')
            plt.ylabel("relative mutual information")
            plt.savefig(os.path.join(out_dir,'relative_cluster_mi.png'),dpi=600,bbox_inches='tight')


            
            plt.clf()
            ax = sns.boxplot(x = 'cluster_method', y='clust_result_purity', data = method_stats)
            ax = sns.swarmplot(x = 'cluster_method', y='clust_result_purity', edgecolor = 'white', linewidth=1, data = method_stats)
            ax.set_yscale('linear')
            plt.ylabel("clustering purity")
            plt.savefig(os.path.join(out_dir,'clust_result_purity.png'),dpi=600,bbox_inches='tight')


            plt.clf()
            ax = sns.boxplot(x = 'cluster_method', y='feat_select_within_across_t_stat', data = method_stats)
            ax = sns.swarmplot(x = 'cluster_method', y='feat_select_within_across_t_stat', edgecolor = 'white', linewidth=1, data = method_stats)
            ax.set_yscale('linear')
            plt.ylabel("within vs across group distance t-statistic")
            plt.savefig(os.path.join(out_dir,'feat_select_within_across_t_stat.png'),dpi=600,bbox_inches='tight')

            for col in method_stats.columns[22:]: 
                print(col) 
                plt.clf()
                ax = sns.boxplot(x = 'cluster_method', y=col, data = method_stats)
                ax = sns.swarmplot(x = 'cluster_method', y=col, edgecolor = 'white', linewidth=1, data = method_stats)
                ax.set_yscale('linear')
                plt.ylabel(col)
                plt.savefig(os.path.join(out_dir,col+'.png'),dpi=600,bbox_inches='tight')

    return


def do_analysis(in_file, out_dir, is_null = False):
    out_dir = os.path.join(out_dir, '')
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    ## read in the summary file

    method_stats = pd.read_csv(in_file, sep = '\t')

    process_stats(method_stats, out_dir, is_null = is_null)
    return
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-in_file", '-i',
        help="The results from the analyse sim script.",
        default = "data/scRNAseq_sim_results/splatter/clustering_result_stats.tsv",
        type = str)

    parser.add_argument("-out_dir", '-out', '-o',
        help="the directory that we'll write the results to",
        default = "data/scRNAseq_sim_results/splatter/",
        type = str)

    parser.add_argument("-is_null", 
        help="if there's only one cluster",
        action = 'store_true')

    args = parser.parse_args()

    in_file = args.in_file
    out_dir = args.out_dir
    do_analysis(in_file, out_dir, is_null = args.is_null)
