#!/usr/bin/env python3
##import dependency libraries
import sys,time,glob,os,pickle,fileinput,argparse,random
from subprocess import Popen, PIPE
from sklearn.cluster import KMeans 
import numpy as np
import seaborn as sns
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import silhouette_samples, silhouette_score
import matplotlib.cm as cm
##############################################################
## basic function library
def read_file(tempFile,linesOraw='lines',quiet=False):
    if not quiet:
        print('reading',tempFile)
    f=open(tempFile,'r')
    if linesOraw=='lines':
        lines=f.readlines()
        for i in range(0,len(lines)):
            lines[i]=lines[i].strip('\n')
    elif linesOraw=='raw':
        lines=f.read()
    f.close()
    return(lines)

def make_file(contents,path):
    f=open(path,'w')
    if isinstance(contents,list):
        f.writelines(contents)
    elif isinstance(contents,str):
        f.write(contents)
    f.close()

    
def flatten_2D_table(table,delim):
    #print(type(table))
    if str(type(table))=="<class 'numpy.ndarray'>":
        out=[]
        for i in range(0,len(table)):
            out.append([])
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i]=delim.join(out[i])+'\n'
        return(out)
    else:
        for i in range(0,len(table)):
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j]=str(table[i][j])
            table[i]=delim.join(table[i])+'\n'
    #print(table[0])
        return(table)


def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def make_table(lines,delim):
    for i in range(0,len(lines)):
        lines[i]=lines[i].strip()
        lines[i]=lines[i].split(delim)
        for j in range(0,len(lines[i])):
            try:
                float(lines[i][j])
            except:
                lines[i][j]=lines[i][j].replace('"','')
            else:
                lines[i][j]=float(lines[i][j])
    return(lines)


def get_file_path(in_path):
    in_path = in_path.split('/')
    in_path = in_path[:-1]
    in_path = '/'.join(in_path)
    return(in_path+'/')


def read_table(file, sep='\t'):
    return(make_table(read_file(file,'lines'),sep))
    
def write_table(table, out_file, sep = '\t'):
    make_file(flatten_2D_table(table,sep), out_file)
    

def import_dict(f):
    f=open(f,'rb')
    d=pickle.load(f)
    f.close()
    return(d)

def save_dict(d,path):
    f=open(path,'wb')
    pickle.dump(d,f)
    f.close()

def cmd(in_message, com=True, get_out = True):
    print('\n',in_message)
    time.sleep(.25)
    return
    if com:
        if get_out:
            out = Popen(in_message,shell=True,stdout=PIPE).communicate()
        else:
            Popen(in_message,shell=True).communicate()

    else:
        Popen(in_message,shell=True)
    if com and get_out:
        return(out)
#######################################################################################




###############################################################
###############################################################


###############################################################
###############################################################

###############################################################
###############################################################

def get_dirs(top_dir):
    return(sorted([(temp_dir) for temp_dir in os.listdir(top_dir) if os.path.isdir(os.path.join(top_dir,temp_dir))]))


def lin_norm_rows(in_mat):
    row_mins = np.min(in_mat, axis = 1)
    for i in range(in_mat.shape[0]):
        in_mat[i,:] -= row_mins[i] 
    row_maxs = np.max(in_mat, axis = 1)
    for i in range(in_mat.shape[0]):
        if row_maxs[i] != 0:
            in_mat[i,:] /= row_maxs[i] 
    return(in_mat)


def get_data(in_mat_full, feature_file):
    print("\tdata: reading")
    # in_mat_full = np.array(read_table(in_file))
    #print(in_mat_full.shape)
    in_mat_num = np.array(in_mat_full[1:,1:],dtype = np.float32)
    #print(in_mat_num.shape)
    if feature_file == None:
        ## if we're working on the full matrix
        return( lin_norm_rows(in_mat_num).T )
    ## otherwise we need to subset the matrix and pick the right features
    features = np.array(read_table(feature_file))
    if features.shape == (0,):
        return("None")
    features = features[:,0].tolist()
    all_features = in_mat_full[1:,0].tolist()
    keep_features_dict = {key:value for value, key in enumerate( features ) }
    print('\t\tlooking for',len(features),'features')
    ## log which indices to keep
    print('\tdata: subsetting')
    keep_indices = []
    for i in range(in_mat_num.shape[0]):
        if all_features[i] in keep_features_dict:
            keep_indices.append(i)
    if len(keep_indices)<=1:
        return("None")
    keep_indices = np.array(keep_indices)
    ## subset the numeric matrix, normalize, transpose, and return it
    print('\tdata: normalizing')
    return( lin_norm_rows(in_mat_num[keep_indices,:]).T )


def get_hypotenus(k, sse):
    return(np.sqrt((k)**2 + sse**2))


def get_k_estimate(sse_vect):
    ## normalize the sse vector
    sse_vect = lin_norm_rows(np.array([sse_vect]))[0].tolist()
    #print('\t\t\tnormalized sse vect:',sse_vect)
    hypotenuses = []
    for k in range(len(sse_vect)):
        hypotenuses.append(get_hypotenus(k/(len(sse_vect)-1), sse_vect[k]))
        print('\t\t\thypotenuse (k='+str(k+1)+'):',hypotenuses[-1])
    k_est = np.argmin(np.array(hypotenuses))+1
    print("\t\t\t\tK estimate:",k_est)
    return(hypotenuses, sse_vect, k_est)


def get_silhouettes(n_clusters, X, cluster_labels):
    if n_clusters == 1:
        ## silhoetting doesn't even work with only one cluster...
        return([0,False])
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("\t\t\tFor n_clusters =", n_clusters,
          "\n\t\t\t\tThe average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    #######################
    # fig, ax1 = plt.subplots(1, 1)
    # fig.set_size_inches(18, 7)

    # # The 1st subplot is the silhouette plot
    # # The silhouette coefficient can range from -1, 1 but in this example all
    # # lie within [-0.1, 1]
    # # ax1.set_xlim([-0.1, 1])
    # # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # # plots of individual clusters, to demarcate them clearly.
    # ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])
    # y_lower = 10
    # for i in range(n_clusters):
    #     # Aggregate the silhouette scores for samples belonging to
    #     # cluster i, and sort them
    #     ith_cluster_silhouette_values = \
    #         sample_silhouette_values[cluster_labels == i]

    #     ith_cluster_silhouette_values.sort()

    #     print("max silhouette:",max(ith_cluster_silhouette_values))

    #     size_cluster_i = ith_cluster_silhouette_values.shape[0]
    #     y_upper = y_lower + size_cluster_i

    #     color = cm.nipy_spectral(float(i) / n_clusters)
    #     ax1.fill_betweenx(np.arange(y_lower, y_upper),
    #                       0, ith_cluster_silhouette_values,
    #                       facecolor=color, edgecolor=color, alpha=0.7)

    #     # Label the silhouette plots with their cluster numbers at the middle
    #     ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

    #     # Compute the new y_lower for next plot
    #     y_lower = y_upper + 10  # 10 for the 0 samples

    # ax1.set_title("The silhouette plot for the various clusters.")
    # ax1.set_xlabel("The silhouette coefficient values")
    # ax1.set_ylabel("Cluster label")

    # # The vertical line for average silhouette score of all the values
    # ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    # # ax1.set_yticks([])  # Clear the yaxis labels / ticks
    # # ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # #plt.show()
    # plt.clf()



    ########################

    for i in range(n_clusters):
        ## get the maximum silhoette for each cluster, comparing it to the global avg, as this indicates if there
        ## are too many clusters
        print(silhouette_avg, max(sample_silhouette_values[cluster_labels == i]))
        is_this_cluster_okay = max(sample_silhouette_values[cluster_labels == i]) > silhouette_avg
        if not is_this_cluster_okay:
            ## True means that this round has too many clusters
            print(n_clusters,"\t\t\t\tclusters was too many by silhoette method")
            return([silhouette_avg, True])
    return([silhouette_avg, False])


def do_method_analysis(full_in_mat, iteration, out_dir, method_name = '', feature_file = None, k_max = 15):
    # Fit KMeans and calculate SSE for each k
    print('working on iteration:', iteration, "and method:", method_name)
    data_normalized = get_data(full_in_mat, feature_file)
    if not type(data_normalized) == np.ndarray:
        return([[iteration, method_name, 0, 0, 0, 0, True]], [iteration, method_name, 1, 1])
    sse = []
    hypotenuses = []
    silho_avg_and_bool = []
    for k in range(1, k_max+1):
        print('\t\tRunning K-means: K =',k)
        kmeans = KMeans(n_clusters=k, random_state=1)
        kmeans.fit(data_normalized)
        sse.append(kmeans.inertia_)
        print('\t\t\tsse:',sse[-1])
        ## get the silhoettes
        silho_avg_and_bool.append(get_silhouettes(k, data_normalized, kmeans.labels_))
    
    hypotenuses, sse_norm, k_est = get_k_estimate(sse)
    silhoette_k_est = k_max
    full_out = []
    for k in range(len(sse)):
        print(silho_avg_and_bool[k])
    for k in range(len(sse)):
        full_out.append([iteration, method_name, k+1, sse_norm[k], hypotenuses[k], silho_avg_and_bool[k][0], silho_avg_and_bool[k][1]])
        ## if this is the first instance in which there were too many clusters, then the previous cluster num was correct
        if silhoette_k_est == k_max and silho_avg_and_bool[k][1]:
            silhoette_k_est = k
        print("\t\t\t\tSilhoette K estimate =",silhoette_k_est)


    # plt.title('The Elbow Method: '+method_name)
    # plt.xlabel('k'); plt.ylabel('normalized SSE')
    # sns.pointplot(x=np.array(list(range(1,k_max+1))), y=np.array(sse))
    # plt.show()
    return(full_out, [iteration, method_name, k_est, silhoette_k_est])


def do_analysis(top_dir, out_dir, k_max = 15):
    ## list out the directories
    all_iters = get_dirs(top_dir)
    all_methods = get_dirs(os.path.join(top_dir, all_iters[0]))

    ## set up the output data frames
    iter_sse_data = []
    iter_k_est = []
    ## do k-means for all datasets & methods
    for iteration in all_iters:
        temp_dir = os.path.join(top_dir, iteration, all_methods[0])
        full_in_mat = np.array(read_table(temp_dir+'/expression.tsv'))
        for method in all_methods:
            temp_dir = os.path.join(top_dir, iteration, method)
            temp_full_out, k_est_vect = do_method_analysis(full_in_mat,
                                                           iteration = iteration,
                                                           out_dir = out_dir,
                                                           method_name = method,
                                                           feature_file = temp_dir + '/sample_clustering_and_summary/genes_used_for_clustering.txt',
                                                           k_max = k_max )
            iter_sse_data += temp_full_out
            iter_k_est.append(k_est_vect)

    ## convert them to pandas data frames
    ## ([iteration, method_name, k+1, sse_norm[k], hypotenuses[k]])
    for line in iter_sse_data:
        print(line)
    iter_sse_data = np.array(iter_sse_data)
    print(iter_sse_data)

    ## [iteration, method_name, k_est]
    for line in iter_k_est:
        print(line)
    iter_k_est = np.array(iter_k_est)
    print(iter_k_est)
    temp_bool_list = np.array([eval(item) for item in iter_sse_data[:,6].tolist()],dtype = bool)
    iter_sse_df = pd.DataFrame( {"iteration":iter_sse_data[:,0].astype(int),
                                 "method":iter_sse_data[:,1],
                                 "K":iter_sse_data[:,2].astype(int),
                                 "Normalized SSE":iter_sse_data[:,3].astype(np.float32),
                                 "hypotenuse":iter_sse_data[:,4].astype(np.float32),
                                 "silhoette avg":iter_sse_data[:,5].astype(np.float32),
                                 "silhoette too many clusters":temp_bool_list} )
    iter_k_est_df = pd.DataFrame( {"iteration":iter_k_est[:,0].astype(int),
                                   "method":iter_k_est[:,1],
                                   "K-estimate":iter_k_est[:,2].astype(int),
                                   "Silhoette K-estimate":iter_k_est[:,3].astype(int)} )

    ## write them to file
    iter_sse_df.to_csv(path_or_buf = os.path.join(out_dir, "sse_and_hypotenuse_data.tsv"),sep='\t')
    iter_k_est_df.to_csv(path_or_buf = os.path.join(out_dir, "estimates_of_k_for_each_method.tsv"),sep='\t')

    ## make the plots
    ax = sns.boxplot(x = 'method', y='K-estimate', data = iter_k_est_df)
    ax = sns.swarmplot(x = 'method', y='K-estimate', edgecolor = 'white', linewidth=1, data = iter_k_est_df)
    plt.savefig(os.path.join(out_dir,'k_estimate_elbow.png'),dpi=600,bbox_inches='tight')

    plt.clf()
    ## make the plots
    ax = sns.boxplot(x = 'method', y='Silhoette K-estimate', data = iter_k_est_df)
    ax = sns.swarmplot(x = 'method', y='Silhoette K-estimate', edgecolor = 'white', linewidth=1, data = iter_k_est_df)
    plt.savefig(os.path.join(out_dir,'k_estimate_silhoette.png'),dpi=600,bbox_inches='tight')

    plt.clf()
    iter_sse_df_no_zero = iter_sse_df.iloc[np.where(iter_sse_df["K"]!=0)[0],:]
    ax = sns.pointplot(x = 'K', y="Normalized SSE", hue = "method", err_style="bars", ci=68, dodge = True, data = iter_sse_df_no_zero)
    plt.savefig(os.path.join(out_dir,'k_vs_normalized_SSE.png'),dpi=600,bbox_inches='tight')

    plt.clf()
    ax = sns.pointplot(x = 'K', y="hypotenuse", hue = "method", err_style="bars", ci=68, dodge = True, data = iter_sse_df_no_zero)
    plt.savefig(os.path.join(out_dir,'k_vs_hypotenuse.png'),dpi=600,bbox_inches='tight')

    return

def do_elbow_main():   
    parser = argparse.ArgumentParser()
    ## global arguments
    parser.add_argument(
        '-in_dir','-i',
        default = "data/scRNAseq_simElbow/1",
        help='the input directory that contains the ',
        type=str)
    parser.add_argument(
        '-out_dir','-o',
        default = "data/scRNAseq_simElbow_results",
        type=str)
    parser.add_argument(
        '-k_max',
        help= 'maximum k to try in k-means clustering (default = 15)',
        default = 15,
        type=int)
    parser.add_argument(
        '-seed','-random_seed','-rand',
        default = 1234567890,
        type=int)
    args = parser.parse_args()
    print('setting seeds to',args.seed)
    random.seed(args.seed)
    np.random.seed(args.seed)
    do_analysis(args.in_dir, args.out_dir, k_max = args.k_max)



if __name__ == '__main__':
    do_elbow_main()
